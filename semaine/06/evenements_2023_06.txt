
.. {ref} in ["local", "list"]
.. {path} in ["images", "{year}/{week}/images"]



2023-02-11 LES LIGNES BOUGENT en #Iran
=============================================

- https://nitter.manasiwibi.com/arminarefi/status/1624284774096330752#m

Après une altercation au sujet d’une femme dévoilée, un capitaine de police
déclare que celle-ci a le droit de ne pas revêtir le hidjab et que son port
obligatoire ne figure nullement dans la loi, contrairement à ce qu’affirment
les autorités.


2023-02-11 Propos homophobes de Raïsi lors de la cérémonie de l’anniversaire de la dictature islamique
===========================================================================================================

Propos homophobes de Raïsi lors de la cérémonie de l’anniversaire de la RI:
«Vous (occidentaux)en ce qui concerne la famille, vous êtes accusés en
promouvant le pire des pratiques immorales et obscènes qui est l’homosexualité
de vouloir faire disparaître l’humanité et la famille»


2023-02-11 #NoushinJafari journaliste, photographe et militante de droits des femmes est libérée
====================================================================================================

- https://twitter.com/LettresTeheran/status/1624201227327225857#m

2023-02-11 #NoushinJafari journaliste, photographe et militante de droits des femmes,
prisonnière politique depuis 2020 et signataire de la lettre de 7 détenues
de la prison d’Evin est libérée.


2023-02-11 La retransmission en direct du discours du président Raïsi à la télé pendant la cérémonie d’anniversaire de la révolution islamique a été interrompu
=====================================================================================================================================================================

- https://nitter.manasiwibi.com/LettresTeheran/status/1624331419857084416#m

La retransmission en direct du discours du président Raïsi à la télé
pendant la cérémonie d’anniversaire de la révolution islamique a été
interrompu par le groupe de hackers « Edalat-e-Ali »qui ont diffusé un
message anti-régime à la télé.



- https://kolektiva.social/@PuckArks/109847600298324712

Hackers interrupt #Iran state TV coverage of a speech by President Ebrahim Raisi

The hackers asked #Iranians to take part in nationwide protests next week
& withdraw their money from banks in protest Ending with Death to #Khamenei
#OpIran #مهسا_امینی #سامان_یاسین #Anonymous


2023-02-11 ♀️✊ ⚖️ **Femme, Vie, Liberté** 21e rassemblement samedi 11 février 2023 à Grenoble **place Félix Poulat** à 14h30 **11 février 1979 11 février 2023 44 ans de dictature cléricale, 44 années de trop** 📣
==============================================================================================================================================================================================================================================================

- :ref:`iran_grenoble_2023_02_11`

En ce 5ᵉ mois depuis la révolte de Mahsa Amini, nous arrivons à la date
anniversaire de la révolution de 1979, celle du peuple iranien pour la
liberté, confisquée par le régime des mollahs.

Et depuis, ce régime sanguinaire se maintient par une répression féroce,
au prix de larmes et de sangs. Depuis son début, la dictature islamique
d'Iran n'a fait qu'emprisonner, torturer, violer et exécuter les opposants.
Toutes les protestations et les aspirations à la vie démocratique et aux
droits fondamentaux ont été réduites en sang pour imposer les lois
obscurantistes.
Des milliers d'opposants ont péri depuis 1979, l'Iran étant la deuxième
pays qui exécute le plus dans le monde. Les prisons sont bondées.

La situation économique d'un pays qui possède la quatrième réserve mondiale
du pétrole et la deuxième réserve mondiale du gaz est catastrophique.

La gestion économique de ce gouvernement est basée sur le pillage et la
distribution de la richesse du pays à ses alliés fanatiques dans la région.

Alors que 60-70 % des Iraniens sont sous le seuil de la pauvreté,
dont 30 % sous le seuil de pauvreté absolue, les membres du Hezbollah
au Liban reçoivent 1300 dollars de salaire de la part du gouvernement
iranien.
Les libertés individuelles n'existent pas et les femmes, considérées
comme des citoyens de deuxième voir troisième classe, subissent un
apartheid violent.
Nous sommes là aujourd'hui pour dénoncer les pratiques du gouvernement
iranien. Nous sommes là pour commémorer nos morts depuis 44 ans de règne
du régime islamique.

Nous sommes là pour être la voix du peuple iranien dans son combat
contre ce régime meurtrier et terroriste qui répand ses tentacules
dans le monde entier.

Le peuple iranien est déterminé à en finir avec ce gouvernement.
Comme dit Chirine Ebadi, avocate iranienne et prix Nobel de la paix,
cette révolution est comme un train qui ne s'arrêta pas avant d'arriver
à sa destination.


2023-02-10 **A Grenoble : Veillée de soutien aux victimes de la répression en Iran** organisée par Amnesty International Grenoble
========================================================================================================================================================

- :ref:`veillee_2023_02_10`


.. _farhad_meysami_2023_02_10_{{ref}}:

2023-02-10 #FarhadMeissami en grève de la faim depuis 4 mois vient d’être libéré
=====================================================================================

- :ref:`farhad_meysami_2023_02_02`

- https://twitter.com/LettresTeheran/status/1624070764893708288#m

Ce médecin et traducteur a purgé la quasi totalité de sa peine injuste
de 4 ans de prison. Les photos récentes de lui, le montrant extrêmement
amaigri après sa grève de la faim avaient énormément choqué.


.. _fariba_adelkhah_2023_02_10_{{ref}}:

2023-02-10 #FaribaAdelkhah la détenue franco-iranienne est libérée de la prison d’Evin ce soir selon plusieurs sources
============================================================================================================================

- https://twitter.com/LettresTeheran/status/1624107983381663745#m

#FaribaAdelkhah la détenue franco-iranienne est libérée de la prison d’Evin
ce soir selon plusieurs sources

Selon l’un des avocats de #FaribaAdelkhah sa libération est définitive.
Cependant il n’est pas encore clair si elle peut quitter le pays. Fariba
a été condamnée en 2020 à 5 ans de prison pour « complot contre la sécurité nationale »,
elle vient d’être « graciée ».



2023-02-10 Les Baloutches ont manifesté pour le 19ème vendredi consécutif contre le régime des mollahs en #Iran
==================================================================================================================

- https://masthead.social/@NaderTeyf/109840761945747651

Les Baloutches ont manifesté pour le 19ème vendredi consécutif contre le
régime des mollahs en #Iran.

Les manifestant.es ont exigé la libération de tous les prisonniers politiques
et dénoncé des grands médias, entre autres Voice of America - Persian
ou la télévision monarchiste Manoto qui censurent leurs slogans
comme:"Mort à l'oppresseur/Qu'il soit le roi ou le guide suprême."

Ce slogan est visible sur cette pancarte datée de ce jour selon le calendrier iranien.


2023-02-10 Les syndicats iraniens dans les mobilisations
=============================================================

- https://laboursolidarity.org/fr/n/2533/les-syndicats-iraniens-dans-les-mobilisations

Les syndicats iraniens dans les mobilisations
Les femmes continuent de mener le soulèvement et de s'opposer non seulement
au hijab obligatoire

Le 16 janvier a marqué le quatrième mois d'un nouveau soulèvement en Iran,
qui a commencé par des protestations contre l'arrestation et le meurtre
par la police d'une jeune femme kurde, Zhina Mahsa Amini, en raison de
son hijab "inapproprié".

Plus de 18 000 personnes ont été arrêtées et plus de 500 ont été tuées
par la police et l'armée lors des manifestations qui ont eu lieu dans
tout le pays. Quatre jeunes hommes ont été pendus pour leur participation
aux manifestations et neuf autres sont dans le couloir de la mort.

Cent neuf autres manifestants risquent la peine de mort. La police et
les soldats tirent sur les manifestants dans les yeux et les parties génitales.

Les rapports indiquent une augmentation de la gravité des passages à
tabac et des viols de femmes et d'hommes en garde à vue.

Lorsque certaines personnes sauvagement battues et violées par la police
meurent de leurs blessures, le gouvernement prétend faussement qu'elles
se sont suicidées.

Le nouveau chef de la police iranienne est le célèbre Ahmad Reza Radan,
qui a été le fer de lance de la répression brutale du Mouvement vert de
2009 et le chef de la police répressive des provinces du Kurdistan,
du Sistan et du Baloutchistan.



2023-02-08 **L’Académie nationale de médecine condamne la restriction d’accès aux soins des victimes de manifestations en Iran**
==================================================================================================================================

- https://www.academie-medecine.fr/lacademie-nationale-de-medecine-condamne-la-restriction-dacces-aux-soins-des-victimes-de-manifestations-en-iran/

Depuis quatre mois, à la suite du décès de Mme Mahsa Amini, des manifestations
quotidiennes d’une part significative de la société iranienne sont réprimées
dans une grande violence.

Dénoncée par des collègues médecins franco-iraniens et par la presse
généraliste et scientifique internationale, cette répression est associée
à une restriction à l’accès aux soins.

Les soignants sont menacés dans l’exercice de leur profession.

Les médecins et infirmier(e)s qui prennent le risque de les soigner
encourent arrestations, sévices et mettent leur propre vie en danger
avec des condamnations à mort prononcées contre des collègues qui ont
rempli leur devoir.

Les blessés, par peur d’être arrêtés ne se rendent plus aux urgences avec
le risque de séquelles graves et de décès.
Dans les prisons, la situation est alarmante : les soins sont volontairement
retardés ou prématurément interrompus et les prisonniers soumis à tortures
et violences sexuelles.

L’accès aux soins et la liberté pour les soignants dans l’exercice de
leur profession sont des droits humains inaliénables et universels.

L’Académie nationale de médecine condamne fermement toute atteinte aux
missions de soins des personnels de santé.
Elle demande que l’Iran garantisse le libre accès aux soins pour les
blessés et les prisonniers, le respect de l’éthique fondamentale des
soignants dans leur activité professionnelle et la cessation de toutes
les condamnations à la peine de mort.


- https://www.academie-medecine.fr/wp-content/uploads/2023/02/23.2.8-communique-acces-aux-soins-des-victimes-de-manifestations-en-Iran-1.pdf



2023-02-08 Erfan Mortezaei : **"Ma cousine Jina Mahsa Amini ne croyait pas au voile"**
===============================================================================================

- :ref:`erfan_mortezaei_2023_02_08`

Le nombre de manifestations en Iran a diminué en ce début d'année 2023.
Ne pensez-vous pas que le mouvement de contestation né après la mort
de votre cousine touche à sa fin ?

**Non, je pense le contraire**. En 1978, cela a pris dix-sept mois au peuple
iranien pour amener la révolution qui a renversé le régime du Shah.
Et à cette époque, il n'y avait pas de manifestations tous les jours.

Aujourd'hui, le peuple continue à protester mais de manière différente
et très réfléchie. Il ne faut donc en aucun cas interpréter la diminution
du nombre de manifestations de rue comme la fin de la révolution.

Chaque nuit, à Téhéran et dans les autres villes du pays, des slogans
contre les autorités sont lancés depuis les toits des maisons.
Chaque jour, des ouvriers, des enseignants, des médecins, des étudiants
et même des écoliers font grève.
Tout cela illustre la persistance d'une vaste colère populaire à travers
l'Iran. Une chose est sûre : la répression terrible menée par les autorités
iraniennes à l'intérieur du pays, ainsi que les ingérences dont se
rendent coupables les Gardiens de la révolution à l'étranger, nous
montrent que la République islamique n'a pas d'avenir.

**La révolution se poursuit. La seule inconnue demeure la date de la chute du régime.**

2023-02-08 #FreeToomaj
==============================

I also called for the release of #ToomajSalehi in front of @IraninBerlin.
Don't worry - I continue to raise awareness for Toomaj. There is a lot
in the making and I keep making requests to the #IRI: #HowHealthyIsToomajReally,
stop his solitary confinement & #FreeToomaj. ✊️




2023-02-07 **Lâcheté de la France (épisode 1000)** Nicolas Roche a dit *être missionné par le président pour mettre fin aux malentendus et développer les relations bilatérales*
======================================================================================================================================================================================

- https://twitter.com/LettresTeheran/status/1623237318432768000#m

Nicolas Roche l’ambassadeur de France en Iran a remis ses lettre de créance
au président Raïsi. Selon IRNA Raïsi a « critiqué l’islamophobie en France »
et Nicolas Roche a dit«être missionné par le président pour mettre fin
aux malentendus et développer les relations bilatérales»

2023-02-07 **Au-delà du soutien, un engagement politique des écologistes auprès du peuple iranien**
=======================================================================================================

- :ref:`eelv_2023_02_07`


Motion
===============

Réuni à Paris les 4 et 5 février 2022, le Conseil Fédéral d’Europe Ecologie Les Verts :

- Apporte son soutien total et entier à la société civile iranienne et
  en particulier aux femmes ;

- Demande la libération immédiate de toutes les personnes détenues ou
  condamnées à mort à raison de participation aux manifestations ;

- Demande un accueil inconditionnel des Iraniennes et des Iraniens à
  l’État français et demande que le dispositif exceptionnel de la protection
  temporaire créée par la directive 2001 / 55 / CE soit proposé par la
  commission européenne au Conseil de l’Union européenne ;

- Demande aux Gouvernement et à l’Union européenne de favoriser la délivrance
  de visas à toute personne persécutée ;

- Appelle la communauté internationale à faire une priorité des violations
  des droits humains et des obligations au titre des traités dans tous
  les pourparlers et négociations avec l’Iran,

- Demande à l’Union européenne, qui a adopté des mesures restrictives
  liées à des violations des droits humains, parmi lesquelles un gel des
  avoirs et une interdiction de visa pour les personnes et entités
  responsables de graves violations des droits humains en Iran, ainsi
  qu’une interdiction d’exporter à destination de l’Iran des équipements
  susceptibles d’être utilisés à des fins de répression interne ou des
  équipements de surveillance des télécommunications, que ces mesures,
  prorogées jusqu’au 13 avril 2023, soient régulièrement mises à jour ;

- Approuve les récentes initiatives de la France, des États-Unis, du
  Royaume-Uni et de l’Allemagne tendant à dénoncer le manque de coopération
  de l’Iran dans la mise en œuvre de l’accord de Vienne sur le nucléaire
  iranien adopté le 18 octobre 2015 mais appelle à une cessation définitive
  de l’application de cet accord ;

- Invite le Gouvernement et l’Union européenne à consolider et à étendre
  la limitation de l’accès aux marchés primaire et secondaire des capitaux
  de l’Union pour les banques iraniennes, y compris celles implantées
  sur le territoire de l’Union européenne ;

- Demande au Gouvernement Français et à l’Union européenne d’ajouter des
  responsables iraniens dont les Gardiens de la Révolution, qui condamnent
  à mort des Iraniens et iraniennes, à la liste établie par l’Union des
  personnes faisant l’objet de mesures restrictives pour de graves
  violations des droits de l’homme et de saisir les biens et les avoirs
  qu’ils possèdent sur le territoire de l’Union européenne ;

- Demande au Gouvernement et à l’Union européenne d’ajouter l’instance
  iranienne des Gardiens de la Révolution à la liste des personnes, groupes
  et entités impliqués dans des actes de terrorisme et faisant l’objet
  de mesures restrictives ;

- Demande que la France rappelle son Ambassadeur en Iran et garde une
  représentation diplomatique officielle (chargé d’affaires…) susceptible
  de servir de refuge.



2023-02-07 **#MehdiBahman écrivain iranien est dans le couloir de la mort pour une simple interview avec un média israélien**
===============================================================================================================================

- nitter.unixfox.eu/LettresTeheran/status/1608446179754528771#m

#MehdiBahman écrivain iranien est dans le couloir de la mort pour une
simple interview avec un média israélien et pour avoir oeuvré toute sa
vie pour la paix entre les religions.



.. _opposition_2023_02_07_{{ref}}:

2023-02-07 8 des principales figures de l’opposition iranienne réunies le 10 février 2023 à l’université Georgetown
=======================================================================================================================

- https://nitter.manasiwibi.com/arminarefi/status/1623070938945269763#m

UNITÉ. 8 des principales figures de l’opposition iranienne à l’étranger
(Reza Pahlavi, Shirin Ebadi, Masih Alinejad, Hamed Esmaeilion, Nazanin Boniadi,
Abdullah Mohtadi, Golshifteh Farahani et Ali Karimi) réunies le 10 février
à l’université Georgetown https://giwps.georgetown.edu/event/the-future-of-irans-democracy-movement-event/

UN VENT D’ESPOIR gagne à nouveau l’opposition iranienne après un mois de
janvier assez morose, avec moins de manifestations de rue.

Huit figures du mouvement à l’étranger acceptent de mettre de côté leurs
divergences pour se réunir à Washington et parler de l’avenir de l’#Iran.

DIS PAPA ? Pourquoi les Iraniens pensent-ils davantage avec leur cœur qu’avec leur tête ? 🤔
---------------------------------------------------------------------------------------------------

- https://nitter.manasiwibi.com/arminarefi/status/1623329621675581441#m

DIS PAPA ? Pourquoi les Iraniens pensent-ils davantage avec leur cœur qu’avec leur tête ? 🤔


2023-02-07 **Pour avoir féfendu le choix d’une femme dévoilée, ce médecin de Kashmar, dans le nord-est de l’#Iran, a vu sa clinique être fermée**
===================================================================================================================================================

- https://nitter.manasiwibi.com/arminarefi/status/1623014848354258950#m

POUR AVOIR DÉFENDU le choix d’une femme dévoilée qui avait été prise à
partie par une patiente voilée, dans une vidéo massivement partagée sur
les réseaux sociaux samedi 4 février, ce médecin de Kashmar, dans le
nord-est de l’#Iran, a vu sa clinique être fermée par les autorités.




2023-02-07 **#ArmitaAbbasi est libérée de la prison de Katchoï à Karadj**
===========================================================================

- https://twitter.com/LettresTeheran/status/1623002891509280774#m

Excellente nouvelle annoncée par son père sur Instagram #ArmitaAbbasi est
libérée de la prison de Katchoï à Karadj aujourd’hui.

- https://nitter.manasiwibi.com/arminarefi/status/1623051939339706369#m

GRANDE NOUVELLE. Le père d’#ArmitaAbbasi, manifestante emprisonnée depuis
le 10 octobre 2022 à Karaj, à l’ouest de Téhéran, et qui aurait été victime
de plusieurs agressions sexuelles en détention selon une enquête de @CNN,
vient d’annoncer la libération de sa fille sur Instagram


2023-02-07 **#KavehMazaheri réalisateur jette à la rivière dans cette vidéo les prix obtenus au festival gouvernemental “Fajr”**
==================================================================================================================================

- https://twitter.com/LettresTeheran/status/1622983489472679937#m

#KavehMazaheri réalisateur jette à la rivière dans cette vidéo les prix
obtenus au festival gouvernemental “Fajr” : « Je ne ferai plus jamais de
film sous ce régime totalitaire et meurtrier.

En espérant la liberté #FemmeVieLiberté »


2023-02-06  #SalmanRushdie, cible d’une fatwa de Khomeyni en 1989, réapparaît dans une interview au @NewYorker
==================================================================================================================

- https://nitter.manasiwibi.com/arminarefi/status/1622844733021102080#m

« J’AI CONNU MIEUX mais vu ce qui s’est passé, je ne vais pas si mal. »
Six mois après l’agression au couteau qui lui a fait perdre la vue d’un oeil
et l’usage d’une main, @SalmanRushdie, cible d’une fatwa de Khomeyni en 1989,
réapparaît dans une interview au @NewYorker ⬇️.



2023-02-06  #Baraye Pour les yeux perdus. Pour la liberté.
==============================================================

- https://nitter.manasiwibi.com/FaridVahiid/status/1622700963256578048#m

Beaucoup d’Iraniens font le parallèle entre les yeux perdus des manifestants
en #Iran et celui de l’écrivain Salman Rushdie. #MahsaAmini


- https://nitter.manasiwibi.com/arminarefi/status/1622858627601121281#m

« SYSTÉMATIQUE », voilà comment @IHRights qualifie la pratique des forces
de sécurité en #Iran visant à cibler les yeux des manifestants.
Cette ONG de référence a recensé 22 cas de protestataires rendus aveugles
d'un œil des suites de tirs, dont 9 femmes.


.. _eborgnes_2023_02_06_{{ref}}:

2023-02-06 **On estime à 600 le nombre de citoyens qui ont été éborgnés** par les forces de sécurité du régime islamique
========================================================================================================================

- https://twitter.com/LettresTeheran/status/1622679676597555201#m

«Cet œil aveugle verra la vérité obscure de ce monde » a écrit #HossinAbedini,
20 ans, visé dans les yeux avec du plomb. Depuis le début de soulèvement
on estime à 600 le nombre de citoyens qui ont été éborgnés par les forces
de sécurité du régime islamique.


.. _benjamin_briere_2023_02_06_{{ref}}:

2023-02-05 **#BenjaminBrière 27 ans, otage français du régime iranien entame une grève de la faim**
=====================================================================================================

- :ref:`benjamin_briere`
- https://twitter.com/LettresTeheran/status/1622716346030075904#m
- https://www.francetvinfo.fr/monde/iran/manifestations/iran-l-otage-francais-benjamin-briere-entame-une-nouvelle-greve-de-la-faim_5643653.html

|BenjaminBriere|  #BenjaminBrière 27 ans, otage français du régime iranien entame une grève
de la faim pour protester contre sa détention arbitraire, déclare sa soeur
« La seul arme avec laquelle il puisse lutter ».

- https://nitter.manasiwibi.com/HRANA_English/status/1622942554764312577#m

Imprisoned French tourist #Benjamin_Briere went on a hunger strike to
protest against not addressing his issues. He is currently serving his
sentence in Vakil-Abad Prison, #Mashhad.
#Iran


.. _hamideh_zeraei_2023_02_06_{{ref}}:

**#HamidehZeraei elle n’a rien fait de regrettable pour s’excuser et qu’elle préfère rester en prison que signer ce papier**
=============================================================================================================================

- https://twitter.com/LettresTeheran/status/1622585553832009728#m

Source proche de #HamidehZeraei condamnée à 1 an de prison : « Elle a refusé
de signer « l’acte de repentir »condition sine qua non pour être « graciée »
en affirmant qu’elle n’a rien fait de regrettable pour s’excuser et qu’elle
préfère rester en prison que signer ce papier»


