
.. local in ["local", "list"]
.. images in ["images", "{year}/{week}/images"]



2023-06-20 **New Zealand's Foreign Affairs Minister @NanaiaMahuta announced on Monday  a third tranche of sanctions on Iran**
=================================================================================================================================

- https://uk.unofficialbird.com/IranIntl_En/status/1671174909844652034#m

New Zealand's Foreign Affairs Minister @NanaiaMahuta announced on Monday
a third tranche of sanctions on Iran, saying it was a message to Iran
that the country would not tolerate denial of human rights and violent
suppression of protests.
