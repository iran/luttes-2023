
.. list in ["local", "list"]
.. 2023/14/images in ["images", "{year}/{week}/images"]


.. _attaque_gaz_2023_04_08_list:

2023-04-08 **UNE DIZAINE d’écoles pour filles en #Iran ont été à nouveau la cible ce samedi 8 avril d’attaques au gaz dans 5 villes (Ardabil, Naghadeh, Orumieh,  Pardis, Haftguel)**
==========================================================================================================================================================================================

- http://nitter.smnz.de/arminarefi/status/1644745542541377536#m
- https://www.lepoint.fr/monde/iran-des-cas-d-intoxications-d-ecolieres-encore-recenses-08-04-2023-2515469_24.php


UNE DIZAINE d’écoles pour filles en #Iran ont été à nouveau la cible ce
samedi 8 avril d’attaques au gaz dans 5 villes (Ardabil, Naghadeh, Orumieh,
Pardis, Haftguel), provoquant l’hospitalisation d’une centaine d’élèves.

Images de l’école primaire Saadi d’Orumieh (nord-ouest).

- http://nitter.smnz.de/arminarefi/status/1644652389297713152#m

REPRISE de la semaine de classe en #Iran ce samedi 8 avril et nouvelle
attaque au gaz (non identifié) contre une école pour fille, ici
l’établissement Khiam de Pardis, ville au nord-est de Téhéran, qui a
abouti à l’empoisonnement de plusieurs élèves.


- http://nitter.smnz.de/arminarefi/status/1644729991702224899#m

REPRISE de la semaine de classe en #Iran ce samedi 8 avril et nouvelle
attaque au gaz (non identifié) contre une école pour fille, ici l’établissement
Mehraj d’Ardabil, ville du nord-ouest de l’#Iran.

- http://nitter.smnz.de/arminarefi/status/1644745542541377536#m


2023-04-08 Today we are calling on the @UN_HRC to take urgent action to protect Iranian rapper **Toomaj Salehi**
=======================================================================================================================

- https://twitter.com/HillelNeuer/status/1644803371226877956#m

Today we are calling on the @UN_HRC to take urgent action to protect Iranian
rapper Toomaj Salehi—jailed for protesting the regime—who faces possible
execution and is being denied proper medical treatment.

The UN’s highest human rights body should demand his immediate release


♀️✊ ⚖️ **Femme, Vie, Liberté** 28e rassemblement samedi 8 avril 2023 à Grenoble **place Félix Poulat** à 14h30 **Non à la république islamique d'Iran, en mémoire de ceux qui ont perdu la vie depuis la révolution Mahasa Jina Amini** 📣
==============================================================================================================================================================================================================================================================

- :ref:`iran_grenoble_2023_04_08`




2023-04-08 #MojahedKourkour La mère de #KianPirfalak a posté une photo avec la mère de Mojahed et apporté son soutien au condamné à mort en écrivant « mon Dieu, ne permet pas que le sang d’un autre innocent soit versé »
=============================================================================================================================================================================================================================

- https://nitter.poast.org/1500tasvir_en/status/1644670364360794112#m

#MojahedKourkour le nouveau bouc émissaire du régime islamique condamné
à mort suite à un simulacre de procès inéquitable et arbitraire pour
avoir tué 7 personnes, notamment l’enfant de 10 ans #KianPirfalak,
assassiné par les forces de sécurité du régime, selon ses parents
témoin de la fusillade.

:ref:`La mère de #KianPirfalak <viens_et_sois_zeynab>`  a posté une photo avec la mère de Mojahed et
apporté son soutien au condamné à mort en écrivant « mon Dieu, ne permet
pas que le sang d’un autre innocent soit versé ».

.. _asenath_barzani_2023_04_06_list:

2023-04-06 **Kurde, Juive et féministe avant la lettre : Asenath Barzani**
===========================================================================

- https://blogs.mediapart.fr/carol-mann
- https://blogs.mediapart.fr/carol-mann/blog/060423/kurde-juive-et-feministe-avant-la-lettre-asenath-barzani
- https://fr.wikipedia.org/wiki/Asenath_Barzani
- https://rojava.frama.io/luttes/_downloads/4ec6d47e562b3fd806429c2db37d9faa/Kurde_Juive_et_feministe_avant_la_lettre_Asenath_Barzani.pdf

Et si le féminisme caractéristique de la gestion kurde de la région du
Rojava en Syrie du nord avait des origines culturelles bien ancrées dans
l’histoire locale ?

L’exemple remarquable de la spécialiste rabbinique Asenath Barzani (1590-1670)
qui défia les stéréotypes de genre avec l’assentiment de sa communauté
est-il encore imaginable dans un Moyen-Orient dominé par Erdogan et ses sbires ?


2023-04-06  🇮🇷 **Imprisoned Dissident Rapper Toomaj Salehi, Voice of Iran’s Protests, Faces Possible Execution**
======================================================================================================================

- https://iranhumanrights.org/2023/04/imprisoned-dissident-rapper-toomaj-salehi-voice-of-irans-protests-faces-possible-execution/


2023-04-05 🇮🇷 Hommage à #Hamidreza_Rouhi, manifestant de 19 ans tué en novembre 2022
=======================================================================================

- https://nitter.net/arminarefi/status/1643871684556931073#m

QUI A DIT que les manifestations étaient terminées en #Iran ?

« Ceci est l’ultime message, notre objectif est [de renverser]
l’ensemble du régime ! », a scandé mercredi 5 avril 2023 une foule réunie à
Téhéran en hommage à #Hamidreza_Rouhi, manifestant de 19 ans tué en novembre 2022.


2023-04-03 🇮🇷 Today, Monday, **the first chemical attack on students was carried out in the Iranian new year**
=================================================================================================================

- https://nitter.grimneko.de/i/status/1642871278406074368

Today, Monday, the first chemical attack on students was carried out in
the Iranian new year.

At least 5 students in Naghade city were poisoned and taken to the hospital.

**This is the first school to be chemically attacked in the new year,
following a series of student.**


2023-04-02  **“Woman, Life, Freedom”: Syrian Women Are Rising Up Against Patriarchy**
============================================================================================

- https://truthout.org/articles/woman-life-freedom-syrian-women-are-rising-up-against-patriarchy/


Syrian women are building on the legacy of Kurdish feminism to lead a political and cultural transformation.



2023-04-02 **Somayeh Rostampour : « En Iran, le mur de la peur est tombé »**
====================================================================================

- :ref:`somaye_iran_2023_04_02`


2023-04-02 « Femme, vie, liberté » au Kurdistan d’Irak
==============================================================

- https://www.mediapart.fr/journal/international/020423/femme-vie-liberte-au-kurdistan-d-irak

Jina sort son smartphone, montre « la vie avant et après ».

Elle dit qu’elle est « née le 17 septembre 2022 », quand, sur la tombe
de Jina Mahsa Amini, elle a ôté son voile, coupé en public ses très longs
cheveux, qu’elle a rasés totalement par la suite.

Un geste qui la surprend encore aujourd’hui, de deuil, de solidarité et
d’émancipation, imité par d’autres femmes, encouragé par la foule en colère.

« On pleurait, on criait, on était si tristes et, en même temps, on se
sentait tous ensemble si puissants pour réclamer la fin du régime et
justice pour Jina Mahsa Amini. »

C’était le matin. Jina avait passé une nouvelle nuit blanche sur les
réseaux sociaux, à suivre le mot-dièse #JinJiyanAzadi, qui n’était pas
encore le cri de ralliement, à discuter avec les copines :
« On ne peut pas rester derrière nos écrans à ne rien faire. »



2023-04-01 🇫🇷 Annonce de "La soirée des cent voix pour l’Iran" pour le 13 avril 2023
===========================================================================================

- :ref:`cent_voix_2023_04_13`
- https://nitter.net/Azadi4iranParis/status/1642866070053609474#m
- https://www.avocatparis.org/agenda-des-formations/soiree-des-cent-voix-pour-liran

Nous organisons avec @Avocats_Paris la soirée des cent voix pour l’Iran.

En soutien à la révolution en cours nous plaidons pour les Iraniennes
et Iraniens.

Venez vous aussi porter votre voix en participant à notre plaidoirie collective !


2023-04-03 🇮🇷 La fille de l’un est #JinaAmini‌ , elle a été assassinée par la police des moeurs d’un régime misogyne, le fils de l’autre est #MohammadMehdiKarami
====================================================================================================================================================================

- https://twitter.com/LettresTeheran/status/1642791901941080065#m

Tous les deux sont kurdes et d’origine modeste.
La fille de l’un est #JinaAmini‌ , elle a été assassinée par la police
des moeurs d’un régime misogyne, le fils de l’autre est #MohammadMehdiKarami,
il a été pendu pour faire perdurer un dictateur.

Unis dans le deuil, ils réclament justice.

.. figure:: 2023/14/images/peres.png
   :align: center


2023-04-03 🇮🇷 Urgent - Tsahal estime que le drone qui est entré sur le territoire israélien et a été intercepté est iranien
===================================================================================================================================

- https://twitter.com/LettresTeheran/status/1642617738139955207#m


2023-04-03 🇮🇷  incitation des miliciens basidji à harceler, agresser et dénoncer les femmes dévoilées, pour créer un climat de terreur
=========================================================================================================================================

- https://twitter.com/LettresTeheran/status/1642551305309749252#m

A l’approche des beaux jours la guerre du régime islamique contre les
femmes refusant le voile obligatoire est lancée sur plusieurs fronts :
l’incitation des miliciens basidji à harceler, agresser et dénoncer les
femmes dévoilées, pour créer un climat de terreur


2023-04-03 🇮🇷 In recent weeks, the Islamic Republic has broken the tombstones of many victims of the recent protests, or has erased the writings and pictures on them with paint
=======================================================================================================================================================================================

- https://nitter.poast.org/1500tasvir_en/status/1642781547261571072#m

Destruction of the tombstone of Abbas Shafiee, who was killed by the
Islamic Republic’s forces.

In recent weeks, the Islamic Republic has broken the tombstones of many
victims of the recent protests, or has erased the writings and pictures
on them with paint.
