
.. list in ["local", "list"]
.. 2023/26/images in ["images", "{year}/{week}/images"]



.. _ref_rapport_cgt_iran_2023_06:

2023-06 **CGT Activités internationales numéro juin 2023 N°40 spécial Iran**
=============================================================================

- :ref:`media_2023:rapport_cgt_iran_2023_06`


2023-06-26 #HanaKamkar, grande chanteuse iranienne, a chanté seule et sans voile lors d'une soirée d'hommage au réalisateur iranien Abbas Kiarostami
========================================================================================================================================================

- http://nitter.unixfox.eu/LettresTeheran/status/1673223785653182466#m

#HanaKamkar, grande chanteuse iranienne, a chanté seule et sans voile
lors d'une soirée d'hommage au réalisateur iranien Abbas Kiarostami.

Selon la loi du régime islamique, les femmes n'ont pas le droit de chanter
seules, sauf en chorale avec des hommes. Hana avait annoncé qu'elle
refuserait de chanter tant qu'elle serait obligée de chanter avec des hommes.


.. _sarah_salemi_2023_06_26_list:

2023-06-26 **L’intervention d’une camarade iranienne sur le paysage social et syndical en Iran** 🇮🇷
======================================================================================================================================================

- https://www.communisteslibertairescgt.org/FERC-Un-vent-syndicaliste-revolutionnaire-souffle-sur-le-congres.html
- :ref:`sarah_selami`

Un vent syndicaliste révolutionnaire souffle sur le congrès de la FERC-CGT

Du 22 au 26 mai, les quelques 150 congressistes de la Fédération Éducation
Recherche Culture de la CGT (FERC) se sont réuni·es à l’École Nationale
de Voile et de Sports Nautiques de Saint-Pierre-Quiberon (56).

Les débats ont été d’une très bonne tenue dans l’ensemble et tout le monde
soulignait une atmosphère de franche camaraderie qui tranche nettement,
et c’est tant mieux, avec celle du 53ème congrès confédéral.

Venu·es de l’éducation nationale, de l’enseignement supérieur, de la recherche,
de l’enseignement privé, de la formation professionnelle, de l’éducation
populaire ou encore du monde du sport, les délégué·es ont pris 5 jours
dans le contexte d’un mouvement social historique pour débattre de 4
résolutions principales et renouveler la direction de leur fédération.

A travers ces textes d’orientation, la 8ème fédération de la CGT a réaffirmé
la priorité du combat syndical contre l’extrême-droite, a affiné sa vision
et ses propositions quant à l’intégration des jeunes, notamment étudiant·es,
dans la CGT et ses perspectives de développement.

La résolution 1, plus longue, visait à replacer la fédération dans le
contexte social et politique, national et international.

Après quelques 500 amendements déposés et étudiés un à un, ces quatre
résolutions ont été adoptées avec a minima plus des trois quarts des
mandats présents.

Cette cohésion de la fédération et de ses différents secteurs s’est
également exprimée dans l’unanimisme avec lequel ont été validés les
rapports d’activité (91,21%) et les rapports financiers (99, 26%)
présentés par la direction sortante.

Loin des caricatures diffusées à l’occasion de la violente campagne de
dénigrement menée contre la secrétaire générale sortante, Marie Buisson,
la FERC est une fédération en constant développement, particulièrement
sous son mandat.
Si elle est minoritaire dans l’éducation nationale, bien qu’en constante
progression, elle est première organisation dans certaines branches de
son champ d’activité.

Le congrès aura ainsi été l’occasion de dénoncer les atteintes aux valeurs
CGT précédant le congrès confédéral et pendant ce dernier, les attaques
dont Marie Buisson a été victime et de rappeler l’attachement de la
fédération à un fonctionnement démocratique.

Si Marie Buisson a comme prévu quitté les fonctions de secrétaire générale
pour céder la place à Charlotte Vanbesien, elle reste membre de la
Commission exécutive fédérale où elle a été brillamment réélue à 99,34%
des mandats, et été saluée à de nombreuses reprises au cours du congrès
pour le rôle qu’elle a joué dans le développement de la fédération ces
dernières années.

Un syndicalisme de branche dynamique
---------------------------------------------

Parmi les éléments marquants que nous tenons à souligner, la fédération
fait état d’un dynamisme certain, avec une augmentation impressionnante
du nombre de syndiqué·es dans quasiment tous ses secteurs, en particulier
dans le privé (enseignement privé & formation pro, éducation populaire,
sport) mais aussi dans l’éducation nationale.

Dans l’éducation populaire, une dizaine de syndicats départementaux ont
vu le jour ou ont été revitalisés au cours des deux dernières années et
devraient constituer d’ici la fin de l’année une union nationale pour
se coordonner au mieux et de façon démocratique. La dynamique a permis
à la FERC de largement contribuer aux grèves historiques (premières grèves
nationales) dans le secteur de l’éducation populaire et en particulier du
périscolaire construites entre novembre 2021 et Juin 2022.

La CGT est ainsi première organisation syndicale dans la branche, en
constante progression avec 43% des voix sur le dernier cycle électoral,
mais avec un taux de syndicalisation encore très faible, d’où l’importance
particulière que revêt la dynamique de structuration syndicale en cours.

Elle joue aussi un rôle important dans l’opposition actuelle au SNU.

Cette structuration s’inscrit parfaitement dans notre perspective
syndicaliste révolutionnaire d’un syndicalisme de branche, unifié
par-delà les divisions que nous imposent les capitalistes.

Elle vise à rassembler les travailleuses et les travailleurs d’un même
secteur et à abolir les petites féodalités syndicales dans lequel le
syndicalisme d’entreprise a entrainé de trop nombreux syndicats.

Cette question de la structuration est également revenue sur le devant
de la scène au moment des échanges concernant l’enseignement supérieur
et la recherche.
La fédération compte actuellement 2 syndicats nationaux dans la recherche
(SNTRS et INRAE), une Union nationale de syndicats locaux FERC Sup
essentiellement structurés autour des universités, et un syndicat de site
des universités de Montpellier, qui syndique tous les travailleurs et
travailleuses de ces sites, quelle que soit l’institution dont ils dépendent,
mais qui refuse de s’affilier à l’Union nationale.

Le congrès a abouti à la mise en place d’un processus de réflexion fédéral
sur la structuration avec l’affirmation de la volonté d’une simplification
et d’une meilleure prise en compte des travailleurs et travailleuses précaires.

Affirmation d’un syndicalisme féministe
--------------------------------------------

Ce congrès a été aussi l’occasion d’affirmer fortement un féminisme
syndical ancré dans le quotidien des travailleuses. Plusieurs interventions
ont été chaudement saluées tout au long du congrès, qu’elles affirment
le soutien aux femmes victimes de violences sexistes et sexuelles (VSS)
ou de nouvelles revendications. La fédération a ainsi largement voté un
amendement intégrant la revendication d’un congé menstruel pour les
travailleuses de nos secteurs.
Elle a aussi révisé ses statuts afin d’intégrer une cellule de veille
fédérale contre les VSS, sur le modèle de ce que les camarades ont
défendu depuis des années au sein de la confédération.

La nécessité d’un tel outil était d’autant plus criante depuis la funeste
affaire Benjamin Amar, ancien animateur médiatique du syndicat de
l’éducation nationale du Val-de-Marne qui a fini par être exclu de
la direction confédérale après une plainte pour viol par une autre
camarade du syndicat.

La table-ronde du mercredi soir sur l’histoire de la syndicalisation des
enseignant·es, à laquelle ont participé plusieurs générations d’anciens
secrétaires généraux de la fédération, a d’ailleurs permis de mesurer
le chemin parcouru depuis la création de la fédération.

Les quelques réticences venues des quelques délégués appartenant aux
réseaux du **POI et néo-stal/FSM qui se sont exprimées à l’occasion de
ces débats ont été très vite mises en minorité par des interventions
brillantes de jeunes camarades en pointe sur ces sujets**.


.. _sara_salemi_2026_06_26_list:

**L’intervention d’une camarade iranienne sur le paysage social et syndical en Iran** 🇮🇷
--------------------------------------------------------------------------------------------

De la même manière, :ref:`l’intervention d’une camarade iranienne sur le
paysage social et syndical en Iran <sarah_selami_cgt_2023_03_30>` a mis tout le monde d’accord quant
au fantasme des **réseaux néostals/FSM** sur le retour de la CGT au sein
de la Fédération Syndicale Mondiale.

Une motion votée à l’unanimité (https://www.ferc-cgt.org/14e-congres-motion-soutien-a-sara-selami-militante-iranienne)
est d’ailleurs venue ré-affirmer notre solidarité avec la camarade qui
s’est exprimée sur ce même sujet au 53ème congrès confédéral, **alors
qu’un courrier menaçant et innacceptable des organisations pro-FSM au
sein de la CGT appelait à la faire châtier**.

Les partisans de la FSM n’ont pas osé protester et le slogan « Femme, Vie, Liberté »
résonnait dans toute l’école de voile alors que le congrès touchait à sa fin.

Un horizon revendicatif syndicaliste révolutionnaire
--------------------------------------------------------

Le débat a été l’occasion pour de nombreuses et nombreux congressistes
d’aborder des problématiques qui touchent le monde du travail mais
dépassent aussi le cadre de l’administration, de l’association ou de
l’entreprise.

Le document d’orientation adopté rappelle l’importance d’adapter nos
outils syndicaux à l’évolution du salariat, tout en développant des
formes de sociabilités syndicales et de structuration à même d’organiser
les précaires et de porter leurs revendications (titularisation dans le
secteur public, suppression des CEE et contrats civiques dans le privé,
refus des temps partiels imposés…).

Mais le débat a aussi été l’occasion de plusieurs interventions venues
rappeler les dangers que représentent les offensives islamophobes de
l’extrême-droite et le glissement de ces thématiques vers le centre de
l’échiquier politique, les tentations antisémites notamment portées par
les multiples théories du complot ou encore l’accélération du dérèglement
climatique et les impacts que celui-ci aura et a déjà sur les
travailleuses et les travailleurs.

Face à cela, le travail unitaire a été plusieurs fois défendu, dans les
prises de parole du débat général, les amendements sur les résolutions,
la présentation de VISA (Vigilance et Initiatives Syndicales Antifascistes),
association dont la FERC-CGT est membre, ou encore la motion sur le 53ème congrès
qui réaffirme dans un jeu de mot bien trouvé (https://www.ferc-cgt.org/14e-congres-motion-congres-confederal-plus-jamais-ca)
la volonté de continuer le travail initié au sein du collectif Plus Jamais Ça.

Tout aussi significatives, la résolution n°1 et la table-ronde du jeudi
après-midi avec la participation de camarades de Sud-Education, de la
FSU et un militant local du SGEN-CFDT ont été l’occasion de réaffirmer
l’horizon d’unification du syndicalisme de lutte et de transformation
sociale que nous défendons en tant que syndicalistes révolutionnaires.

Pour finir en beauté, la brise marine qui souffle sur la presqu’île a
regonflé les voiles de notre barque syndicale révolutionnaire puisqu’un
amendement d’abord rejeté par la commission a été fièrement défendu par
un syndicaliste des Hauts-de-Seine puis adopté à une très confortable
majorité…

Cet amendement défend l’idée que le syndicat combat l’Etat-Nation et
envisage la destruction de l’Etat comme un prérequis nécessaire pour
l’avènement de la paix dans le monde.

A l’heure où les nationalismes prospèrent dans le sang des travailleuses
et des travailleurs de trop nombreuses régions du monde, nous saluons
ce retour aux origines syndicalistes-révolutionnaires de la CGT dans
lequel nous nous inscrivons totalement.

Il reste évidemment bien des chantiers à mener pour confirmer cette
orientation, notamment en renouant avec ce que notre courant a
historiquement appelé « stratégie de contrôle ouvrier » en en déclinant
une version adaptée à l’évolution du salariat et en prenant en compte
les restructurations capitalistes dans notre secteur, et leurs effets,
pour mieux les combattre.

Mais une chose est sure : ce 14ème congrès aura été par bien des aspects
un congrès historique pour la FERC.

Tâchons désormais d’élargir la bouffée d’air qu’a constitué ce congrès
à l’ensemble de la confédération !



