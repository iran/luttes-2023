
.. local in ["local", "list"]
.. images in ["images", "{year}/{week}/images"]



2023-05-20 ♀️✊ ⚖️ 📣 **Femme, Vie, Liberté** 32e rassemblement samedi 20 mai 2023 à Grenoble **place Félix Poulat** à 14h30 **Unis contre les exécutions en Iran** 📣
=====================================================================================================================================================================================================================================================================================================================

- :ref:`iran_grenoble_2023_05_20`


2023-05-20 |JinaAmini| ❤️ **Samedi 20 mai 2023 toute la journéee à la MJC Exposition "Femme, Vie, Liberté" en soutien au peuple iranien** #FemmeVieLiberté #MasahAmini ♀️✊ 📣
===================================================================================================================================================================================


**Une semaine d'actualité sur l'iran (Semaine 20 Du lundi 15 mai au dimanche 21 mai 2023 par Bahareh Akrami)**
===================================================================================================================

- :ref:`akrami_iran:bahareh_akrami_2023_20`


2023-05-19 **Ce vendredi les habitants de Téhéran laissent exploser leur colère contre le régime assassin des mollahs depuis les toits de leurs appartements**
================================================================================================================================================================

- :ref:`pendaison_2023_05_19`
- http://nitter.unixfox.eu/LettresTeheran/status/1659479921876508674#m

Ce vendredi (jour férié en Iran), après l’annonce de l’exécution des trois
manifestants, les habitants de Téhéran laissent exploser leur colère contre
le régime assassin des mollahs depuis les toits de leurs appartements.


2023-05-19 **#SalehMirhashemi, #MajidKazemi, and #SaeedYaghoubi three jailed protestors, have been executed by Iran regime this morning**
=============================================================================================================================================

- https://nitter.poast.org/IranHrm/status/1659483400988753924#m

The death sentences of 3 prisoners were carried out in dastgerd Prison
in Esfahan on Thurs, May 19 2023.

#SalehMirhashemi, #MajidKazemi, and #SaeedYaghoubi three jailed protestors,
have been executed by Iran regime this morning.

#IRGCterroirsts
#StopExecutionsInIran


2023-05-17 **« NE LES LAISSEZ PAS nous tuer.»**
===================================================

- http://nitter.unixfox.eu/arminarefi/status/1658833466920763393#m

« NE LES LAISSEZ PAS nous tuer.» Dans un message manuscrit sorti mercredi
de la prison Dastguerd d'Ispahan, Saeed Yaghoubi, Majid Kazemi et Saleh Mirhashemi,
arbitrairement condamnés à mort pour le décès de 3 agents pro-régime,
implorent leurs compatriotes de les soutenir.

- http://nitter.unixfox.eu/LettresTeheran/status/1657282303368855552#m

#MajidKazemi manifestant qui risque la pendaison à tout moment raconte
depuis la prison comment ses aveux forcés lui sont extorqués « Ils m’ont
brisé les côtes et m’ont fait monter sur une chaise (simulation de pendaison)
mais je n’ai pas avoué, quand ils m’ont dit ta femme et ta cousine sont
ici pour avouer, j’ai dit relâchez les je dirai tous ce que vous voulez »


2023-05-17 **Communiqué de l’union syndicale des enseignants iraniens en soutien à Cécile Kohler** |CecileKohler| **et Jacques Paris** |JacquesParis|
=========================================================================================================================================================

- http://nitter.unixfox.eu/LettresTeheran/status/1658745105153511428#m
- :ref:`cecile_kohler`
- :ref:`jacques_paris`

A l’occasion de l’anniversaire de l’arrestation du couple d’enseignants
français Cécile Kohler et Jacques Paris ainsi que des enseignants iraniens
dans le même dossier, l’union syndicale des enseignants iraniens publie un
communiqué en soutien à ces deux otages d’état et demande leur libération
sans délai, voici un extrait :

« l'année dernière Mme Cécile Kohler et sa compagne M. Jacques Paris,
ont eu une rencontre très amicale et informelle avec un groupe d'enseignants
lors d'un voyage touristique en Iran. Ces deux français qui s'étaient
rendus en Iran avec un visa touristique et avec l'autorisation du ministère
des Affaires étrangères ont été arrêtés et sont détenus dans le quartier
de sécurité 209 depuis cette date.

Dans un acte éhonté et illégal, la télé d’état, en collusion avec les
forces de sécurité, a publié les aveux forcés de ces deux enseignants
dans une émission télévisée.

Il est évident pour tout le monde que les aveux télévisés n'ont jamais
eu et n'auront jamais de validité légale. Les forces de sécurité à la hâte
et avant d'interroger les enseignants et ouvriers arrêtés, dans une
action illégale dans les tout premiers jours de l'arrestation, en réalisant
un clip, ont tenté d'attribuer les protestations des enseignants aux
deux enseignants français.

Au bout de six mois, ils tentent à nouveau avec un nouveau clip d'attribuer
le soulèvement populaire de septembre à la direction de ces deux enseignants.

Tout le monde sait que ces histoires sont l'œuvre des scénaristes des
institutions de sécurité et sont sans fondement...»
