
.. local in ["local", "list"]
.. images in ["images", "{year}/{week}/images"]




♀️✊ ⚖️ **Femme, Vie, Liberté** 26e rassemblement samedi 25 mars 2023 à Grenoble place Félix Poulat à 14h30 **A l'occasion du nouvel an iranien, arrêt des exécutions, annulation des condamnations à mort, libération des prisonniers politiques, contre les exécutions en Iran** 📣
=====================================================================================================================================================================================================================================================================================================================================================

- :ref:`iran_grenoble_2023_03_25`


2023-03-22 🇫🇷  **Sept hivers à Téhéran** À Annecy au Cinéma Nemours, salle comble également pour accueillir la réalisatrice Steffi Niederzoll ! 🔥👏
=====================================================================================================================================================

- https://twitter.com/nourfilms\_/status/1638468591870697478#m

Ce soir, on se voit à Marseille et Grenoble pour ce deuxième jour de
tournée ! Prenez vos places 🤩🎬

Ce film n’est pas seulement l’histoire de femme et le châtiment cruel de
la vengeance, c’est l’histoire d’une injustice généralisée, de procès-spectacles
et des aveux forcés qui met parfaitement en place la situation actuelle
d’Iran.

Dès le 29 mars 2023 dans les salles françaises.


2023-03-20 🇮🇷 40 BLESSÉS après des tirs des forces de sécurité lundi 20 mars 2023
===================================================================================

- http://nitter.smnz.de/arminarefi/status/1638231317862031365#m
- https://hengaw.net/en/news/2023/03/more-than-40-people-were-injured-by-direct-fire-from-the-iranian-government-forces-during-the-newroz-celebrations


URGENT. 40 BLESSÉS après des tirs des forces de sécurité lundi 20 mars
contre des manifestants qui célébraient le nouvel an perse dans les villes
de Mahabad, Bolbolanabad, Saghez et Sanandaj, selon @Hengaw_English.
