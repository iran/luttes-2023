.. index::
   pair: Réalisateur ; Dariush Mehrjui


.. _dariush_mehrjui_2023_10_17:

=============================================================================================
2023-10-17 **Grande figure du cinéma iranien, Dariush Mehrjui a été poignardé à mort**
=============================================================================================

:download:`Télécharger le fichier au format PDF <pdf/dariush_mehrjui_a_ete_poignarde_a_mort.pdf>`


Introduction
=============

Le réalisateur et son épouse ont été retrouvés morts à leur domicile de
Karaj.

Dans une vidéo, il avait fait savoir que des « assassins » rôdaient
autour d’eux.
Dans une récente vidéo, devenue virale sur les réseaux sociaux iraniens,
le cinéaste Dariush Mehrjui et son épouse, Vahideh Mohammadifar,
lançaient cet avertissement, en persan et en français::

    « Les assassins sont parmi nous. » Lui seul murmurait
    ensuite, et en persan seulement : « Soyez prudents. » Au
    début de la même vidéo, il avait aussi déclaré : « Vive la
    liberté ! »

Une mise en garde qu’il semblait s’adresser à lui-même en premier lieu.
S’agissait-il d’un pressentiment ou le couple avait-il reçu un avertissement ?
Quoi qu’il en soit, Vahideh Mohammadifar, 54 ans, et Dariush Mehrjui, 83
ans, ont été poignardés à mort dans la nuit de samedi à dimanche à leur
domicile de Karaj, une grande banlieue résidentielle à une quarantaine
de kilomètres à l’ouest de Téhéran.

Après ce double assassinat, qui a été découvert par la fille du couple
venue dîner chez ses parents, cinq personnes ont été arrêtées lundi,
selon un communiqué de la police iranienne.

Selon elle, les deux victimes ont reçu de multiples coups de couteau et
de bâton. Les mobiles d’un tel acte de cruauté ne sont pas connus.

Dans un entretien publié dimanche par le journal Etemaad, principal
quotidien réformateur, la femme du cinéaste avait déclaré avoir été
récemment menacée par un individu et que leur domicile avait été
cambriolé.

« L’enquête a montré qu’aucune plainte n’avait été déposée concernant
l’entrée illégale dans la villa de la famille Mehrjui et le vol de leurs
biens », a réagi Hossein Fazeli Harikandi, chef de la justice de la province
d’Alborz, où est située Karaj.

De son côté, le ministre de la culture, Mohammad Mehdi Esmaïli, a déclaré
dans un communiqué avoir demandé « des éclaircissements sur les circonstances
de cet événement triste et douloureux ».

Il a également rendu hommage à « l’un des pionniers du cinéma iranien »
et au « créateur d’œuvres éternelles ».

C’est en 1969 que Dariush Mehrjui, un cinéaste débutant et ancien étudiant
en philosophie à Los Angeles, se fait connaître à La Mostra de Venise en
présentant son film La Vache, qui demeure son long métrage le plus célèbre
et va l’imposer comme le chef de file de la nouvelle vague du jeune cinéma
iranien, à cette époque totalement méconnu en Europe – La Vache ne sortira en
France qu’en 2014, soit quarante-cinq ans après sa réalisation.

L’œuvre, qui raconte la vie des villageois iraniens, peut se lire comme
une fable teintée de néoréalisme ou une parabole mystique.

Elle a eu d’emblée des problèmes de censure avec le régime du chah.

Curieusement, il deviendra le film préféré de l’ayatollah Khomeiny, qui
estimera qu’il défend les valeurs traditionnelles de l’Iran.

Dès lors, il représentera un enjeu politique.

Pourchassé par la censure
============================

Mais cette période de grâce que le réalisateur va vivre sous la jeune
République islamique ne durera pas.

Bientôt, il sera confronté, comme quasiment tous les cinéastes iraniens,
à la censure.
À propos de celle-ci, il dira : « C’est le même principe [qu’à l’époque du chah –
ndlr], voyez-vous, dès que vous faites quelque chose qui est au-delà de
la compréhension de ceux qui nous gouvernent. Mais qui sont-ils, ceux qui gouvernement ? »

En 1978, Le Cycle, qui évoque le trafic de sang humain, sera interdit
par le tout nouveau ministère de l’orientation islamique.

Puis son film **L’école où nous allions** le contraint à un exil de
quelques années en France, où il tournera Voyage au pays de Rimbaud.

En 1986, sa nouvelle œuvre, Les Locataires, qui raconte l’histoire d’un
immeuble insalubre en train de s’effondrer mais dont les habitants ne
veulent pourtant pas s’occuper, apparaît comme la métaphore d’une
société sclérosée.

Suivront des films consacrés à de beaux portraits de femmes – La Dame,
Sara, Pari, Leila.

Pour lui, les femmes iraniennes sont devenues les mostazafin
(« les déshéritées »).

.. youtube:: 1Fcxe4Vv9TM

« Anéantissez-moi »
========================

Sorti en 2007, son dernier film, Santouri, ne sera projeté en salles
qu’une seule fois avant d’être interdit.

Mais des DVD du film circuleront, à la manière d’un samizdat, sous le
manteau. Il relate l’histoire d’un joueur de santour qui va de fête en
fête pour gagner sa vie et acheter de l’héroïne, dont il est dépendant.

Pour autant, Dariush Mehrjui ne sera jamais un opposant déclaré de la
République islamique.

Jusqu’à de récentes vidéos, dans lesquelles il déclarait ne plus pouvoir
supporter la censure. Dans l’une d’elles, casquette vissée sur la tête,
il s’insurgeait en criant contre le régime, qui retenait son film
La-Mi-Do (histoire d’une artiste confrontée à un père dictateur) :
« Vous n’êtes pas des hommes ! Pourquoi m’avez-vous laissé travailler
pendant deux ou trois ans pour, finalement, ne pas vouloir que le film
sorte. […] Vous prétendez que son temps de validation est dépassé mais
un film, ce n’est pas du lait ou de la viande. Et vous avez même jeté
l’un de mes films à la poubelle. Je vais faire un sit-in devant l’Ershad
[le ministère de l’orientation islamique – ndlr] et, tant que je n’aurai
pas mes droits, je ne lâcherai pas. »

Dans la même vidéo, il déclarait la guerre ouvertement au régime :
« Je vais me battre, allez, venez me battre, me tuer, anéantissez-moi. »

Dans une autre vidéo, bouleversante, on le découvre s’efforçant de
convaincre Vahideh Mohammadifar de ne plus porter le voile : « N’aie pas peur.
L’époque du hidjab est finie. — Si, j’ai peur. Cela fait quarante ans
que je le porte », répond-elle.

Puis, on voit son mari jeter son chapeau sur un arbre avec des yeux pour
symboliser la censure : « On nous a mis un chapeau sur la tête depuis
quarante ans. Je n’en veux plus », crie-t-il.

L’assassinat du cinéaste et de sa femme rappelle beaucoup celui de Dariush
Forouhar, l’un des principaux opposants iraniens, et de son épouse, qui
avaient été, eux aussi, poignardés à mort à leur domicile de Téhéran, le 22
novembre 1998, alors que l’Iran connaissait déjà une période d’intenses
tensions intérieures.
Arrêté, l’instigateur du double crime, le vice-ministre des renseignements
Saïd Emami, s’était, selon la version officielle, « suicidé » l’année
suivante dans la prison d’Evin avec… de la mousse à raser.

Pour le moment, rien n’incrimine le gouvernement iranien dans le double
assassinat de Karaj, et les opposants en exil restent prudents, préférant
mettre l’accent sur l’insécurité qui, selon eux, règne en Iran.

Peut-être pourrait-il s’agir alors de groupes ultras dans le cadre d’une
lutte de pouvoir interne.

Le cinéaste et scénariste Mani Haghighi, dans une interview à la BBC, a
fait savoir, faisant référence à l’assassinat des époux Forouhar, que la
piste du régime iranien ne pouvait être écartée.

En revanche, pour le grand photographe iranien Manoocher Deghati,
rencontré en France, « le modus operandi des tueurs ne permet pas de
douter que ce sont des gens du régime qui sont à l’origine de ces meurtres.

Non pas que Mehrjui représente un danger, mais il s’agit avant tout pour
eux de faire peur ».

::

    Jean-Pierre Perrin
