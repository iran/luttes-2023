.. index::
   pair: Maurtres; Armita Garavand

.. _armita_geravand_2023_10_28:

======================================================================================
2023-10-28 IHRNGO: Islamic Republic is Responsible for Armita Garavand’s Murder
======================================================================================

- https://iranhr.net/en/articles/6270/


Iran Human Rights (IHRNGO); October 28, 2023: State media have reported
the death of Armita Garavand, a 16-year-old girl who suffered brain
injury after a “violent encounter with metro officers enforcing the
mandatory hijab” on 1 October 2023.

She lost consciousness when she was knocked to the ground by officers
and has been kept in a coma for 28 days.

From day one, authorities attempted to conceal the truth by transferring
Armita to a military hospital, arresting journalists covering the story,
intense pressure on her family and controlling the narrative.

Armita’s friends and parents were also forced into giving television
interviews parroting the official narrative.

Reiterating the need for an independent investigation into events at
Shohada metro station that led to the hospitalisation of Armita Garavand
on 1 October 2023, Iran Human Rights rejects the Islamic Republic narrative
about her death.

Given the Islamic Republic’s history of covering up its crimes and
citizen reports, it considers her death to be state murder.

Director, Mahmood Amiry-Moghaddam said: “Ali Khamenei is personally
responsible for Armita Garavand’s death unless an independent
international investigation proves otherwise.

Islamic Republic authorities must also be held accountable for the pressure
and ill-treatment they have subjected her family to.”

16-year-old Armita Garavand lost consciousness at the Shohada metro
station in Tehran on the morning of 1 October and was transferred to
the Tehran Air Force’s Fajr Hospital where she was declared to be
in a coma.

On 4 October 2023, Iran Human Rights called for an independent investigation
into events that led to the hospitalisation of Armita Garavand.

On 7 October, the UN Fact-finding Mission stated that it would be
investigated as part of their mission.

There have been many reports of the pressure on Armita’s family since
her hospitalisation.

On 2 October, security forces arrested Maryam Lotfi, a journalist from
Shargh newspaper who went to the hospital to report on Armita’s condition.

She was released hours later. There have also been reports of security
threats made against the medical team at the hospital and the hospital
CCTV being cut off.

Armita Garavand being in a coma after physical assault by state forces
for violating mandatory hijab laws is reminiscent of Jina (Mahsa) Amini’s
state murder a year ago.

Jina was a 22-year-old Kurdish girl who was violently arrested by the
morality police on 13 September 2022 and died in their custody on
16 September.
Iran Human Rights called it a state murder and called on the United
Nations Human Rights Council (UNHRC) to force the Islamic Republic
authorities into accepting a UN fact-finding mission.

On 24 November 2022, the UNHRC adopted Resolution S35/1 to establish
an International Independent Fact-Finding Mission (FFMI) on the
Islamic Republic of Iran’s human rights violations since the start
of the “Woman-Life- Freedom” protests.

Iran Human Rights now calls for international community pressure for
Iran to allow an investigation team into the country to independently
investigate how Armita fell into a coma.

If not, they will cover up another crime and the perpetrators will
escape accountability again.



