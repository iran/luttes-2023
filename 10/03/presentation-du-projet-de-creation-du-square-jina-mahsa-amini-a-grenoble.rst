.. index::
   pair: Square ; Jina Mahsa Amini
   pair: Grenoble ; Square Jîna Mahsa Amini

.. _ref_square_jina_amini_2023_10_03:

====================================================================================================================
Mardi 3 octobre 2023 : **Présentation du projet de création du square Jîna Mahsa Amini à Grenoble** de 17 à 19h
====================================================================================================================

- :ref:`iran_luttes:square_jina_amini_2023_10_03`
