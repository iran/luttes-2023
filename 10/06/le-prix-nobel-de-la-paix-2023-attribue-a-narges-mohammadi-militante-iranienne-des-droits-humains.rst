.. index::
   pair: Prix Nobel de la Paix; Narges Mohammadi
   pair: Narges ; Mohammadi

.. _nobel_2023_10_06:

======================================================================================================================================
2023-10-06 **Le prix Nobel de la paix 2023 attribué à Narges Mohammadi pour son combat contre l’oppression des femmes en Iran**
======================================================================================================================================

Biographie
=============

- https://fr.wikipedia.org/wiki/Narges_Mohammadi

.. figure:: images/narges.png

**Narges Mohammadi** (en persan : نرگس محمدی), née le 21 avril 1972, est
une militante iranienne des droits humains et vice-présidente du Defenders
of Human Rights Center (en), dirigé par la lauréate du prix Nobel de la
paix Shirin Ebadi.

Pour son action en faveur des droits humains, elle est arrêtée et détenue
à plusieurs reprises depuis 1998.

Amnesty International proteste contre sa détention comme prisonnière
d'opinion.
À chaque fois, divers gouvernements, organismes internationaux, et
commissions législatives réclament sa libération.

En mai 2016, elle est encore condamnée à Téhéran, à 16 ans d'emprisonnement
pour avoir créé et dirigé « un mouvement de défense des droits de l'homme
qui milite pour l'abolition de la peine de mort ».

Elle est libérée en octobre 2020, mais emprisonnée de nouveau quelques
mois plus tard.

Alors qu'elle est en détention à la prison d'Evin, elle reçoit le
prix Nobel de la paix 2023.



Introduction
==============

Le prix Nobel de la paix a été attribué, vendredi 6 octobre 2023 à Oslo,
à la militante iranienne **Narges Mohammadi**, en détention depuis un an
à Téhéran.

Une distinction vivement critiquée par l’Iran et saluée par la communauté
internationale.

La militante et journaliste de 51 ans est récompensée « pour son combat
contre l’oppression des femmes en Iran et sa lutte pour la promotion des
droits humains et la liberté pour tous », a déclaré la présidente du
comité Nobel norvégien, Berit Reiss-Andersen.

« Cela distingue vraiment le courage et la détermination des femmes en
Iran, qui sont une source d’inspiration pour le monde entier », a déclaré
la porte-parole du Haut-Commissariat des Nations Unies aux droits de
l’homme, Elizabeth Throssell, à Genève, ajoutant : « Nous avons vu leur
courage et leur détermination face aux représailles, aux intimidations,
à la violence et aux détentions. »

L’ONU a, dans la foulée, demandé la libération de Narges Mohammadi,
et « celle de tous les défenseurs des droits humains emprisonnés en Iran ».

La journaliste avait été condamnée en mai 2016 à seize ans de prison
pour son activisme en faveur des droits humains, une peine allongée
en août.

Elle est vice-présidente de l’association Defenders of Human Rights Centre,
dirigé par l’avocate `Prix Nobel de la paix 2003 Shirin Ebadi <shirin_Ebadi_2023_03_07>`.


Shirin Ebadi, prix Nobel de la paix en 2003
============================================

- :ref:`shirin_Ebadi_2023_03_07`
