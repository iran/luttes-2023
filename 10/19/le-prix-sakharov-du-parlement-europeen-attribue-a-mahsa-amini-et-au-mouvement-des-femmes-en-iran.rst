.. index::
   pair: Prix Sakharov; 2023-10-19


.. _prox_sakharov_2023_10_19:

========================================================================================================
le-prix-sakharov-du-parlement-europeen-attribue-a-mahsa-amini-et-au-mouvement-des-femmes-en-iran
========================================================================================================

- https://www.lemonde.fr/international/article/2023/10/19/le-prix-sakharov-du-parlement-europeen-attribue-a-mahsa-amini-et-au-mouvement-des-femmes-en-iran_6195401_3210.html
- https://www.europarl.europa.eu/news/fr/press-room/20231013IPR07134/jina-mahsa-amini-et-les-femmes-iraniennes-laureates-du-prix-sakharov-2023


Mahsa Amini et le mouvement des femmes en Iran ont reçu jeudi 19 octobre
le prix Sakharov, du Parlement européen. La candidature de « Jina Mahsa Amini
et le mouvement Femme, Vie, Liberté en Iran », soutenue par les trois
principaux groupes politiques du Parlement, faisait figure de favorite
pour ce prix, plus haute distinction de l’Union européenne pour les droits humains.

Mahsa Amini, Kurde iranienne arrêtée par la police des mœurs pour avoir
prétendument enfreint les règles vestimentaires strictes imposées aux
femmes dans ce pays, est morte en détention le 16 septembre 2022.

La mort de cette femme d’une vingtaine d’années a déclenché un vaste
mouvement de contestation dans le pays, qui a causé la mort de plusieurs
centaines de personnes et au cours duquel des milliers d’autres ont été arrêtées.

Le prix Sakharov pour la liberté de l’esprit est décerné chaque année,
depuis 1988, à des individus et à des organisations défendant les droits
humains et les libertés fondamentales.

Il porte le nom du physicien et dissident soviétique Andreï Sakharov et
est doté d’une bourse de 50 000 euros.
