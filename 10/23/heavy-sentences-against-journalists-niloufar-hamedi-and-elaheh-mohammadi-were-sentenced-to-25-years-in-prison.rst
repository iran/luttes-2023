.. index::
   pair: Niloufar Hamedi ; 2023-10-23
   pair: Elaheh Mohammadi ; 2023-10-23

.. _niloufar_elaheh_iran_2023_10_23:

===================================================================================================================================
2023-10-23 **Heavy sentences against journalists; Niloufar Hamedi and Elaheh Mohammadi were sentenced to 25 years in prison**
===================================================================================================================================


- https://iran-hrm.com/2023/10/23/heavy-sentences-against-journalists-niloufar-hamedi-and-elaheh-mohammadi-were-sentenced-to-25-years-in-prison/
- :ref:`iran_luttes:viens_en_iran_2023_03_08`


Introduction
============


After 396 days of waiting, right on the birthday of Niloufar Hamedi, the
Revolutionary Court announced the verdict of the preliminary trial of
Niloufar Hamedi and Elaheh Mohammadi.

Elaheh Mohammadi, a journalist of Ham Mihan newspaper and Niloufar Hamidi,
a journalist of Sharq newspaper in Tehran, who both prepared a report on
the death of Jina Amini, were sentenced to 25 years in prison by the
Iranian judicial system.

The two political prisoners are currently in Evin prison.

Niloufar Hamedi
=================

According to the report received by the Iran Human Rights Monitor (IranHRM),
Branch 15 of the Tehran Revolutionary Court headed by Abolghasem Salavati,
Niloufar Hamedi was sentenced to 7 years in prison on the charge of
“collaboration with the hostile government of America.”

She was sentenced to five years in prison on the charge of “gathering
and colluding against the security of the country,” and sentenced to
one year in prison on the charge of propaganda against the Iranian regime.

In total, **Niloufar has been sentenced to 13 years in prison**.

Elaheh Mohammadi
==================

The other accused journalist, Elaheh Mohammadi, was also sentenced to
six years in prison on the charge of “collaboration with the hostile
government of America,” five years on the charge of “gathering and
colluding against the security of the country,” and one year on the
charge of propaganda against the Iranian regime.

**Elaheh Mohammadi has been sentenced to a total of 12 years in prison**.

According to the Iranian regime’s punishment law, the most severe punishment,
six years for Elaheh Mohammadi and seven years for Niloufer Hamedi, will
be applicable.

These two journalists were also sentenced to a two-year ban from
journalistic activities, as well as being banned from being a member
of parties, groups, political groups, and activities in cyberspace.

It should be mentioned that Niloufar Hamedi published pictures of
Jina Amini’s mother and father in the hospital, and Elaheh Mohammadi
was also present in the city of Saqqez and prepared and published a
report on the death of Jina Amini.
