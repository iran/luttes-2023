.. index::
   pair: Film ; No land’s song
   ! No land’s song

.. _no_lands_son_2023_11_10:

==================================================================================
|SaraNajafi| 2023-11-10 **Projection du film 'No land’s song'** à Claix
==================================================================================

- https://fr.wikipedia.org/wiki/No_Land%27s_Song


Introduction
================



A l'occasion du **Mois du film documentaire**, une projection d’un
réalisateur iranien, Ayat Najafi :  **No land’s song** est programmé
à Claix .

Ce sera le vendredi 10 novembre 2023 à 19h au Déclic (c'est la salle de
spectale de la Ville de Claix).

Le film dure 1h30.


Annonce par la médiathèque de Claix
======================================

- https://mediatheque.ville-claix.fr/cms/articleview/id/208

Film documentaire iranien d'Ayat Najafi, à partir de 12 ans.

En Iran, depuis la révolution de 1979, les femmes n'ont plus le droit de
chanter en public en tant que solistes.

Une jeune compositrice, Sara Najafi, avec l'aide de trois artistes
venues de France (Elise Caron, Jeanne Cherhal et Emel Mathlouthi), va
braver censure et tabous pour tenter d'organiser un concert de chanteuses solo.



La salle **le Déclic** 11ter chemin de Risset - 38640 Claix
================================================================

- https://www.ville-claix.fr/7015-le-declic.htm

.. raw:: html

   <iframe width="800" height="400" src="https://www.openstreetmap.org/export/embed.html?bbox=5.66820502281189%2C45.1073783834332%2C5.675286054611207%2C45.11058512074639&amp;layer=mapnik"
       style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=18/45.10898/5.67175">Afficher une carte plus grande</a></small>


Le film
===========

- https://fr.wikipedia.org/wiki/No_Land%27s_Song

No Land's Song (en persan : آواز بی‌سرزمین) est un film iranien d'Ayat Najafi,
sorti en 2014.

Synopsis
-------------

Selon les mollahs iraniens, la voix féminine est source de péché car elle
est susceptible, dans la loi islamique, de donner du plaisir aux hommes.

Il est donc interdit pour les femmes de chanter en solo en public, sauf s
i celui-ci est exclusivement féminin.
Les autorités tolèrent que les femmes chantent en public à condition
qu'elles aient un rôle musical secondaire et qu'elle soient impérativement
accompagnées de plusieurs hommes qui couvrent nettement leurs voix.

En revanche, elles sont autorisées à chanter dans la sphère privée.

**La jeune compositrice iranienne Sara Najafi, féministe active et sœur du
réalisateur a cependant un projet fou à Téhéran : organiser, peu avant
les élections présidentielles iraniennes de 2013, un concert de chant au
cours duquel des femmes chanteront en solo sur scène devant un public mixte**,
afin de renouer avec une tradition de chansons persanes qui existait et
avait beaucoup de succès avant l'arrivée de l'Ayatollah Khomeini et la
révolution islamique d'Iran en 1979.

Pour mener à bien son projet, Sara Najafi décide de tisser des liens avec
**les chanteuses françaises Jeanne Cherhal et Élise Caron**, ainsi que la
**chanteuse tunisienne Emel Mathlouthi, militante lors de la révolution
tunisienne de 2011**, au titre des échanges culturels internationaux.

Mais les tracasseries administratives et religieuses iraniennes sont de
grands freins au projet, et les patientes tentatives de négociations de
Sara avec les Mollahs se heurtent à un mur constant : en Iran,
l'administration islamique ne donne jamais d’explication à ses prises de
position et à ses refus.

Sara, encouragée par l'équipe de musiciens et de personnalités du cinéma
du film, n'hésite pourtant pas à braver les interdits, la censure et les
tabous grâce à son courage et **sa détermination à sortir les femmes
iraniennes de cette prison culturelle**.

|SaraNajafi| **L'actrice Sara Najafi**
========================================

- https://www.aufeminin.com/news-societe/sara-najafi-celle-qui-a-libere-la-voix-des-femmes-en-iran-s1775900.html

Il y a quelques années, cette chanteuse, musicienne et compositrice de
Téhéran a relevé le défi de faire chanter des femmes solistes face à un
public mixte, ce qui est parfaitement interdit en Iran.

Son projet et surtout, son parcours du combattant pour le faire passer
auprès des autorités, sont devenus un documentaire à voir absolument,
**No Land’s Song**.



.. figure:: images/sara_najafi.png
   :align: center

Le réalisateur Ayat Najafi
==============================

- https://fr.wikipedia.org/wiki/Ayat_Najafi

Ayat Najafi (Persan آیت نجفی ), né le 23 septembre 1973 à Téhéran, est un
réalisateur, documentariste, scénariste et producteur de films iranien.

Son film documentaire No Land's Song (2014) sur le parcours de chanteuses
en Iran, a reçu une vingtaine de prix lors de sa sortie en 2016.

Il est aussi connu pour les films Football Under Cover (2008) and
Nothing Has Ever Happened Here (2016).

Sa première réalisation Football Under Cover sur la vie de joueuses de
football en Iran a été primée à la Berlinale avec le prix Teddy Award en 2008.


Ressources
============

- :ref:`iran_exposition:qamar_vaziri`
