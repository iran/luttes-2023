

=================================================================================================================
2023-11-02 **Malgré la répression, l'Iran préside le forum social du Conseil des droits de l'homme de l'ONU**
=================================================================================================================

- https://www.tf1info.fr/international/malgre-la-repression-l-iran-preside-le-forum-social-du-conseil-des-droits-de-l-homme-de-l-onu-2274953.html

L'Iran préside le forum social du Conseil des droits de l'homme de
l'Organisation des Nations unies (ONU), qui se tient ces jeudi 2 et
vendredi 3 novembre à Genève (Suisse).

Un choix dénoncé par des parlementaires et des associations et qui fait
polémique, alors que la République islamique réprime les mouvements
contestataires dans le pays.

Les États-Unis ont annoncé qu'ils n'y participeraient pas.

Une situation paradoxale, voire absurde, qui choque les défenseurs des
droits humains.
En avril dernier, le Conseil des droits de l'homme des Nations unies
avait exprimé sa "profonde préoccupation face au nombre croissant
d'exécutions" en Iran, "y compris celles d'individus condamnés à mort
pour leur participation supposée aux récentes manifestations" survenues
après la mort de Mahsa Amini.

Le Conseil avait également déploré "la discrimination systématique et
la violence basées sur le genre, l'origine ethnique, la religion ou
les opinions politiques" en Iran.

Mais ces 2 et 3 novembre à Genève (Suisse), le forum social de ce même
Conseil des droits de l'homme de l'ONU sera présidé par Ali Bahreini,
ambassadeur de l'Iran aux Nations unies.


De quoi s'agit-il ?
========================

Le forum social est une réunion annuelle organisée par le Conseil des
droits de l’homme.
"Il s’agit d’un événement unique favorisant un dialogue ouvert interactif
entre la société civile, les représentants des États Membres et les
organisations intergouvernementales sur un thème choisi chaque année
par le Conseil", lit-on sur le site de l'ONU.

Cette année, ils discuteront de "la contribution de la science, de la
technologie et de l'innovation à la promotion des droits de l'homme, y
compris dans le contexte de la reprise après la pandémie".


Une présidence "irresponsable" et "choquante"
====================================================

La présidence de l'Iran à cette réunion, décidée depuis de nombreux mois
déjà, fait polémique en France et ailleurs.

La vice-présidente de l'Assemblée nationale et députée socialiste
Valérie Rabault a dénoncé sur X (ex-Twitter) une présidence "irresponsable"
face à laquelle elle avait alerté le gouvernement le 30 mai 2023 lors
d'une séance de Questions au gouvernement. "Le 10 mai dernier, le président
du Conseil des droits de l'homme de l'ONU [a désigné] l'Iran pour présider
le prochain forum social du Conseil des droits de l'homme des Nations unies.

C'est un outrage aux victimes, aux morts, aux prisonniers de la liberté.

Aussi, je vous demande une chose, faire entendre la voix de la France
pour annuler cette nomination", avait-elle déclaré, sans obtenir de
réponse de l'exécutif.

"Cette présidence est indigne, scandaleuse et discrédite ce forum social
du conseil des droits de l'homme de l'ONU", a abondé le député socialiste
Gérard Leseul.

Le député Renaissance Hadrien Ghomi a dénoncé une présidence "choquant[e]
et inadmissible".

Du côté des associations, la Licra a regretté que "parfois, la réalité
dépasse la fiction".

