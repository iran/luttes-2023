.. index::
   pair: SHALKOOI Shirin; Analyse


.. _shalkooi_shirin_2023_09_15:

=================================================================================================================================================================================================
2023-09-15 **Un an après le meurtre de Jina Masha Amini et le soulèvement Femme, Vie, Liberté, un point de vue depuis le mouvement féministe de solidarité à Bruxelles** par SHALKOOI Shirin
=================================================================================================================================================================================================

- https://www.gaucheanticapitaliste.org/un-an-apres-le-meurtre-de-jina-mahsa-amini-et-le-soulevement-femme-vie-liberte/
- https://www.europe-solidaire.org/spip.php?article67942
- https://www.europe-solidaire.org/spip.php?auteur26218


Introduction
==============

Le samedi 16 septembre 2023 marquera les un an du meurtre de Jina Mahsa
Amini et du soulèvement populaire en Iran et au Kurdistan connu sous le
nom de « Femme, vie, liberté », slogan historique du mouvement de libération
kurde « Jin, Jiyan, Azadi ».

Ce soulèvement a jailli dans un contexte précis :

- Depuis 2017, on peut constater une accélération et une intensification
  de la contestation sociale.
  La population fait face à une inflation terrible du prix des aliments
  de base, les arriérés de salaire peuvent parfois s’étendre à une année.
  La moitié de la population vit sous le seuil de pauvreté alors que le
  pays est la quatrième réserve mondiale de pétrole et la deuxième de
  gaz au monde.
- Le régime consacre l’essentiel de son budget aux dépenses militaires
  et à l’enrichissement d’une oligarchie.
- Il durcit la répression envers les femmes et les personnes LGBTI+.
  Il persécute les minorités nationales (kurdes, baloutches, arabes, …),
  immigrées (afghan·e·s) et religieuses (bahaïs, athé·e·s…).
- Les politiques d’extractivisme pétrolier, de déforestation et la gestion
  désastreuse de l’acheminement en eau aggravent la crise climatique,
  ce qui a un impact sur la santé et rend certaines zones inhabitables.


Point de non-retour
========================

Ce n’est pas la première fois que les femmes prennent la rue, ni que la
population se soulève.
Mais le soulèvement de septembre 2022 est plus fédérateur que tous les précédents.

Son caractère massif et révolutionnaire marque une rupture, une crise de
légitimité profonde, non seulement du gouvernement dirigé par le président
Raïssi mais de l’État islamique dans son ensemble.
Lors du mouvement vert en 2009 contre la réélection de Mahmoud Ahmadinejad,
une frange importante du mouvement défendait la possibilité de réformer
l’État.
Actuellement, la population a abandonné tout espoir en ce sens et s’attaque
directement à la tête de l’État, le guide suprême : à bas le dictateur,
à bas Khamenei !

Mettre fin au régime est devenu une revendication centrale portée d’abord
dans les régions du Kurdistan (1) et du Sistan-Balouchistan (2) pour
s’étendre à l’ensemble du pays.

En quelques jours et pendant environ six mois, des milliers de personnes
sont descendues dans les rues pour s’opposer aux meurtres du régime,
clamer leur soif de liberté et de meilleures conditions de vie.

Les femmes, les filles et les personnes LGBTI+ ont pris une place centrale
dans le mouvement.

Les actions se sont aussi multipliées dans les écoles et les universités.

Il y a eu une multiplication de grèves dans différents secteurs comme la
pétrochimie, l’enseignement, les bazars, les raffineries.
Il y a aussi eu plusieurs grèves générales dans l’ensemble du pays et
dans certaines villes.
Le régime est fragilisé économiquement mais pas encore suffisamment.
C’est surtout sa légitimité qui a volé en éclat.

Un soulèvement révolutionnaire est un temps de prise de conscience populaire
et d’apprentissages massifs et accélérés de techniques de lutte.

A Oshnavieh, les manifestant·e·s ont pris quelques heures le pouvoir dans
la ville et ont fait fuir la police.
Des écolières ont chassé la direction de leur école.
Des étudiant·e·s ont cassé les barrières séparant la partie hommes et
femmes dans les cantines d’universités.

Surtout, malgré la répression, le soulèvement ouvre un espace d’expression
et de prise de confiance dans ses propres capacités et celles de la
collectivité.
Sans faire disparaître la peur, il brise au moins pour un instant la
généralisation de l’effroi, du sentiment d’impuissance et de l’attentisme.

Dans la douleur, un champ d’espoir et de créativité s’ouvre comme le montre
notamment la nouvelle production de nombreux chants révolutionnaires.
Enfin, il force la population à s’auto-organiser pour renverser le régime
et l’encourage à se poser des questions essentielles de stratégie de
lutte révolutionnaire. (3)

Malgré le reflux des manifestations dans la rue après les six premiers
mois de soulèvement, rien ne sera plus jamais comme avant.
En témoignent notamment les nombreuses femmes et filles qui refusent
encore aujourd’hui, au risque de leur vie, de se soumettre à l’obligation
de porter le hijab.
Le régime n’est pas normalisé aux yeux de la population et n’a regagné
aucune légitimité.

Réponse du régime
=====================

Dans un premier temps, le régime a compté sur le fait que le mouvement
se tasse.
Supposant par ailleurs que, à part un soutien symbolique, très peu de
soutien international concret serait apporté à la population. Il a coupé
internet et empêché l’accès aux principaux réseaux sociaux.
En octobre 2019, 1500 personnes ont été tuées en un mois.
Cette fois-ci, on dénombre moins de personnes tuées (au moins 550) (4)
mais des milliers d’emprisonnements (au moins 22.000) (5), la contrainte
à des aveux forcés et l’usage de la torture, des condamnations aux travaux
forcés, des mutilations (notamment des éborgnements), des empoisonnements
de filles dans leurs écoles (28 provinces) et des exécutions (au moins 26). (6)

Des étudiant·e·s sont exclu·e·s des cours et contraint·e·s de poursuivre
leurs études à des centaines de kilomètres de leur université.

La machine utilisée par le régime est directement commandée par le Guide
Suprême.
Elle est constituée de quatre principales forces :

- le ministère des renseignements (surveillance et espionnage),
- la police (dont la fameuse **police des mœurs** nouvellement baptisée
  **police des vêtements inhabituels**),
- le corps des gardiens de la révolution (Pasdaran/IRGC)
- et les bassidjis (paramilitaires).

Les « bon·ne·s musulman·e·s » aux yeux du régime sont aussi encouragé·e·s à
« faire le bien et empêcher ce qui est mal », un devoir inscrit dans la
Charia.
Concrètement, les civil·e·s sont utilisé·e·s pour surveiller, réprimer
ou dénoncer des opposant·e·s ou actes jugés délictueux.

Dans les régions du Kurdistan et du Sistan-Balouchistan, l’armée est
déployée de façon permanente et les massacres sont nombreux.
A la veille de l’anniversaire du meurtre de Jina, on constate la
démultiplication des dispositifs de répression dans le Kurdistan.

La justice est, elle aussi, directement sous le contrôle du Guide Suprême.
En plus de la répression physique, c’est une véritable guerre psychologique
qui est menée.
Cette brutalité sans nom est à la fois terrorisante et à la fois, grâce
aux mobilisations collectives, source d’un rejet toujours plus affirmé
du régime.

La réaction du régime a aussi été idéologique. Le Guide suprême Khamenei
et les différentes institutions ont asséné le discours qu’ils tiennent
depuis toujours pour diviser la population. Un discours de dénigrement
des pauvres, des minorités nationales, des femmes et des opposant·e·s
accusé·e·s d’être des agents de l’Occident, des ennemis de l’Islam, des
êtres manipulés, dépourvus de raison ou des rebuts de la société, des
paresseux et des trafiquants.
Les motifs de condamnation à la peine de mort attestent de cette lutte
idéologique.
Vingt-six personnes exécutées ont été pendues ou jetées dans le vide au
motif d’être « entrées en guerre contre Dieu » et d’avoir « répandu la
corruption sur terre ».

Jusqu’à ce jour, le régime n’a accédé, même partiellement, à aucune des
principales revendications du mouvement : la fin du port obligatoire du
hijab, de l’apartheid et de la domination de genre, le droit à la vie
des personnes LGBTI+, l’arrêt des exécutions et la libération de tou·te·s
les prisonnier·e·s politiques, le droit à l’autodétermination des minorités
nationales, la liberté d’organisation politique et syndicale et
l’amélioration économique des conditions de vie.

La répression et le refus de céder aux revendications génèrent des tensions
au sein de l’État.
Le 3 janvier 2023, les hauts-chefs du corps des Gardiens de la révolution
font état à Khamenei de défections au sein de leurs troupes.
De petites brèches sont ouvertes dans l’appareil militaire et politique
par le mouvement. Mais rien qui ne fasse encore vaciller le pouvoir.

Défis dans le mouvement de solidarité à bruxelles
=======================================================

En dehors de l’Iran, le soulèvement a eu des retentissements internationaux.
Le soulèvement a amené des exilé·e·s, des étudiant·e·s et des descendant·e·s
des deux premières générations de migration à se rassembler.

Des mouvements de solidarité organisés par des individus et des organisations
politiques de la diaspora se sont formés à différents endroits du monde.
En particulier en Europe, au Canada et aux USA.

Le soulèvement a aussi rencontré la solidarité de mouvements et collectifs
féministes de différentes tendances politiques.
Le mouvement « femme, vie, liberté ! », grâce à son caractère massif,
sa capacité à intégrer de multiples revendications et sa perspective de
transformation radicale de la société, entre en écho avec ce qu’on peut
qualifier de « quatrième vague » du féminisme dans les pays du Nord Global.

Mais **il a aussi rencontré la solidarité de forces féministes libérales
et de droite qui décident de ne voir dans le soulèvement que la lutte
de femmes contre le voile voire contre l’islam.**
**Ces tendances vont souvent jusqu’à instrumentaliser la lutte des femmes
iraniennes pour défendre la suprématie des soi-disant « valeurs occidentales »
et justifier des politiques racistes et discriminatoires envers les femmes
musulmanes qui portent le foulard et envers les hommes immigrés**.

Au niveau médiatique, les femmes iraniennes sont érigées en symbole de
la femme tantôt soumise, tantôt qui refuse de se soumettre.
Cette vaste couverture médiatique cliché contraste avec la faiblesse du
partage d’informations sur les aspirations et modalités de lutte en Iran.

En Belgique, où il n’existe pas une grande diaspora iranienne, chacun·e
vivait sa vie dans son coin.
La critique publique du régime était très limitée.
Les quatre manifestations nationales de solidarité à Bruxelles ont rassemblé
en moyenne 2 000 personnes chacune.
Des participant·e·s et organisatrices ont pris la décision consciente de
construire un mouvement de solidarité politique et culturel à visage
découvert et de renoncer, pour celles et ceux qui le pouvaient encore, à
la possibilité de rendre visite à leurs familles et ami·e·s en Iran tant
que le régime sera toujours en place.
Des décisions encore inimaginables la veille du soulèvement.

L’opposition au régime iranien en exil n’est pas plus politiquement
homogène en Belgique.
Elle charrie aussi différentes tendances politiques de droite et de gauche,
elles-mêmes constituées de différents groupes avec différents programmes
et stratégies, comme les royalistes, les nationalistes perses, les kurdes,
les communistes, les fedayins et les Moudjahedins du peuple.

Dans le mouvement actuel, on a vu émerger une tendance libérale s’auto-proclamant
« apolitique » mais soutenant des sanctions sévères de la part des gouvernements
occidentaux et des institutions européennes.

On a aussi vu la création de deux nouveaux collectifs : « Women, life, freedom – Gent »
et « Women, life, freedom – Belgium » (7).
Ce dernier a été à l’initiative de nombreuses actions de sensibilisation
et de solidarité « par en bas ». (8)
Il a aussi relayé les principales revendications ayant émergé dans le
soulèvement en Iran et tenté de nouer des solidarités sur ces bases
avec des organisations progressistes en Belgique.
Le mouvement a aussi mobilisé de nombreuses personnes non-organisées et
n’ayant pas pris une part active dans l’organisation d’actions de solidarité.

**Derrière des proclamations de solidarité de façade, quelques mèches de
cheveux coupées et le portrait de Jina Mahsa Amini brandi, les partis
au pouvoir au gouvernement fédéral (libéraux, verts, socialistes et
chrétiens-démocrates flamands) n’ont pas cessé de normaliser leurs
relations avec l’État iranien**.

En témoigne notamment l’invitation et l’octroi de visas à une délégation
de 15 représentants du régime iranien dont le maire de Téhéran (connu pour
sa répression sanglante de la contestation) au Brussels Urban Summit en
juin dernier, qui valut à Pascal Smet de démissionner.

Au même moment, l’État belge a continué de conditionner l’octroi du statut
de réfugié, de mettre en détention en centre fermé et d’expulser de force
des exilé·e·s iranien·ne·s du territoire. (9)

**Malgré les appels internationaux, l’ambassade iranienne n’a pas été fermée**.

Lors d’une manifestation à Bruxelles, le consulat a même été barricadé
par les forces de police belges.
Il est également toujours possible pour les entreprises et grandes fortunes
belges de continuer leurs investissements en Iran, comme ceux de la famille
Spoelberch (10) dans des entreprises technologiques du régime iranien.

Le gouvernement a également fait le jeu de la « diplomatie des otages »
de l’État iranien en libérant un agent des renseignements condamné à 20 ans
de prison pour tentative d’attentat, Asadollah Assadi, contre Olivier Vandecasteele.
Un accord qui s’est fait sur le dos des milliers de détenu·e·s dans les
prisons iraniennes et des groupes d’opposants iranien·ne·s au régime en exil. (11)

Notons en passant qu’Amnesty international, acteur important de la campagne
de libération de Vandecasteele mais invisible aux quatre grandes manifestations
nationales de solidarité, a clairement opté pour une stratégie centrée
sur un individu et indifférente à la conjoncture politique.

Au niveau régional, le soulèvement iranien a particulièrement rencontré
l’intérêt de la N-VA, parti nationaliste flamand au pouvoir en région
flamande et dans l’opposition au niveau fédéral.
Ce parti mène un combat contre les forces syndicales, féministes et antiracistes
et défend notamment le désinvestissement dans les services publics, la
baisse des salaires, la limitation de l’accès à l’avortement, l’emprisonnement
et l’expulsion des personnes migrantes.
Le 3 décembre 2022, Theo Francken et Darya Safai, tou·te·s deux député·e·s
N-VA au parlement fédéral, participaient à un meeting avec le « front du 7 Aban »,
une coalition d’iranien·ne·s nationalistes perses.

Darya Safai, belgo-iranienne, se prétend militante pour l’émancipation
des femmes.
En réalité, son implication dans le mouvement de solidarité a renforcé
les tendances politiques nationalistes soutenant l’intervention des pays é
trangers et opposées à l’auto-détermination des kurdes et aux droits des
personnes LGBTI+.

Comme pour l’Irak ou la Syrie, les gouvernements belges et européens n’ont
absolument aucun intérêt à ce que des forces politiques démocratiques et
sociales émergent en Iran.
Par contre, ils pourraient avoir un intérêt à un changement de régime si
une nouvelle direction politique favorable aux intérêts économiques et
politiques occidentaux se dessine.
Par exemple, un bloc de droite, libéral et autoritaire qui se détournerait
de la Russie et de la Chine. Ce n’est pas encore le cas.

Malgré au moins une tentative avec la formation de « L’alliance pour la
liberté et la démocratie en Iran » en janvier 2023, composée notamment
de Reza Pahlavi (fils du dernier Shah d’Iran), Masih Alinejad (journaliste
américaine), Shirin Ebadi (avocate), Hamed Esmaeilion (ancien porte-parole
de l’association des familles des victimes du vol PS752) et Abdullah Mohtadi
(secrétaire générale du parti Komala du kurdistan iranien).

Cette alliance sans principe et prête à s’allier à l’impérialisme sans
fournir de garantie ni sociale ni démocratique, a rencontré de nombreuses
critiques et semble, pour le moment, s’être soldée par un échec.

Mais, c’est au moment le plus fort de celle-ci que Alireza Akhundi
(député Suédois, Centerparteit) et Darya Safai ont appelé à une grande
manifestation internationale devant le Conseil de l’Europe à Bruxelles
le 20 février 2023.
L’appel soutenait une revendication centrale : l’ajout de l’ensemble
du Corps des Gardiens de la Révolution sur la liste des organisations
terroristes du Conseil de l’Europe.

Malgré toutes les divergences politiques mises en évidence, dès le premier
rassemblement en septembre 2022, la police a déclaré qu’elle ne donnerait
qu’une seule autorisation par mois à des manifestations « d’iranien·ne·s »
en se justifiant avec paternalisme et non sans hypocrisie : « si vous voulez
renverser ce régime, il faut que vous soyez capable de manifester ensemble ».

Dans les faits, cette position a contribué à renforcer la position dominante
de l’aile droite, mieux dotée en moyens, en marginalisant des forces
politiques progressistes de l’opposition et en restreignant leurs
possibilités d’expression et leur visibilité.

Quelles perspectives pour le futur ?
=======================================

Ces dernières semaines, le régime iranien a redoublé de répression pour
empêcher une nouvelle vague protestataire.
Pour ne pas céder au désespoir, il est important de donner un sens au
soulèvement et aux mobilisations dans lesquelles la population a fait
un « pas en avant » majeur vers la victoire.

Dans ce pas en avant, des forces politiques capables de soutenir
l’auto-organisation  et de créer la possibilité de convergence doivent
encore émerger et être créées.
La population gagne en expérience sur les stratégies d’autoprotection et
de survivance face à la répression.

Les forces progressistes en Iran et en exil ont un rôle-clé à jouer dans
la création d’espaces d’auto-formation, d’analyse stratégique et
d’auto-organisation.
S’il y a une leçon que nous pouvons retenir de toutes les révolutions de
l’histoire, c’est qu’on ne peut pas séparer le renversement du régime de
la question de l’organisation sociale et démocratique, du projet de
société à construire.

La nature des coalitions qui permettront le renversement du régime colorera
l’avenir social et politique du pays.
Malgré l’épuisement physique et psychique, de nombreux révolutionnaires
perçoivent un enjeu stratégique essentiel : renforcer les alliances entre
les ouvrier·ère·s, les étudiant·e·s, les familles en deuil, les minorités
nationales et les groupes de femmes.

Une seule certitude, de nouvelles crises et soulèvements ne pourront
qu’advenir.
S’organiser, c’est se donner les moyens de mieux s’y préparer et de contribuer
ici à soutenir le rapport de forces là-bas.

Quelles sont les tâches du mouvement de solidarité en dehors du pays ?
===============================================================================

- Combattre toute tentative d’imposition d’une alternative politique au
  régime venant de l’extérieur du pays.
- Révéler la duplicité et la dangerosité des gouvernements occidentaux
  et de celles et ceux qui se proclament comme figures de la diaspora
  iranienne.
- Renforcer les sanctions internationales sur les hauts dirigeants des
  Gardiens de la Révolution et de l’État islamique : boycott, gel des
  avoirs et des comptes bancaires.
- Exiger la levée du secret bancaire et commercial en Belgique et en
  Europe pour bloquer les avoirs des dirigeants du régime et arrêter
  tout partenariat économique.
- Établir des coalitions nationales et internationales solides avec les
  forces syndicales, féministes, antiracistes, socialistes pour construire
  des liens de solidarité « par en bas ».
  Par exemple, pour la libération des tou·te·s les prisonnier·e·s politiques
  et de droit commun.
- Favoriser l’auto-formation sur les expériences révolutionnaires passées
  et contemporaines. Par exemple : la Syrie et le Soudan.

C’est dans ce sens qu’une coordination internationale de groupes et de
collectifs féministes de la diaspora iranienne appelle à une action féministe
commune de solidarité avec le mouvement révolutionnaire ce vendredi 15/09/2023.

Pour réclamer le droit à l’émancipation et la nécessité de continuer
le combat « femme, vie, liberté » !

La Gauche anticapitaliste sera présente au rassemblement organisé à
Bruxelles pour marquer son soutien.
Rendez-vous à 16h30 devant le Palais de Justice, Place Poelaert.

Notes
=======

1. Jina Mahsa Amini venait de cette région du nord-ouest.
2. Région du sud-est, la plus pauvre du pays.
3. La charte des demandes minimales de collectifs et syndicats indépendants
   publiée le 15 février 2023 témoigne de ces questionnements.
   Pour plus d’information, lire l’article de Niloofar Golkar
   https://socialistproject.ca/2023/03/workers-organizations-woman-life-freedom/

4. Chiffre datant d’avril 2023.
5. Chiffre datant de mars 2023.
6. Chiffre datant de juillet 2023 et basé sur des décisions condamnant
   explicitement la participation à la contestation.
   Le nombre d’exécutions sur base d’autres motifs (par exemple, le trafic
   de drogue) a aussi significativement augmenté par rapport à 2021.
   L’accusation de participer au trafic de drogue est un motif central
   des exécutions dans le Sistan-Balouchistan.
   Ainsi, le régime masque le vrai motif d’exécution des opposant·e·s.

7. Sur Instagram @womanlifefreedom.be et sur Telegram @zendegiazadicol
8. Solidarité internationale entre les populations opprimées et exploitées.
   Par opposition à la solidarité « par en haut » qui compte sur les
   gouvernements étrangers et l’imposition de solutions et d’alternatives
   politiques à la population.

9. Article dans Le Soir : « Vive émotion autour de l’expulsion de Belgique
   d’une iranienne de vingt an », https://www.lesoir.be/467211/article/2022-09-23/vive-emotion-autour-de-lexpulsion-de-belgique-dune-iranienne-de-vingt-ans, 23-09-2022.
   Article dans Le Vif, « La Belgique tente d’expulser trois iraniens »,
   https://www.levif.be/belgique/la-belgique-tente-dexpulser-3-iraniens-sils-rentrent-en-iran-ils-seront-tues/,
   23-01-23.

10. https://www.lecho.be/entreprises/private-equity/les-spoelberch-en-iran-des-investissements-sensibles/10446639.html

11. https://www.rtbf.be/article/la-liberation-dolivier-vandecasteele-en-echange-dun-prisonnier-iranien-cree-t-elle-un-precedent-11204107

P.-S.
======

- https://www.gaucheanticapitaliste.org/un-an-apres-le-meurtre-de-jina-mahsa-amini-et-le-soulevement-femme-vie-liberte/
