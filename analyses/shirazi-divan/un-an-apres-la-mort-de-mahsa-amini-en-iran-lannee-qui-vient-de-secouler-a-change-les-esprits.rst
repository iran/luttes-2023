
.. index::
   pair: Shirazi Divan ; L’année qui vient de s’écouler a changé les esprits

.. _shirazi_divan_2023_09_15:

==========================================================================================================================
**Un an après la mort de Mahsa Amini en Iran : «L’année qui vient de s’écouler a changé les esprits»** par Shirazi Divan
==========================================================================================================================

- https://www.europe-solidaire.org/spip.php?article67950
- https://www.liberation.fr/international/un-an-apres-la-mort-de-mahsa-amini-en-iran-lannee-qui-vient-de-secouler-a-change-les-esprits-20230915_PBIOVILDJZG33ODG2HC3Z3PPJU/


Introduction
===============

Un an après la mort de la jeune femme, tuée par la police des mœurs, et
le début d’une révolte inédite, les Iraniens oscillent entre espoir et
résignation dans la lutte contre le régime islamique.

Il y a quelques jours, Tahereh (1) a eu un geste fou. « Ces dernières
semaines, la présence constante et renforcée de la police antiémeute
aux quatre coins de la ville est devenue vraiment oppressante et angoissante,
raconte cette enseignante de 52 ans, mère de deux enfants, qui vit à Téhéran.

Alors un jour, j’ai décidé d’offrir des fleurs à ces officiers, en signe
de paix. Ils ont pris ce geste comme une provocation. J’ai passé deux
jours en détention. »

A l’approche du 16 septembre, jour du premier anniversaire de la mort,
à 22 ans, de Mahsa Jina Amini, tombée sous les coups de la police des
mœurs qui lui reprochait un foulard mal ajusté, le régime de la République
islamique d’Iran a renforcé la répression. Une nouvelle vague d’arrestations
qui lance à la nation un message glacial.

L’objectif des autorités est limpide : instiller la terreur pour que les
Iraniens restent chez eux et ainsi étouffer toute protestation.
Les manifestations de masse dans tout l’Iran sont, sans aucun doute, le
plus formidable défi à la République islamique en quarante ans de règne.

Le scénario se répète, encore et encore
===========================================

**Mehdi Yarrahi est un chanteur pop célèbre**.

Depuis le début, il a été un ardent soutien des opposants.
Ses deux chansons Soroode Zan (« Hymne à la femme ») et Soroode Zendegi
(« Hymne à la vie ») sont devenues des cris de ralliement, murmurés
dans les espaces privés et repris en chœur lors de rassemblements
publics.

Jusqu’à ces derniers jours, il n’avait pas été inquiété.

Et puis, il a sorti sa dernière chanson Roosarito (« Ton foulard », en farsi),
en hommage « aux nobles femmes de ma patrie, qui se sont courageusement
tenues au premier plan du mouvement Femme, vie, liberté ».

Les premiers mots sonnaient comme un défi : « Enlève ton foulard, le soleil
se couche. N’aie pas peur mon amour, ris face au chagrin. »

Le 28 août au matin, Mehdi Yarrahi a été arrêté. Dans un message poignant
posté sur X (anciennement Twitter), juste avant son arrestation, il a
imploré : « Soyons la voix d’Izeh et continuons à célébrer l’anniversaire
du meurtre de Mahsa Amini. Pour Femme, vie, liberté. »

Izeh, ville du sud iranien, est devenue emblématique de la résistance.

C’est là que se sont concentrées l’an dernier d’innombrables manifestations,
parfois mortelles. Récemment encore, deux opposants ont été assassinés et
les membres de famille de victimes ont été arrêtées.

Izeh n’est pas un cas isolé. Onze femmes activistes ont été emprisonnées
récemment dans la province de Gilan, dans le nord du pays.
Le 7 janvier, Mohammad Mehdi Karami, 22 ans, champion d’Iran de karaté,
était pendu, accusé d’avoir tué un policier lors d’une manifestation.
Ces derniers jours, son père, qui porte le même nom, a partagé sur les
réseaux sociaux des photos de repas que lui et sa femme avaient préparé
et offert aux nécessiteux, en mémoire de leur fils. Il a été interpellé.

Aujourd’hui, les manifestations de masse semblent éteintes.
Derrière les portes, entre deux chuchotements, les jeunes restent pourtant
persuadés que la mobilisation peut renaître à n’importe quel moment,
qu’il suffit d’une petite étincelle.

La colère est toujours là, exacerbée aussi par une situation économique
de plus en plus tendue.

« On ne peut pas revenir à la situation d’avant la disparition de Mahsa.
L’année qui vient de s’écouler a changé les esprits, dessiné la vision
de ce que pourrait être l’Iran sans la domination de la République
islamique », estime Mobina, une jeune tatoueuse qui travaille clandestinement,
les tatouages étant interdits.

Sur son avant-bras, elle a inscrit : « Femme, vie, liberté. »

Elle non plus n’est pas sûre que les gens descendront dans la rue pour
commémorer Mahsa Amini.
Mais « toutes les nuits, je tague les murs dans les rues pour appeler à
la révolte.

Le matin, les graffitis sont souvent recouverts par les partisans du
régime, mais je persiste et j’y retourne pour recopier le message ».

« Aspiration naïve »
=========================

Les mobilisations de masse ont attiré beaucoup d’adolescents.
Certains ont perdu la vie, comme les lycéens Nika Shakarami, Abolfazl
Adinezadeh, Kumar Daroftade et Sarina Esmailzadeh.
Farbod, 17 ans, vit dans une petite ville côtière du sud du pays.
Tout en préparant ses examens d’entrée à l’université, il anime une page
Instagram avec 40 000 abonnés, qui avait initialement pour objet de
motiver les étudiants.

« Mais après le meurtre de Mahsa, je me suis concentré exclusivement sur
la mise en lumière des conditions de vie en Iran et la mobilisation de
l’opposition au gouvernement, explique-t-il.
Ces derniers jours, je n’ai posté qu’à propos des commémorations de la
mort de Mahsa, et la réponse immense de mes abonnés suggère que l’envie
est encore là et qu’il pourrait se passer quelque chose de significatif
le 16 septembre. »

Tous ne partagent pas cet espoir
=======================================

- Khosro, 31 ans, programmeur pour une start-up, est plus sceptique.
  « Je n’anticipe rien d’exceptionnel, nous serons sans doute, une fois
  de plus, déçus. La République islamique a démontré qu’elle ne reculera
  devant aucun moyen pour faire taire les voix rebelles », explique-t-il.
- Mina, médecin de 44 ans, partage le même découragement.
  Elle a joué un rôle discret mais vital pendant les manifestations à
  Téhéran et Karaj, la quatrième ville du pays.
  En secret, elle s’est occupée de soigner les manifestants frappés par
  les forces de l’ordre, et ils étaient nombreux. « Je doute que la population
  se mobilise massivement dans les prochains jours.
  Placer son espoir dans ces mouvements me semble futile. Il est temps
  d’accepter que le règne de la République islamique va se poursuivre »,
  ajoute-t-elle.
  Mina a décidé de quitter l’Iran. Pour elle, « un changement réel de
  régime ou une révolution nécessite un leader.
  L’idée d’une révolution démocratique sans dirigeant identifié et unique
  pouvait sembler attirante initialement, mais il est désormais évident
  qu’il s’agissait d’une aspiration naïve ».

La question de la diaspora
===============================

La répression a définitivement étouffé l’émergence d’une opposition
robuste à l’intérieur de l’Iran.

Et, à l’extérieur, les multiples courants d’opposition ont suscité des
réactions mitigées.

En un an, huit personnalités notables se sont détachées :

- Reza Pahlavi, le fils du dernier chah d’Iran,
- Hamed Esmaeilion, écrivain et dentiste dont la femme et la fille ont
  péri dans le crash de l’avion ukrainien abattu par des missiles iraniens
  en janvier 2020,
- Masih Alinejad, journaliste,
- Abdullah Mohtadi, secrétaire général du parti Komala dans le Kurdistan iranien,
- Shirin Ebadi, avocate des droits de l’homme et prix Nobel de la paix,
- le célèbre footballeur Ali Karimi
- et les actrices Nazanin Boniadi
- et Golshifteh Farahani.

Après le décès de Mahsa Amini, les désaccords initiaux ont semblé laisser
la place à un front uni derrière le slogan « Femme, vie, liberté ».

Le 10 février, tous ont même partagé une plateforme à l’université
Georgetown à Washington et se sont engagés à travailler à la formation
d’un front d’opposition commun.
Mais l’alliance n’a pas tenu, les dissensions ont ressurgi et la belle
entente s’est écroulée.

Hamid, 26 ans, travaille dans la construction.
Originaire d’un village du Nord, il est venu à Téhéran chercher du travail.
Sur son temps libre, il aime pratiquer la lutte. Pour lui, le changement
et l’opposition ne peuvent venir que de l’intérieur de l’Iran.
« Beaucoup de mes connaissances admirent ces personnalités en exil.
Certains imaginent même Reza Pahlavi comme un futur monarque. Je ne
soutiens pas le retour de la monarchie, mais c’est frappant de voir
que la répression a réussi à rendre cette hypothèse attirante pour
certains », note-t-il.

Sa petite amie, Zahra, 24 ans, qui travaille dans un salon de beauté,
n’est pas tout à fait d’accord. Pour elle, le soutien de la diaspora
peut apporter un changement.

Si le régime n’a pas bougé, ni même vraiment vacillé, a-t-il pour autant gagné ?

Les événements de l’année passée ont indéniablement laissé une marque
indélébile sur l’Iran et sa société.
« Il y a un an, lorsqu’une femme refusait de porter le hijab, elle suscitait
des réactions de surprise.

Aujourd’hui, ce geste, répété quotidiennement par des milliers de femmes,
est accueilli par des encouragements et des sourires du public, en dépit
de la répression accrue », remarque Elnaz, 25 ans, étudiante kurde en
psychologie.
Pour elle, ces actes de résistance quotidiens sont significatifs.
« Une année s’est écoulée et nous avons mené plusieurs batailles.
Nous en avons gagné certaines et perdu d’autres.

Mais une chose est certaine : quel que soit le temps que prendra cette
guerre, c’est nous qui la gagnerons. »

(1) Tous les noms des personnes citées sont des noms d’emprunt par mesure
de protection

