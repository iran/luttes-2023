.. index::
   pair: Meurtre ; Faramaz Javidzad
   ! Faramaz Javidzad


.. _faramaz_javidzad_2023_09_23:

=========================================================================================================================
2023-09-23 **Faramaz Javidzad an Iranian-American Jewish citizen dies in Evin Prison due to lack of medical care**
=========================================================================================================================


- https://iran-hrm.com/2023/09/25/an-iranian-american-jewish-citizen-dies-in-evin-prison-due-to-lack-of-medical-care/


.. figure:: images/faramaz_javidzad.png
   :align: center

On Saturday, September 23, 2023, Faramaz Javidzad, an Iranian-American
Jewish citizen detained in Evin Prison, lost his life due to lack of
medical care.

According to reports received by the Iran Human Rights Monitor (IranHRM),
the 63-year-old Javidzad suffered from cardiovascular disease, diabetes,
and gastric bleeding.

Following the worsening of his medical condition, he was transferred to
the prison’s infirmary on Friday, September 22, 2023.
However, after a short period of time, without any attention to the critical
condition of this sick prisoner and without providing proper medical care,
he was returned to the general ward.

An important point to note is that, prior to this incident, despite the
judge’s approval to transfer Faramaz Javidzad to civic medical centers
for treatment, he faced obstacles and refusal by prison authorities,
and this arrangement had not been realized.

The disregard for the medical history and condition of this sick prisoner
and the officials’ evasiveness in transferring him to a hospital are
considered as among the most significant reasons for his death.

Faramaz Javidzad, an Iranian-American dual citizen of the Jewish faith,
was a resident of Los Angeles, USA.
Prior to his arrest, he was involved in real estate activities in Iran.

In March 2022, he was arrested by agents of the Ministry of Intelligence
based on a complaint from the “Executive Headquarters of Imam’s Order.”
He spent three months in solitary confinement in Ward 209 of Evin Prison,
also known as the Ministry of Intelligence ward.
He was eventually transferred to the general ward of Evin.

The conditions in Iranian prisons are examples of psychological and emotional
torture for the prisoners, and they endure harrowing circumstances.
Some of them lose their lives due to lack of medical care.

**However, due to media censorship and the prevailing atmosphere of suppression,
news of their deaths does not receive widespread coverage.
Only a limited number of cases of prisoners’ deaths are reported in the media.**

