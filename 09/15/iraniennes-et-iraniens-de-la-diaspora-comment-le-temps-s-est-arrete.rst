.. index::
   pair: Diaspora ; Iran
   pair: France Culture ; Lespieds sur terre

.. _france_culture_2023_09_15_diaspora:

=================================================================================================================================
2023-09-15 **Iraniennes et Iraniens de la diaspora, comment le temps s'est arrêté** par France Culture/Les pieds sur terre
=================================================================================================================================

- https://www.radiofrance.fr/franceculture/podcasts/les-pieds-sur-terre/iraniennes-et-iraniens-de-la-diaspora-comment-le-temps-s-est-arrete-3572470

Préambule
============

Depuis la mort de Mahsa Amini, Chirinne, Enghareh et Alireza ont tout
arrêté pour soutenir ici, en France, le mouvement de révolte iranien
« Zan, Zedegi, Azadi » (« Femme, vie, liberté »).

Elles et il racontent cette année de révolte, d’espoirs et de déceptions
au micro de Pauline Chanu.

Le temps s'est arrêté pour ces Iraniens et Iraniennes de la diaspora.

Ils et elles ont cessé brutalement toutes leurs activités, posé leur sac
et retroussé leur manche.
Ils ont informé, soutenu, petitionné, relayé, écrit, parlé, et répété.
Elles ont crié très fort très haut, ce que le peuple ne pouvait exprimer
que plus bas.

“Tout le monde s'est soulevé et là j'ai compris qu'il était possible de faire quelque chose”
==================================================================================================

Enghareh a été élevée à Montpellier par deux parents iraniens.
Son enfance a été rythmée par les voyages dans leur pays natal. Le dernier
remonte à mai 2022, elle en garde un terrible souvenir : “ça a été pire
que tout. Ma famille, comme tous les gens avec qui je pouvais parler,
avait perdu tout espoir de vivre bien un jour.

En rentrant, j’ai passé 24h au lit à pleurer”.

Le 16 septembre 2022, son père, comme à chaque brèche d’espoir, annonce
la fin du régime. Enghareh l’espère sans trop y croire, mais décide de
s’engager activement dans le soutien à la révolte déclenchée par la mort
de la jeune Mahsa Amini.
Les mois qui suivent sont difficiles : “Pendant six mois, clairement,
j'ai déprimé, mais en étant très impliquée, très active, en donnant vraiment
toute l'énergie que j'avais”.
L’inquiétude, la désillusion, l’impuissance, la fatigue de hurler sans
avoir l’impression d’être entendue l’usent.

Le suicide de Mohammad Moradi, un Iranien résident à Lyon, en décembre 2022,
est un coup de massue : “Je le comprenais, parce que depuis trois mois,
à ce moment-là, j'étais engagé et je voyais tous les obstacles qui
s’opposaient à nous”.

Sa mère refuse qu’elle flanche au nom de toutes celles qui souffrent en Iran.
Elle reprend alors espoir devant les images des femmes dévoilées qui dansent
place Azadi à Téhéran : la place de la liberté.
Elle s’émerveille et relaie ces centaines de petits gestes héroïques déployés
par les militants en Iran : « Tout est interdit mais ils se débrouillent ».


“Je ne pourrai pas retourner en Iran tant que le régime ne sera pas tombé”
==============================================================================

Chirinne est avocate.

Depuis Paris, elle collecte les preuves (photos, vidéos, témoignages) des
exactions commises par le régime iranien dans le but de traduire les
responsables devant la justice internationale et d’aider les familles des
victimes à fuir.
Aveux forcés, enlèvements, viols, stigmates de la torture : tous les jours,
elle consigne minutieusement dans un fichier Excel des dizaines d’images
d’atrocités commises sur les manifestants par les autorités iraniennes.

“Pendant un an, j'ai vu ça toute la journée et en fait, on est tellement
dans le rush que j'ai même pas percuté que c’était dur”, confie-t-elle,
la gorge nouée. “Le risque, c’est de devenir insensible”.

Évoquant le souvenir d’Hadis Najafi, jeune Iranienne abattue par les
forces de sécurité, Chirinne constate : « à la logique de la mort, c’est
le droit de vivre qu’oppose les manifestants, le droit au bonheur ».

L’avocate le sait : elle ne pourra pas retourner en Iran tant que le
régime ne sera pas tombé. Un lourd sacrifice, puisque son père âgé s’y
trouve : “s’il lui arrivait quelque chose, je ne pourrai pas être là”,
mais “c’est le prix à payer” d'une lutte qu’elle espère victorieuse.


"J’ai quitté l’Iran mais l’Iran ne m’a jamais quitté"
=======================================================

Installé en France depuis 32 ans, Alireza est anéanti par la répression
sanglante que subit son peuple.

Moralement et matériellement, il s’est dévoué intégralement à contribuer
à leur lutte, à distance, au détriment de toute autre activité :
“Je me suis consacré pendant sept mois, matin et soir, à ce mouvement ,
à l'organisation de manifestations, à regarder sur les chaînes accessibles
ce qui se passe en Iran.

On dormait quasiment pas, on restait en réunion avec des amis sur des
groupes WhatsApp et Signal jusqu'à 3 ou 4 h du matin” .

Il lui est insupportable de voir des enfants mourir sous les balles, à
l’image de Kian Pirfalak , “un gamin de 10 ans” tué dans la région d’Izeh,
au sud de l’Iran, où Alireza a passé ses plus beaux étés.

Déçu par la mobilisation de la diaspora iranienne en France, **Ali imagine
retourner faire la révolution dans son pays natal et retrouver sa famille,
qui ne cesse de lui répéter que “pour comprendre ce qu’il se passe en Iran,
il faut aller en Iran”** .

Alireza y croit : « le vent de la liberté a commencé à souffler sur le pays » .

:Reportage : Pauline Chanu
:Réalisation : Emmanuel Geoffroy

NB : Pour protéger les iraniens et iraniennes sur place (les familles des
victimes, les personnes en prison, etc.), nous avons dû retirer certaines
informations et renoncer à certains témoignages.

Pour aller plus loin
========================

- :ref:`livre_femme_vie_makaremi_2023`
- :ref:`livre_femme_vie_satrapi_2023`
- :ref:`livre_femme_reve_liberte`
- :ref:`temoignages_2023_09_08`

A voir
========

- `Rise <https://www.youtube.com/watch?app=desktop&v=BJnhsF1ST_U>`_, réalisé par Ariane Gray

..  youtube:: BJnhsF1ST_U


A écouter
============

Baraye
---------

- :ref:`Baraye par Shervin Hajipour (2023) <baraye>`
- `Baraye <https://www.youtube.com/watch?v=0th9_v-BbUI>`_


Roosarito
------------

- `Roosarito », par Mehdi Yarrahi (2023) <https://www.youtube.com/watch?v=7ChZablq2OY>`_

..  youtube:: 7ChZablq2OY

Normal
--------

- `Normal » , par Toomaj Salehi (2021) <https://www.youtube.com/watch?v=jcGMGFBaQrU>`_

..  youtube:: jcGMGFBaQrU


Merci à Bahar Makooi et Ariane Gray, ainsi qu’à Jean-Pierre Perrin,
Aïla Navidi, Hura Mirshekari, Atoosa, Yasmine et Amir.

Musique de fin :ref:`Baraye par Shervin Hajipour (2023) <baraye>`

