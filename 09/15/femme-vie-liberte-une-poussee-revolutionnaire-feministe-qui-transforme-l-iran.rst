

.. _mediapart_revolution_2023_09_15:
.. _mediapart_chowra_2023_09_15:

=================================================================================================================================
2023-09-15 **Femme, vie, liberté : une « poussée révolutionnaire féministe » qui transforme l’Iran** par Rachida El Azzouzi
=================================================================================================================================

- https://www.mediapart.fr/journal/international/150923/femme-vie-liberte-une-poussee-revolutionnaire-feministe-qui-transforme-l-iran


Préambule
===========

Pour l’anthropologue iranienne Chowra Makaremi, c’est un divorce :
« La société iranienne se définit désormais contre son État ».
Elle est aujourd’hui « déterminée à en finir avec la République islamique ».


Introduction
===============

« Tant« Tant que le courage est là », Chowra Makaremi a de l’« espoir »,
celui que l’un des régimes les plus durs au monde, la République islamique
d’Iran, et son projet théocratique totalitaire tombent.

Elle explique d’emblée d’où elle parle : elle est anthropologue et
chercheuse au CNRS, travaille sur l’Iran depuis des années, attachée
aux émotions collectives et aux contre-archives pour ce qu’elles racontent
du passé et du présent.
Mais Chowra Makaremi est aussi une femme qui « vient des marges de l’identité
iranienne » à travers son histoire familiale, « des marges où ont été
relégués tous ceux qui ont participé à la révolution de 1979 et qui ont
été exclus au moment de la constitution de la République islamique,
ceux qui ont été supprimés par la mort ou le silence ».

Elle a grandi dans une famille d’opposant·es au régime des mollahs.
Sa tante a été arrêtée et exécutée en 1982 ; sa mère, arrêtée en 1981
et assassinée en 1988 au cours d’exécutions de masse des prisonniers
politiques.
Chowra Makaremi avait alors huit ans. Elle en avait une vingtaine à
l’hiver 2004, quand elle a découvert le cahier d’Aziz Zarei, son grand-père,
disparu depuis dix ans, dans lequel il consignait les années noires de
ses deux filles ainsi que leurs lettres.

Elle en a fait un livre bouleversant publié en 2013 qui sort en poche,
Cahier d’Aziz. Au cœur de la révolution iranienne 1979-1988 (Folio, Actuel),
un an après la mort de Mahsa Amini, sous les coups de la police des mollahs.
Le Cahier d’Aziz sort en même temps que le livre qui a occupé Chowra
Makaremi ces derniers mois, un essai remarquable tant il éclaire une
histoire longue du pouvoir et de la résistance : Femme ! Vie ! Liberté !
Échos d’un soulèvement révolutionnaire en Iran (La Découverte).

.. figure:: images/manif_nuit.png
   :align: center

   Lors d'une manifestation dans la ville de Mahabad en novembre 2022 après la mort de Mahsa Amini. © Photo SalamPix / Abaca

C’est un livre dédié « aux guerrières ». Il est né dans l’urgence, sur
le vif, depuis l’étranger, pour documenter et archiver au plus près l’un
des soulèvements populaires les plus exceptionnels de l’histoire contemporaine,
« une poussée révolutionnaire féministe » surgie après la mort de Mahsa Amini.

« Durant la révolution de 1979, qui est au cœur de mes travaux, des petits
livres ont été rédigés sur le coup. Ils ont été très précieux car ils
ont véhiculé les affects, les espoirs, les imaginaires qui traversaient
les révolutionnaires, et c’est ce qui m’intéresse en tant qu’anthropologue.
Il me semblait important de le reproduire aujourd’hui. »

**Mediapart : La dictature islamique tient bon et en même temps, depuis un  an et la mort de Jina Mahsa Amini, plus rien n’est comme avant en Iran. Qu’est-ce qui a changé, selon vous ?**
==============================================================================================================================================================================================

Chowra Makaremi : Crise après crise, défaite après défaite, le pouvoir
iranien, très opaque, se clarifie. L’ordre hégémonique, l’identité iranienne
nationale ont été construits autour de la République islamique.
Aujourd’hui, nous assistons à un divorce : la société iranienne se définit
explicitement contre son État alors qu’auparavant, elle tentait de négocier
des marges de respiration à l’intérieur de la République islamique, sans
remettre en cause ses limites et ses frontières.

Elle est déterminée à en finir avec la République islamique, ce qui
n’était pas le cas dans les dernières décennies. La société iranienne a
aussi changé de valeurs, collectivement, notamment autour de la solidarité
interethnique nationale.
Des hiérarchies ethno-sociales qui se faisaient le relais des dominations
d’État, avec une xénophobie très forte à l’encontre des minorités kurdes,
baloutches ou des réfugiés afghans, se modifient.

Le projet réformiste est la forme de contestation la plus autorisée et  la plus prégnante en Iran. Ce soulèvement, parti des femmes, signe-t-il  son effondrement ?
======================================================================================================================================================================

« Femme, vie, liberté », hérité du mouvement kurde, un mouvement socialiste
et fédéraliste, n’est pas une lutte pour le droit des femmes mais une
poussée révolutionnaire du féminisme, rendue possible par une série
d’évolutions : notamment l’échec du projet réformiste, arrivé à une
impasse, à la suite d’une série de trahisons et d’impossibilités actées
par la société civile.

Les mouvements féministes iraniens, qui ont majoritairement emprunté la
voie réformiste à partir des années 1990, ont pris acte, avec « Femme, vie, liberté »,
que seul un changement de régime pouvait permettre d’accéder à la demande
d’égalité constitutionnelle, juridique, politique, économique.
Cette demande rejoint la demande d’égalité des minorités ethniques et nationales.

On voit bien d’ailleurs que c’est au Kurdistan et au Baloutchistan que
le feu est maintenu. Les minorités ethniques et nationales sont le fer
de lance de la révolte, car c’est l’égalité juridique et sociale qui est
le ciment du projet.
Le slogan n’est pas « Femmes, hommes, on veut les mêmes droits » mais
« À bas la dictature ! ».

On est passés de la prudence réformiste au courage, et de l’indifférence
à l’attachement. Nous ne sommes plus dans l’ingénierie du silence qui
était en place depuis les années 1980.
Les procès révolutionnaires, le spectacle de la mise à mort des « ennemis de Dieu »
par la dictature n’ont plus lieu en silence.

Certes, des mécanismes du silence sont encore à l’œuvre mais la résistance
de la société iranienne passe aujourd’hui par la solidarité comme valeur
cardinale du soulèvement. On le voit dans la relation affective à la
figure de Jina Mahsa Amini, aux prisonnières politiques, aux manifestants
exécutés, à leurs familles…

Votre livre est dédié aux « guerrières ». Y a-t-il une figure du soulèvement, au-delà de celle de Jina Mahsa Amini, qui retient votre attention ?
====================================================================================================================================================

Je dirais Sepideh Gholian, même si c’est paradoxal, car elle était en
prison en 2022.
Cette journaliste iranienne, spécialiste des mouvements syndicaux et du
droit du travail, est pour moi une figure du mouvement « Femme, vie liberté ».
Détenue depuis 2018, elle a été de nouveau arrêtée et emprisonnée quatre
heures après sa libération, le 15 mars 2023.
En 2018, elle participait déjà aux premières révoltes où on entendait
« À bas la dictature ! » : c’étaient des mouvements contre la vie chère,
dont les classes moyennes étaient.

.. figure:: images/chowra_makaremi.png
   :align: center

   Chowra Makaremi à Paris, en septembre 2023. © Photo Rachida El Azzouzi / Mediapart

:ref:`Sepideh Gholian <sepideh_golian_2023_09_15>` appelait à ne pas
avoir peur, à manifester. Elle était applaudie par les manifestants,
majoritairement des jeunes hommes des classes populaires, du prolétariat,
premières cibles de la violence d’État.
C’est une syndicaliste mais elle était aussi dans une forme de lutte
ouvertement féministe. Je vois en elle une généalogie de « Femme, vie, liberté ».

En 2023, elle a été graciée par le Guide suprême. Aussitôt libre, elle a
refusé de porter le voile et elle a crié : « Khomeini, tyran ! ».
Quelques mètres plus loin, elle a été arrêtée pour cela. Elle est
toujours en prison. En août 2023, elle a refusé de se voiler pour aller
à son procès. À l’audience, elle a craché sur son inquisitrice, une
journaliste connue pour obtenir des aveux forcés, dont on peut lire une
lettre écrite depuis sa prison à Evin. Sepideh Gholian nous donne les
coordonnées du courage, et tant que le courage reste une valeur, j’ai espoir.

Le mouvement a dû se transformer face à la répression. Peut-on vraiment mesurer l’ampleur de cette répression ?
======================================================================================================================

Cela peut prendre des années, on l’a vu avec la révolution de 1979,
notamment concernant les viols. Les femmes non mariées, donc vierges,
étaient violées systématiquement « pour qu’elles n’aillent pas au paradis ».
Mais on a commencé à parler des violences sexuelles subies par les
prisonnières seulement après 2009, quand les manifestants du mouvement vert,
incarcérés notamment à Kahrizak à l’été 2009, ont dénoncé les viols
qu’ils avaient subis.
La mémoire des femmes violées dans les années 1980 est devenue audible.
J’ai l’habitude de travailler avec des matériaux qui deviennent dicibles,
audibles longtemps après.

Vous vous attachez aussi aux rumeurs. Pourquoi en faire un sujet d’étude ?
===============================================================================

Parce qu’elles nous renseignent sur le niveau et la texture de la violence
politique. Des historiens ont travaillé dessus en contexte colonial : plus
l’information devient invérifiable et circule sous forme de rumeur, plus
la violence d’État refaçonne les rapports sociaux.
La rumeur en Iran porte sur le fait que les corps sont martyrisés et victimes
de vols d’organes, comme pour les manifestantes kidnappées et tuées,
telles Nika Shahkarami ou Aïda Rostami, cette jeune médecin qui soignait les blessés.

On sait à quel point la vente des organes est importante en Iran. Elle
dit le niveau de détresse économique, les conditions dans lesquelles
vivent les Iraniens, mais aussi une réalité : quelqu’un qui vend sa
cornée pour nourrir sa famille est quelqu’un qui accepte de devenir
aveugle pour assurer sa survie économique.
La vente et le trafic d’organes illustrent le néolibéralisme prédateur,
une forme de cannibalisme du pouvoir.
**Ce soulèvement, c’est aussi une lutte socio-économique**.

Vous n’hésitez pas à faire place dans vos recherches à la subjectivité
et aux émotions, parce que l’intime est politique. D’ailleurs, votre vie
en témoigne : vous avez grandi dans une famille d’opposants au régime
des mollahs. Votre tante a été exécutée en 1982 ; votre mère en 1988…

Je viens des marges de l’identité iranienne, de par mon histoire familiale,
des marges où ont été relégués tous ceux qui ont participé à la révolution
de 1979 et qui ont été exclus au moment de la constitution de la République
islamique, ceux qui ont été supprimés par la mort ou le silence.
J’ai travaillé avec « le reste » de la nation et sur la manière dont
ce « reste » éclaire le projet théocratique totalitaire en Iran.

En étudiant ses ressorts, ses violences d’État, avec du matériel chaud,
en prêtant une attention aux imaginaires, affects, émotions, j’ai voulu
comprendre comment il a réussi à faire coïncider identité iranienne et
identité républicaine islamique.
Ce qui m’a placée aux premières loges pour saisir l’effondrement de
cet ordre-là, même si le pouvoir se maintient par la force.
À la lumière du soulèvement, le passé pose au présent une question :
que fait-on de l’héritage révolutionnaire de 1979 ?

Vous avez participé à la rédaction d’une tribune féministe transnationale
qui doit être publiée samedi 16 septembre 2023 dans le Club de Mediapart,
et qui dénonce « le féminisme libéral, islamophobe et transphobe » des
pouvoirs occidentaux, qui n’ont eu à la bouche qu’admiration pour le
« courage des femmes iraniennes », tout en réhabilitant Téhéran sur la
scène internationale.
L’émancipation des peuples n’est pas un enjeu pour les pouvoirs occidentaux ?

Les pouvoirs occidentaux se sont assis sur les acquis de la rue iranienne
qui a mis genoux le régime, pour négocier avec ce dernier dans une relation
de force qui était à leur avantage. Ils ont relégitimé et réinstitué la
République islamique dans le concert des nations, comme en témoignent
l’invitation par la Belgique du maire de Téhéran, Alireza Zakani, un
ultraradical du régime iranien, ou encore la reprise au très haut niveau,
avec le bureau du Guide, des négociations sur le nucléaire en janvier 2023,
la non-inscription par l’Union européenne des Gardiens de la révolution
sur les listes des organisations terroristes.

.. figure:: images/jina_amini.png
   :align: center

   Mahsa Amini. © Photo Réseaux sociaux via ZUMA Press Wire / Abaca

Le message envoyé au peuple iranien est terrible : l’Occident souhaite
le maintien du régime des mollahs. En 1978, un général américain avait
été dépêché en Iran pour rencontrer l’armée iranienne et lui demander
de ne pas intervenir en soutien du Shah, de rester neutre en cas de
soulèvement populaire.
Les niveaux géopolitiques et locaux sont intimement imbriqués dans
l’aboutissement des mouvements révolutionnaires, **et on sait désormais
dans quel contexte les Iraniens doivent lutter : seuls face à leurs bourreaux**.

La République islamique a sacralisé le voile et en a fait un des piliers
de sa théocratie.
**L’Occident fait une fixette sur ce bout de tissu, en particulier la France,
au point que les femmes iraniennes sont régulièrement instrumentalisées**
sur la scène française, y compris par les féministes. Comment l’analysez-vous ?

Je m’attache normalement à explorer les ambiguïtés. **Mais sur ce point,
les choses sont simples et claires : la question en Iran n’est pas le
voile mais son obligation, c’est-à-dire l’inscription de la loi sur le
corps des femmes, que ce soit par l’obligation ou l’interdiction vestimentaire**.

Le voile est en Occident le symbole de l’oppression des femmes dans les
sociétés musulmanes. L’attention particulière accordée à ce sujet est un
héritage des pouvoirs coloniaux français et anglais qui ont utilisé le
voile pour construire une partie de leur discours sur les bienfaits
civilisateurs de la colonisation : il s’agissait dès le XIXe siècle de
dévoiler les femmes arabes et musulmanes pour rendre ces sociétés libres.

Cette façon d’imaginer, et même de désirer l’acte du dévoilement comme
un « éveil » de la femme iranienne ou afghane, est un problème propre
aux sociétés occidentales et à leur rapport avec leur héritage colonial.

**Il ne concerne pas les Iraniennes**.

**La ligne de partage chez elles ne passe pas entre les femmes voilées et
non voilées, mais entre celles qui sont d’accord avec ces obligations
vestimentaires et celles qui les refusent**.

Par ailleurs, les Iraniennes n’éprouvent aucun sentiment d’altérité,
d’hostilité, d’étrangeté face au voile. Il est familier : c’est celui
de nos grands-mères, de nos tantes, de nos mères et amies.
Ici, une expérience de l’altérité se mêle à la lutte des idées et des
principes sur cette question : plus elle est déniée et plus on se dit
que le problème de la xénophobie est profond et complexe.

Cela non plus ne concerne pas les Iraniennes.

Il n’y a aucune contradiction entre le port voulu du voile et la révolte
des iraniennes. Il n’y a même pas de rapport entre les deux.
C’est facile à analyser, mais si difficile à rendre audible.
On devrait se demander pourquoi.

Il est essentiel de saisir que les Iraniennes ne sont pas « enfin »
réveillées après 40 ans en ôtant leurs voiles.
Actant l’échec du réformisme, qui ne s’attaquait pas aux fondements de
la théocratie mais tentait de négocier progressivement l’égalité, elles
ont basculé dans la confrontation directe, qui passe stratégiquement par
la remise en cause du voile obligatoire en tant qu’ADN non négociable
de la République islamique : elles ont transformé une « ligne rouge »
du pouvoir en barricade.

C’est cela que signifie, pour les Iraniennes et les Iraniens, le fait
de reconfigurer la contestation révolutionnaire dans la lutte contre
l’obligation du voile !

**Les États occidentaux ont trahi la rue iranienne en 2023**.

**Il ne faudrait pas que la société civile en France la trahisse elle a
ussi, une seconde fois, en instrumentalisant la lutte progressiste et
courageuse des Iraniennes dans ses polémiques autour du voile, qui au
fond intéressent très peu de gens**.


:ref:`Chowra Makaremi, Femme ! Vie ! Liberté ! Échos d’un soulèvement
révolutionnaire en Iran, La Découverte, septembre 2023 <livre_femme_vie_makaremi_2023>`

.. figure:: ../../livres/femme-vie-liberte-chowra-makaremi/images/recto_livre.png
   :align: center



**Rachida El Azzouzi**
