

.. _mediapart_2023_09_15_resistance:

=====================================================================================================================
2023-09-15 **Face à une répression tentaculaire, l’opiniâtre résistance des Iraniennes** par Jean-Pierre Perrin
=====================================================================================================================

- https://www.mediapart.fr/journal/international/150923/face-une-repression-tentaculaire-l-opiniatre-resistance-des-iraniennes


Préambule
============

À l’approche du premier anniversaire de la mort de Mahsa Amini, le régime
frappe préventivement toute contestation.

Sans pouvoir empêcher les femmes de transformer les rues en espaces de
lutte, ni de s’exprimer depuis leurs lieux de détention.

Introduction
================

Elles sont les principales figures de la désobéissance civile en Iran.

Cinq prisonnières politiques, quatre embastillées à Evin, près de Téhéran,
et une à Yazd, dans l’est du pays.
Toutes brutalisées, certaines agressées sexuellement.
Toutes dans l’impossibilité de communiquer avec l’extérieur, sauf avec leurs familles.

Ce sont pourtant elles qui, par leurs actions, leurs lettres clandestines,
leurs messages réguliers, ont fait des prisons le premier pôle de la
résistance face à un régime qui, craignant le retour des révoltes pour
l’anniversaire, le 16 septembre, de la mort de Mahsa Jina Amini, sombre
dans la répression la plus féroce.

**Narges Mohammadi** est journaliste et militante de longue date des droits humains.

**Sepideh Gholian** est journaliste également, spécialisée dans les conflits
sociaux.

**Bahareh Hedayat** est étudiante en sciences économiques à l’université de
Téhéran.

**Zeinab Jalalian**, une militante kurde, n’a pas de profession bien définie,
ayant été emprisonnée très jeune.

Et **Niloufar Bayani** est une spécialiste de la biodiversité.

Narges Mohammadi
==================

La plus connue, Narges Mohammadi, 51 ans, condamnée à 32 ans de prison
et 154 coups de fouet lors de six procès, qui a déjà reçu le prix Sakharov
et est en lice pour le prochain prix Nobel de la paix, a ainsi réussi à
faire sortir de prison plusieurs billets témoignant de la condition carcérale.

En 2022, son enquête sur « la torture blanche » exposait le témoignage
de 14 détenues soumises à un isolement total dans une cellule aux murs
blancs, sans aucune notion du temps.
Ses actions lui ont valu depuis décembre 2022 cinq procédures du parquet
de sûreté d’Evin, ainsi que onze convocations et avertissements.


.. figure:: images/les_5_femmes.png
   :align: center

   Zeinab Jalalian, Niloufar Bayani, Narges Mohammadi, Bahareh Hedayat et Sepideh Gholian. © DR

Lundi 11 septembre 2023, elle a été battue à l’hôpital de la prison par un geôlier
parce qu’elle ne portait pas de voile, selon le site indépendant IranWire.


.. _sepideh_golian_2023_09_15:

Sepideh Gholian
=================

Sepideh Gholian, 28 ans, a été arrêtée en juillet dernier, peu après sa
libération d’Evin où elle avait purgé une peine de cinq ans.
À sa sortie, elle avait posté sur ses comptes X (ex-Twitter) et Instagram
une vidéo qui la montrait tête nue, clamant un slogan hostile au Guide
suprême iranien : « Khamenei le tyran, nous te traînerons sur le sol ! »

Bahareh Hedayat
==================

Maintes fois arrêtée, Bahareh Hedayat, 41 ans, est l’une des initiatrices
de la campagne « Un million de signatures » pour changer les lois qui
discriminent les femmes.
Arrêtée le 11 octobre 2022, elle ignore les charges retenues contre elle.
Hospitalisée « dans un état sérieux », selon son avocate, elle a mis fin
mercredi 13 septembre à une grève de la faim de 14 jours, à la demande
notamment de ses codétenues et de la mère de Mahsa Amini.


Zeinab Jalalian
================

Condamnée à mort, puis à la perpétuité incompressible pour appartenance
à un parti kurde, ce qu’elle nie, Zeinab Jalalian, 41 ans, est la détenue
qui subit depuis 15 ans les conditions les plus dures dans la prison de
Yazd, au plus loin du Kurdistan.

Torturée, agressée sexuellement, malade et privée de soins médicaux, elle
est interdite de toute communication avec l’extérieur.
Elle a cependant réussi à faire connaître ses terribles conditions de
détention et fait savoir qu’elle ne céderait jamais, même si, a-t-elle indiqué,
« tous les jours, on vient [lui] demander de [s]e repentir ».


.. figure:: images/prison_evin.png
   :align: center


Niloufar Bayani
==================

Enfin, l’écologiste Niloufar Bayani, 36 ans, a été condamnée, en février 2020,
à dix ans de prison pour « espionnage » en raison de ses actions en faveur
de l’environnement.

Auparavant, elle avait subi, selon ses proches, 1 200 heures d’interrogatoires
et huit mois d’isolement. En prison, elle a récemment réussi à cosigner
une tribune appelant à la libération de tous les prisonniers et prisonnières
politiques.

Prisonnières, actrices et avocates en première ligne
===========================================================

Pour toutes, la prison n’est pas seulement un lieu de détention.

C’est aussi un lieu de contestation. Ainsi, c’est depuis la cour de
promenade d’Evin qu’elles avaient organisé une petite réunion pour
appeler à la mobilisation contre la prochaine exécution de trois jeunes
manifestants à Ispahan. Sans succès : ils ont été pendus le 11 mai 2023.

« Emprisonnées mais ne pliant toujours pas, ces femmes incarnent aux
yeux de tous la résistance et la désobéissance civiles, souligne l’avocate
franco-iranienne Chirinne Ardakani, qui enquête sur la répression pour un
collectif de juristes franco-iranien·nes.
En Iran, il n’y a pas eu de structuration politique de l’opposition dans
une quelconque organisation. Mais le grand mouvement de la société civile
s’est quand même articulé autour de trois pôles, dont le premier est
celui des prisonnières politiques.

Le second est constitué des artistes, des milieux du cinéma, en particulier
des actrices qui continuent d’apparaître sans voile à des cérémonies publiques,
à l’exemple de Katayoun Riahi, en dépit de plaintes en justice déposées
par la police de Téhéran.

Le troisième pôle est celui des avocats et avocates. »

« Le fait que les manifestations se soient arrêtées ne veut pas dire que
la séquence de protestations a cessé, ajoute l’historien Jonathan Piron,
professeur de relations internationales à l’école HELMo de Liège (Belgique).
Il y a une transformation dans le mode de contestation. Il ne faut pas
oublier que manifester coûte cher, au sens pécuniaire notamment.
Il y a eu aussi un changement des tactiques de contestation. On voit
depuis le début de l’année que ce sont principalement des actions de
désobéissance civile qui prolongent la séquence : le refus de porter le
voile dans la rue, le partage de photos mettant en scène des gestes de
défiance explicites contre le régime, des pétitions regroupant divers
acteurs collectifs pour plus d’égalité… »

.. figure:: images/femme_sur_la_voiture.png
   :align: center

Ces trois pôles agissent ensemble comme les doigts d’une main.
Quand le chanteur Mehdi Yarrahi est emprisonné pour son morceau « Roozarito »
(« Ôte ton voile »), Sepideh Gholian lui rend aussitôt hommage dans une
lettre sortie là encore clandestinement de prison : « Dans le climat de
répression incessante […] et le sentiment de désespoir dans la société,
ta voix est devenue notre courage. Nous serons à tes côtés jusqu’à ce
que les fondements de l’oppression soient renversés. »

::

    Les femmes ont récupéré une partie de l’espace public sur l’État. […]
    Aujourd’hui, ce sont des espaces de luttes.
    Jonathan Piron, historien

Même si ces trois pôles de désobéissance ne constituent pas une menace
existentielle pour le régime, celui-ci s’acharne à les réprimer.

Il s’y emploie d’autant plus à l’approche de l’anniversaire de la mort de
Mahsa Amini qu’il craint une relance de la contestation.

L’actrice de théâtre Kosar Eftekhari, gravement blessée à l’œil lors
d’une manifestation et qui sort, elle aussi, dévoilée et en se montrant
éborgnée, vient de faire savoir qu’elle avait été convoquée devant un
tribunal pour répondre des accusations de « complot contre la sécurité
nationale » et « propagande contre le régime ».

Dans la province de Gilan (nord de l’Iran), ce sont 12 militantes des
droits des femmes qui ont été arrêtés les 16 et 17 août 2023 pour « préparation
des émeutes avec l’argent des puissances étrangères », une accusation
des plus graves qui ne relève plus du droit islamique mais de la sécurité
nationale, ce qui leur vaudrait, selon IranWire, d’être placées en cellule
d’isolement et torturées.

Plus inquiétant encore, deux d’entre elles, Jelveh Javaheri et Forough
Sami’nia, n’ont donné aucune nouvelle depuis leur arrestation.

Un militant, qui a été appréhendé avec elles, est apparu avec des signes
évidents de tortures.

« Les femmes ont récupéré une partie de l’espace public sur l’État,
explique Jonathan Piron. Les rues ont longtemps été l’espace où le régime
se mettait en scène. Aujourd’hui, ce sont des espaces de luttes.
Les femmes ont gagné en confiance en elles pour prolonger le combat.
Le changement de mentalité qui s’est produit est encore bien plus vaste.
Cela se voit dans le mur de la peur qui s’est effondré pour beaucoup,
dans l’augmentation du nombre de protestations et de grèves.
Les minorités ethniques comme les Kurdes et les Baloutches, victimes de
discriminations depuis des décennies, sont aussi présentes dans ces luttes,
comme une partie de la classe ouvrière qui a une expérience de la protestation.

Mais tout cela reste difficile à analyser : l’Iran est devenu un pays
fermé aux observations de terrain. »


« Purification » de l’université
=======================================

Tentaculaire, la répression frappe tous les milieux.

Au Kurdistan, ce sont 35 membres de l’association environnementale Sabzchia,
qui joue un rôle important dans la lutte contre les feux de forêt, qui ont
été arrêtés le 10 septembre 2023 lors de leur réunion annuelle à Marivan,
par les Pasdarans (Gardiens de la révolution), selon le Kurdistan Human
Rights Network (KHRN).

Sont désormais ciblées aussi les familles des victimes de la répression,
pour les empêcher d’organiser des cérémonies de deuil, qui pourraient
le 16 septembre se transformer en manifestations.
Parmi les personnes arrêtées, Safa Aeli, l’oncle de Mahsa Amini, le 5
septembre à Saqqez, la ville natale de la jeune fille, dont la tombe
est désormais surveillée par des caméras.

La mère d’un manifestant de 21 ans, Erfan Rezaï, tué à Amol (nord du pays)
le 21 septembre, d’une balle tirée à bout portant dans le dos alors qu’il
décollait une affiche gouvernementale, l’a été également.

Le durcissement du régime frappe aussi les universités, où persistait
une certaine liberté de discussion.
Cinq professeurs, certains renommés, comme l’historien Dariush Rahmanian,
ont été ainsi contraints à la démission et remplacés par des ultraconservateurs.
Le ministère de l’intérieur a justifié cette purge en accusant les
enseignants d’avoir fait preuve de « sentiments antinationaux » pendant
les protestations et de « stagner scientifiquement ».

Les réformistes ont réagi. L’ancien maire de Téhéran, Pirouz Hanachi, a
déploré une « purification des universités » et le quotidien Etemadi a
prédit que « ce processus de purification ne s’arrêterait pas là » et
que « les étudiants seraient les prochaines victimes ».

« Le régime s’engage de plus en plus dans une volonté de répression
continue et de renforcement de l’autoritarisme, conclut Jonathan Piron.
Les ultraconservateurs ne se soucient même plus de répondre aux attentes
sociales et économiques.
Les quelques mesures prises pour aider les classes les plus précaires
sont directement avalées par le taux d’inflation, continuellement
supérieur à 40 %.
Et le régime alimente les motifs de ressentiment : les règles sur le port
du voile deviennent encore plus strictes et rigoureusement appliquées.

La répression touche tous ceux qui s’écartent des lignes rouges et la
fracture entre l’élite et la population est toujours aussi grande.
Le régime est dans une logique d’État de surveillance, de tension
permanente, qui lui permet de maintenir un contrôle sur la population
mais lui coûte aussi beaucoup d’énergie et donc, d’une certaine manière,
l’épuise petit à petit. »

Jean-Pierre Perrin

