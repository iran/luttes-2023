.. index::
   pair: Assemblée Féministe Transnationale ; Appel en anglais


.. _apppel_transnational_2023_09_16_en:

=============================================================================================================================================
2023-09-16 **The fight of Iranians is the fight of feminists and all those in struggle around the world. Woman, Life, Freedom (anglais)**
=============================================================================================================================================

- https://blogs.mediapart.fr/les-invites-de-mediapart/blog/160923/fight-iranians-fight-feminists-and-all-those-struggle-around-world-woman-life-freedom
- https://linktr.ee/ass_feministe_transnat
- https://www.instagram.com/assfemtransnat/
- https://blogs.mediapart.fr/les-invites-de-mediapart/blog/160923/le-combat-des-iranien-nes-est-celui-des-feministes-et-des-corps-en-lutte-du-monde-enti
- https://framaforms.org/solidarite-feministe-transnationale-avec-le-soulevement-femme-vie-liberte-en-iran-transnational


.. figure:: images/linkertree_ass_feministe.png
   :align: center

   https://linktr.ee/ass_feministe_transnat


Introduction
=============

An initiative of the Transnational Feminist Assembly and signed by a
multitude of associations and individuals from around the world, this
statement of solidarity with the Woman*, Life, Freedom uprising in Iran
explains that, despite near exhaustion, it is still possible to organise
our forces to change the future, and against all odds.

“The Iranian revolution is not satisfied with just opposing the deadly
policies of the Islamic Republic, it’s mapping out a project for a
post-capitalist society, one of solidarity and emancipation.”

For Iranian journalist :ref:`Sepideh Gholian <akrami_iran:sepideh_qoliyan>`, a specialist in labour laws and
an activist for the cause of trades union rights, and who has been imprisoned
in Iran since 2018.
During her time in jail, she has reported on the conditions of women in
prison, via letters and verbal accounts.

An activist of  the Woman*, Life, Freedom movement[1] , she was sent back
to prison on March 21st 2023 just four hours after her release from a
previous jail term.

**Her courage and determination is an inspiration to feminists the world over,
and serves to underline the level at which resistance is now situated**


On September 16th 2022, Jina Mahsa Amini, a 22-year-old Kurdish woman,
died in Tehran after being violently assaulted by the Islamic Republic’s
morality police.

Her murder led to a popular uprising that shook Iran, and which has been
relayed around the world.
Originally a political slogan used in Kurdistan, “Woman*, Life, Freedom”
became the rallying cry for the street protests in Iran.
It was insurrectional, vital, and fluid – it requested nothing and demanded
everything.

Over the past 12 months, and in the face of bloody repression, the
insurrection has evolved frequently.
From it has emerged an extraordinary network of solidarity, including
spontaneous demonstrations from district to district, neighbors who leave
their front doors open to offer protection to fleeing demonstrators, and
strikes by shopkeepers, and nighttime demonstrations in front of prisons
in protest at executions.

Trade unions, in a historic joint statement, demanded decent working
conditions, an end to ecocidal policies, to nuclear arms and to privatisation
of natural spaces, but they also demanded political equality for women,
for ethnic minorities and for the LGBTQIA+ population – who were “at the
forefront” of the revolution[2] - for the Iranian feminist revolution is
a total revolution.

The issue is not whether or not to wear the hijab
====================================================

**The issue is not whether or not to wear the hijab**.

In the context of Islamophobia in France, this can never be explained too
often, it is for women to decide for themselves.
What is at stake with the Iranian law requiring women to wear the headscarf
is the control and subjection of all bodies by the state, with the aim,
of a minority, to monopolize resources.

*The Islamic Republic governs by use of gender apartheid and state racism**.

It keeps its hold only through the furious deployment of a policing system
that is adapted to the racial character of the populations that it seeks
to constrain.
**All of these techniques are instilled in the global colonial economy**.

Lives don’t count: that reality was apparent in France in June 2023 with
the murder of Nahel, a teenager by a police officer of during a traffic
stop and the bloody repression of the protesting movements that ensued.

We find it at every level: from the Mediterranean coast, where the pushbacks
of migrant boats have transformed the sea into a cemetery, to working-class
areas in Europe, and also to Mayotte and French Guiana, Brazil, Palestine,
Sudan, Lebanon, Afghanistan and Iran.

The heart of the feminism which we champion is the fight against this
continuum of violence and de-humanisation which is at work in capitalism.

For as long as we do not assert our voice, feminism will remain monopolized
by views that legitimise this order.
That was the case in the past year; Western powers have had only praise
for the “courage of Iranian women”, while rolling out the red carpet for
a liberal, Islamophobic and transphobic[3] feminism which took care to
separate the fight for women’s rights from those others that are against
all the forms of oppression that are opposed by the revolutionary uprisings
in Iran.

In their international power games, these same Western governments benefit
from the destabilisation of the Islamic Republic by the Iranian street
protestors, while abandoning them to an alarming wave of executions, arrests
and torture.
It has never been clearer that the emancipation of peoples is an nonexistent
issue in international affairs.
This is why a feminist silence is not an option, and why ignorance is not
an excuse.

While it is impossible to order the paths of revolutions, it nevertheless
remains essential to proceed with the exchange of knowledge and know-how
that come from local movements of resistance and to maintain concrete
networks of solidarity to weave together the threads of a people mobilised
at a global level.

**It has never been more urgent to learn from the endurance and methods of
the “Woman*, Life, Freedom” movement, and to lend support to our Iranian
comrades in the face of oppression**.
What we are up against here, like over there, are, in different forms,
the apparatuses of states in the hands of radicalised sections of the
bourgeoisie, whose arguments, religious or secular, are increasingly at
pains to conceal a similar and competing capture of riches, and the
exploitation of all that is living.

We, activists of different organisations, linked together by anti-capitalist
feminist preoccupations, know just how exhausting the struggle is in the
current balance of power.
**This exhaustion is an integral part of government techniques used against
peoples**.
We are collectively thrown into **climate chaos**, our futures compromised
by catastrophes, our present lives suffocated by stress, repression, racial
profiling, poverty, illegality, bodies worn out by work, and a lack of care
and consideration.
It has never been clearer that all the talk of law-and-order, which is
served up daily by the media owned by a reactionary oligarchy, in fact
designates the security of golf courses; security for capital, to the
point that the world dies.

This observation must not let us to lose sight of the fact that it is
not only necessary, but also possible, to organise our forces and to
change the future course of events.

**The curve is tight, and not easy, but it is possible**.

We can begin by urgently taking European feminism out of a state of denial,
for it to confront and keenly combat its own colonial history, and to
direct our actions towards solidarity and transnational reflection.

The Iranian revolution is not satisfied with just opposing the deadly
policies of the Islamic Republic, it is mapping a project for a post-capitalist
society, one of solidarity and emancipation.

It is a lesson of a movement, of political and theoretical reinvention,
and that is why the struggle of Iranians is the struggle of feminists and
all those engaged in emancipatory movements around the world.
Woman*, Life, Freedom.

* "Woman" applies here to all women in the non-biological sense of the word.

[1] `“Femme, Vie, Liberté : déclaration des revendications minimales <https://blogs.mediapart.fr/les-invites-de-mediapart/blog/310523/pour-une-societe-degalite-et-de-liberte-nous-soutenons-les-iranien-nes>`_
     des organisations indépendantes syndicales et civiles d’Iran ”, February 15th 2023

    - :ref:`iran_luttes:revendications_2023`

[2] `Katayoun Jalilipour, “LGBTQIA+ people are at the forefront of Iran’s revolution <https://gal-dem.com/lgbtqi-people-forefront-iran-revolution/>`_
     they should not be forgotten”. Gal-Dem, December 23rd 2022

[3] A symposium was held in Nantes in April 2023 centred on womens’ rights
    and support for Afghan and Iranian women.
    The organisers, the Comité Laïcité République, withdrew their invitation
    to Marguerite Stern, a former member of the feminist activist group
    Femen known for her transphobic positions.

Premier·es signataires
==================================

- Roja. Paris, collectif féministe , Paris
- Feminist 4 Jina, Paris
- Jina Collective - the Netherlands, feminist, queer, anti-capitalist, anti-racist collective, The Netherlands
- Alternatiba Paris, Paris
- Asso FièrEs, Paris, France
- ACGLSF, association LGBTQIA+ des personnes sourdes, France
- Association Collectif Transistor, association of transgender and sex workers, Angouleme, France
- BIG Bari International Gender Festival, cooperativa, Bari, Italia
- Colectiva Feminista La Revuelta en Neuquén, Patagonia, Argentina
- Collectif Cases Rebelles, collectif panafrorévolutionnaire et maison d'édition
- Collectif Stéphanois contre l’islamophobie et pour l’égalité, Saint-Etienne, France
- Commons : Journal of Social Criticism, revue ukrainienne de critique sociale
- Feminist Workshop, organisation féministe ukrainienne, Lviv, Ukraine
- Feminita KZ, queer-feminist human rights organisation, Kazakhstan
- FLIRT - Front Transfem, collectif, Paris, France
- Front de mères, France
- Gras Politique, association, Paris, France
- International Consortium of Critical Theory Programs, USA
- INVERTI-E-S, collectif marxiste Trans Pédés Gouines, Paris, France
- LASTESIS, coliectivo interdisciplinario y feminista, Valparaíso, Chili
- Lesbiennes contre le patriarcat, collectif, Lyon, France
- Les Grenades, média féministe, Belgique
- Les Pétrolettes, Brest et Rennes, France
- Les Soulèvements de la Terre, France
- Ni Una Menos, Argentine
- ORAAJuive, organisation militante, France
- Planning Familial, France
- Pride des banlieues, Saint Denis
- Punto Froce, collectif transféministe, Venise
- Queer sex workers initiative for Refugees, Nairobi, Kenya
- Sex Work Polska, Poland
- Strass, France
- TALAY’AN NGO, trans-led feminist group supporting sexworkers, Morocco
- Union des femmes de Martinique, association de femmes, Martinique
- Ukraine CombArt, association de solidarité avec la resistance ukrainienne, Paris, France
- WANA Wayaki Collective - وانا ویاكي
- #NousToutes, France
