.. index::
   pair: Jardin ; Mahsa Jina Amini
   pair: Paris ; Jardin Villemenin Mahsa Jina Amini

.. _jardin_mahsa_jina_amini_2023_09_16:

===================================================================================================================
Samedi 16 septembre 2023 : **Inauguration du jardin Mahsa Jîna Amini, 14 rue des Récollets, Paris 10e**
===================================================================================================================

- https://www.paris.fr/lieux/jardin-villemin-mahsa-jina-amini-1798

::

    - https://fr.wikipedia.org/wiki/Jardin_Villemin_-_Mahsa_J%C3%AEna_Amini


.. raw:: html

   <iframe width="800" height="400" src="https://www.openstreetmap.org/export/embed.html?bbox=2.360402941703797%2C48.87410258463669%2C2.363943457603455%2C48.87559678145669&amp;layer=mapnik"
           style="border: 1px solid black">
   </iframe>
   <br/><small>
   <a href="https://www.openstreetmap.org/#map=19/48.87485/2.36217">Afficher une carte plus grande</a></small>


La plaque
============

.. figure:: images/plaque_mahsa_jina_amini.png
   :align: center


.. figure:: images/inauguration_par_anne_hidalgo.png
   :align: center


Articles/annonces
=======================

- https://www.lemonde.fr/m-le-mag/article/2023/09/15/a-paris-le-jardin-villemin-rebaptise-en-memoire-de-l-iranienne-mahsa-amini_6189591_4500055.html
- https://www.institutkurde.org/info/inauguration-du-jardin-mahsa-jina-amini-1232552306

PARIS. Inauguration du Jardin Mahsa Jîna Amini
-------------------------------------------------

- https://kurdistan-au-feminin.fr/2023/09/08/paris-inauguration-du-jardin-mahsa-jina-amini/

PARIS – Après la citoyenneté d’honneur de Paris accordée à Jina Mahsa Amini,
une jeune Kurde tuée à Téhéran par la police des mœurs à cause d’un
« voile inapproprié » en septembre 2022, un square parisien portera
également son nom comme l’avait promis la Maire de Paris, Anne Hidalgo.

Le samedi 16 septembre 2023, la maire de Paris, Anne Hidalgo dirigera
la cérémonie d’inauguration du « Jardin Villemin -Mahsa Jina Amini »,
à l’occasion du premier anniversaire du meurtre de Jina Amini par la
police des mœurs iranienne à Téhéran pour un voile « mal porté ».

Lors de l’inauguration, Anne Hidalgo sera accompagnée par la maire du
10e arrondissement, Alexandra CORDEBARD, ainsi que par ses adjoints
Arnaud NGATCHA, Laurence PATRICE, Jean-Luc ROMERO-MICHEL.

Jina Amini est une jeune femme kurde tuée le 16 septembre 2022 à Téhéran
à par la police des mœurs pour un voile « mal porté », devenant l’étincelle
qui a mis le feu au poudrier islamo-fasciste iranien…

**Jina Mahsa Amini a également été citoyenne d’honneur de Paris à titre posthume**.

RDV le samedi 16 septembre 2023, à 10 heures

ADRESSE
105 Quai de Valmy
75010 PARIS
Métro: Gare de l’Est


