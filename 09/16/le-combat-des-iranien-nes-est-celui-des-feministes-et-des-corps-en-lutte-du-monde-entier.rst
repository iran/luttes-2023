.. index::
   pair: Assemblée Féministe Transnationale ; Appel en français


.. _apppel_transnational_2023_09_16_fr:

======================================================================================================================================
2023-09-16 **Le combat des Iranien·nes est celui des féministes et des corps en lutte du monde entier (français)**
======================================================================================================================================

- https://linktr.ee/ass_feministe_transnat
- https://www.instagram.com/assfemtransnat/
- https://blogs.mediapart.fr/les-invites-de-mediapart/blog/160923/le-combat-des-iranien-nes-est-celui-des-feministes-et-des-corps-en-lutte-du-monde-enti
- https://framaforms.org/solidarite-feministe-transnationale-avec-le-soulevement-femme-vie-liberte-en-iran-transnational


Autres références
===================

- https://www.europe-solidaire.org/spip.php?article67857


Préambule
=============

.. figure:: images/linkertree_ass_feministe.png
   :align: center

   https://linktr.ee/ass_feministe_transnat


**La révolution iranienne ne se contente pas de s’opposer à la politique
mortifère de la République Islamique, elle trace un projet de société
post-capitaliste, solidaire et émancipateur**

Introduction
=============

À l'initiative de l'Assemblée Féministe Transnationale, cette tribune de
solidarité avec le soulèvement iranien « Femme*, Vie, Liberté », signée
par une multitude de collectifs et personnalités du monde entier, affirme
que malgré l'épuisement, il est possible d'organiser nos forces pour
changer le cours des choses.

« La révolution iranienne ne se contente pas de s’opposer à la politique
mortifère de la République Islamique, elle trace un projet de société
post-capitaliste, solidaire et émancipateur. »


Sepideh Gholian
==================

Pour :ref:`Sepideh Gholian <akrami_iran:sepideh_gholian>`, journaliste iranienne spécialiste du droit du travail,
défenseuse du mouvement syndical, incarcérée depuis 2018.
Depuis sa détention, elle couvre la situation des femmes en prison à travers
des lettres et des témoignages.
Militante du mouvement Femme* Vie Liberté, elle est réincarcérée le 21 mars 2023,
quatre heures après avoir été libérée. Son courage et sa détermination
inspirent les féministes du monde entier. Ils rappellent le niveau auquel
se situe désormais la résistance.


Le 16 septembre 2022, Jina Mahsa Amini, une jeune femme kurde de 22 ans,
est morte à Téhéran sous les coups de la police des mœurs.
Ce meurtre a été le point de départ d’un soulèvement qui a secoué l’Iran
et traversé le monde.
Parti du Kurdistan, le cri « Femme*, Vie, Liberté » a entraîné la rue
iranienne dans un élan insurrectionnel et vital, global, protéiforme,
qui ne demandait rien et exigeait tout.

Au fil des mois et face à une répression sanguinaire, l’insurrection n’a
cessé de se transformer. Elle a fait émerger un réseau inouï de solidarité :
manifestations spontanées organisées par quartiers, voisins laissant
leurs portes ouvertes pour permettre la fuite des manifestant·e·s,
manifestations de nuit devant les prisons pour s’opposer aux exécutions,
grèves des commerçant·es…
Les organisations syndicales ont réclamé dans un communiqué unitaire historique[1]
des conditions de travail décentes, la fin des politiques écocides, de
l’armement nucléaire, de la privatisation des espaces naturels; mais aussi
l’égalité politique pour les femmes, les minorités ethniques, nationales
et les LGBTQIA+ – qui constituent  par ailleurs un des « fers de lance
de la révolution »[2].

**C’est que la révolution féministe iranienne est une révolution totale**.

La question n’est pas de porter ou non le voile. On ne le répètera jamais
assez dans le contexte islamophobe français : c’est aux femmes de le décider
pour elles-mêmes.
Ce qui est en jeu dans le voile obligatoire en Iran, c’est le contrôle
et l’assujettissement de tous les corps par l’État, dans l’objectif, pour
une minorité, de s’accaparer les ressources.

La République Islamique gouverne à travers un apartheid de genre et un
racisme d’État. Elle ne tient que par le déploiement effréné d’un maintien
de l’ordre ciselé selon les coordonnées raciales des populations à mettre
au pas.
Toutes ces techniques infusent l’économie coloniale globale.

Les vies ne se valent pas : cette réalité a été rappelée en France en
juin 2023 par le meurtre de Nahel et la répression sanglante des mouvements
de révolte qui ont suivis.
Nous la retrouvons à toutes les échelles : des côtes méditerranéennes que
les pratiques de push-back ont transformées en fosses communes, aux quartiers
populaires d’Europe, de Mayotte, de Guyane, en passant par ceux du Brésil,
de Palestine, du Soudan, du Liban, d’Afghanistan et d’Iran.

Le cœur du féminisme que nous défendons est le combat contre ce continuum
de violences et de déshumanisations à l'œuvre dans le capitalisme.
Tant que nous n’affirmons pas notre voix, le féminisme restera monopolisé
au profit d’un discours qui légitime cet ordre. Cela a été le cas lors de
l’année écoulée : les pouvoirs occidentaux n’ont eu alors à la bouche
qu’admiration pour le « courage des femmes iraniennes », tout en déroulant
le tapis rouge à un féminisme libéral, islamophobe, et transphobe[3],
qui prenait bien soin de séparer la lutte pour les droits des femmes de
celles contre l’ensemble des oppressions contestées par les soulèvements
révolutionnaires en Iran. Dans leurs jeux de pouvoir internationaux, ces
mêmes gouvernements occidentaux tirent aujourd’hui profit de la déstabilisation
de la République Islamique par la rue iranienne, tout en abandonnant
cette dernière à des vagues alarmantes d’exécutions, d’arrestations et
de tortures.
Il n’est jamais apparu aussi clairement que l’émancipation des peuples
est un enjeu inexistant sur l'échiquier international.
C’est pourquoi le silence féministe n’est pas une option, et l’ignorance
n’est pas une excuse.

Si les chemins des révolutions sont impossibles à décréter, il n’en reste
pas moins essentiel de pratiquer l’échange des savoirs et des savoir-faire
issus de résistances locales, d’entretenir des réseaux de solidarité
concrète, de tisser la trame d’un peuple mobilisé à l’échelle mondiale.

Il y a urgence à apprendre de l’endurance et des modalités du mouvement
« Femme* Vie Liberté », et à soutenir les camarades iranien·nes face à
la répression.
Car ce à quoi nous avons à faire ici comme là-bas, ce sont, sous des
modalités différentes, des appareils d’États aux mains de franges
radicalisées de la bourgeoisie, dont les discours, religieux ou laïcs,
recouvrent de plus en plus mal un projet similaire et concurrent de
captation des richesses et d’exploitation de tout ce qui est vivant.

Aujourd’hui, après avoir traversé en France comme en Iran une année de
lutte sociale, nous, militant·es de différentes organisations, relié·es
par des préoccupations féministes anticapitalistes, savons comme la lutte
dans le rapport de forces actuel est épuisante.
Cet épuisement fait partie intégrante des techniques de gouvernement contre
les peuples.

Nous sommes jeté·es collectivement dans un chaos climatique, nos avenirs
hypothéqués par les catastrophes, notre présent étouffé par le stress,
la répression, le profilage racial, les corps épuisés par le travail,
la pauvreté, l’illégalité, le manque de soin et de considération.

Il ne nous est jamais apparu aussi clairement que les discours sécuritaires,
dont nous abreuvent quotidiennement les médias possédés par une oligarchie
réactionnaire, désignent en réalité la sécurité des pelouses de golfs.
Sécurité pour le capital jusqu’à ce que le monde crève.

Ce constat ne doit pas nous faire perdre de vue qu’il est non seulement
nécessaire, mais possible d’organiser nos forces et de changer le cours
des choses.

**Le virage est serré, pas facile, mais faisable**.

Et nous commençons par affirmer qu’il consiste depuis notre position à
sortir d’urgence le féminisme européen du déni, à affronter et combattre
vivement son histoire coloniale, et à orienter nos pratiques vers une
solidarité et une réflexion transnationale.

La révolution iranienne ne se contente pas de s’opposer à la politique
mortifère de la République Islamique, elle trace un projet de société
post-capitaliste, solidaire et émancipateur.

Elle est une leçon de mouvement, de réinvention politique et théorique,
et c’est pourquoi le combat des Iranien·nes est celui des féministes et
des corps en lutte du monde entier. Femme* Vie Liberté.

.. youtube:: ZbSUxSvEDPY


[ndlr : Femme* : toutes les femmes, au sens non biologique du terme]


[1] `“Femme, Vie, Liberté : déclaration des revendications minimales <https://blogs.mediapart.fr/les-invites-de-mediapart/blog/310523/pour-une-societe-degalite-et-de-liberte-nous-soutenons-les-iranien-nes>`_
     des organisations indépendantes syndicales et civiles d’Iran ”, February 15th 2023

    - :ref:`iran_luttes:revendications_2023`

[2] `Katayoun Jalilipour, "Les LGBTQI sont le fer de lance de la révolution <https://gal-dem.com/lgbtqi-people-forefront-iran-revolution/>`_
     ils ne devraient pas être oubliés”. Gal-Dem, December 23rd 2022

[3] A Nantes en avril 2023 se tient un colloque concernant le droit des
    femmes et en soutien aux afghanes et iraniennes.
    Le « Comité Laïcité République » a fini `par  renoncer à inviter
    Marguerite Stern, ancienne Femen connue pour ses propos transphobes <https://www.ouest-france.fr/pays-de-la-loire/nantes-44000/apres-la-polemique-autour-de-marguerite-stern-le-colloque-sur-le-droit-des-femmes-a-nantes-annule-f699f7ec-d395-11ed-8669-5c45fd74f594>`_.


Vous pouvez signer cette tribune, en cliquant `ici <https://framaforms.org/solidarite-feministe-transnationale-avec-le-soulevement-femme-vie-liberte-en-iran-transnational>`_ (https://framaforms.org/solidarite-feministe-transnationale-avec-le-soulevement-femme-vie-liberte-en-iran-transnational)
