.. index::
   pair: Framaform ; Solidarité féministe transnationale avec le soulèvement Femme* Vie Liberté en Iran


.. _framaform_apppel_transnational_2023_09_16_fr:

========================================================================================================================================================================================
2023-09-16 **Solidarité féministe transnationale avec le soulèvement Femme* Vie Liberté en Iran / Transnational feminist solidarity with the Woman Life Freedom uprising in Iran**
========================================================================================================================================================================================

- https://framaforms.org/solidarite-feministe-transnationale-avec-le-soulevement-femme-vie-liberte-en-iran-transnational
- https://linktr.ee/ass_feministe_transnat
- https://www.instagram.com/assfemtransnat/

Version française
=====================

Chèr·es allié·es,

En cette rentrée, suite à l’année et l’été de lutte en France, et pour
marquer les un an du soulèvement féministe en Iran, l’Assemblée Féministe
Transnationale propose cette tribune à signatures (tribune à lire en pièce
jointe ouverte aux collectifs et aux signatures individuelles).

Elle a vocation à être publiée sur Mediapart à la date anniversaire du 16/09.

En effet, la mort de Jina Masha Amini en septembre 2022 a été le départ
d’une révolte qui s’est propagée du Kurdistan aux recoins de l’Iran.

Elle a trouvé un écho dans le monde où les colères se propagent, comme
les réponses au meurtre de Nahel et de tous les autres se sont étendues
des banlieues aux villes et villages, bravant des jours de lourde répression
les empêchant de s’installer sur un temps long.

A l’approche de l’anniversaire de la mort de Jina, de nombreuses arrestations,
notamment d'activistes féministes, ont lieu et de très nombreux feux de
forêt au Kurdistan iranien semblent être un fait de vengeance : mais
ceux-ci unifient les gens qui se réunissent pour les éteindre.

Dans un contexte où des structures des États Unis et de la Russie ont
inondé l’Europe de millions d’euros en soutien aux anti-lgbt, où la
militarisation prend le budget des écoles et hôpitaux, et où le féminisme
tente d’être récupéré par les islamophobes, nous pouvons, depuis notre
place, être présent·es, démontrer de la solidarité, visibiliser, exprimer,
pour contrer la désinformation.

Sur l’instant ou sur le territoire de la durée

Pour une résistance vivante et constructive

Tribune ouverte aux signatures individuelles et aux collectifs.
Merci de ne pas diffuser ce texte sur les Réseaux Sociaux avant sa publication.

N’hésitez pas à partager le lien dans vos réseaux militants.

Attention : pour publication samedi 16 septembre, merci de bien vouloir
signer avant vendredi 15 à 12h. Les signatures enregistrées après cet
horaire seront publiées quelques jours plus tard.

Rédactrices : Groupe de travail pour une Assemblée Féministe Transnationale
(@AssFemTransnat sur Twitter, assfemtransnat sur Instagram, linktr.ee/ass_feministe_transnat)


Version anglaise
=====================

Dear friends and allies,

Following a year of struggle and a summer of uprisings in France, and to
mark the one-year anniversary of the feminist uprising in Iran, a group
of feminists working towards transnational solidarity initiated this
manifesto, to be published in several languages on the anniversary date
of 16/09.

The death of Jina Masha Amini in September 2022 was the starting point
for a revolt that spread from Kurdistan to the far corners of Iran.

It has echoed around the world, where anger is spreading, just as the
response to the murder of Nahel and all the others in France has spread
from the suburbs to towns and villages, braving days of heavy repression
to prevent them from taking hold over the long term.

As the anniversary of Jina's death approaches, many arrests are taking place,
particularly of feminist activists, and many forest fires in Iranian
Kurdistan seem to be a revenge act: but they unite the people who come
together to extinguish them.

In a context where structures in the United States and Russia have flooded
Europe with millions of euros in support of anti-lgbt groups, where militarisation
is taking over the budgets of schools and hospitals, and where feminism
is trying to be co-opted by Islamophobes - we can, from our place, be
present, show solidarity, make ourselves visible, express ourselves to
counter disinformation.

In the moment or over the long term

For a living and constructive resistance

Open to individual and collective signatures. Please do not circulate this
text on social media before publication.

Feel free to share with your activist networks.

Caution : for a publication the 16th, thanks to sign until the 15th at noon.
Signatures registred after will be published a few days after.

Writers : Working Group for a Transnational Feminist Assembly
(@AssFemTransnat on Twitter, assfemtransnat on Instagram, linktr.ee/ass_feministe_transnat)

Fichiers attachés
===================

- :download:`Solidarité féministe transnationale avec le soulèvement Femme* Vie Liberté <https://framaforms.org/sites/default/files/forms/files/tribune_16_09-_francais.pdf>`
- :download:`English - transnational feminist solidarity Woman* Life Freedom / Deutsch - feministische Solidarität Frau* Leben Freiheit <https://framaforms.org/sites/default/files/forms/files/tribune_16_09_english_-_deutsch_0.pdf>`
- :download:`solidaridad feminista transnacional Mujer* Vida Libertad / Português - Solidariedade Feminista Mulheres*, Vida, Libe <https://framaforms.org/sites/default/files/forms/files/tribune_16_09_espanol-portugues.pdf>`
- :download:`Solidarietà femminista transnazionale con la rivolta di Donna* Vita Libertà in Iran <https://framaforms.org/sites/default/files/forms/files/tribune_it0409_rev.pdf>`

