.. index::
   pair: Colloque ; La situation des femmes et des droits humains au Kurdistan et en Iran

.. _situation_2023_09_30:

============================================================================================================================================
Samedi 30 septembre 2023 **Colloque: La situation des femmes et des droits humains au Kurdistan et en Iran** Paris 10e de 15h à 17h30
============================================================================================================================================

- https://www.institutkurde.org/activites_culturelles/event-626/


La situation des femmes et des droits humains au Kurdistan et en Iran
un an après le début du mouvement **Femme, Vie, Liberté**

Organisé par l’Institut kurde de Paris, le samedi 30 septembre 2023
de 15h à 17h30

::

    La Mairie de Paris 10e
    72 rue du Faubourg Saint-Martin, 75010 Paris


.. raw:: html

   <iframe width="800" height="400" src="https://www.openstreetmap.org/export/embed.html?bbox=2.3552665114402775%2C48.87107347965152%2C2.3588070273399357%2C48.872507784604345&amp;layer=mapnik"
       style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=19/48.87179/2.35704">Afficher une carte plus grande</a></small>


Pour des raisons de sécurité l’inscription préalable est obligatoire
avant le 29 septembre 2023, à 15h


.. figure:: images/situtation_des_femmes_en_iran_et_kurdistan.png
   :align: center


Programme
===========

14h30 – 15h00 — Accueil des participants
-------------------------------------------

15h00 – 15h15 — Mots de bienvenue
-------------------------------------

- M. Rémi FÉRAUD, sénateur de Paris, ancien maire du 10ème arrondissement
- Melis KAYA, chargée de mission pour les droits humains, Institut kurde de Paris

15h15 – 17h00 — TABLE RONDE
----------------------------

Modérateur : Hamit BOZARSLAN, directeur d'études à l'École des hautes
études en sciences sociales (EHESS).

Intervenants
+++++++++++++++++

- Irène ANSARI, coordinatrice de la Ligue des femmes Iraniennes pour la démocratie
  La situation actuelle des femmes en Iran

- Asso HASSAN ZADEH, juriste et universitaire
  La situation au Kurdistan iranien

- Marie LADIER-FOULADI, directrice de recherches à l'EHESS-CETOBAC
  Femme, Vie, Liberté : Le corps de la femme comme terrain de lutte
  et de résistance

- Azadeh THIRIEZ-ARJANGI, enseignante à Science-PO Rennes
  Iran et la grandeur tragique de la liberté

17h00 - 17h30 — Débat
-----------------------



Marie LADIER-FOULADI
====================

- https://cetobac.ehess.fr/membres/marie-ladier-fouladi
- https://www.cairn.info/publications-de-Marie-Ladier-Fouladi--15089.htm



2023-04- La génération « Femme, vie, liberté » Marie Ladier-Fouladi, Propos retranscrits par Jonathan Chalier
------------------------------------------------------------------------------------------------------------------

- https://www.cairn.info/revue-esprit-2023-4-page-39.htm

Dans Esprit 2023/4 (Avril), pages 39 à 44.

Azadeh Thiriez-Arjangi
==========================

2023-04 Femme, vie, liberté ! Un mouvement révolutionnaire en Iran
-----------------------------------------------------------------------

- https://www.cairn.info/revue-esprit-2023-4-page-35.htm

Quelques mois après la mort de Mahsa Amini, ce dossier, coordonné par
Azadeh Thiriez-Arjangi, propose un éclairage sur le mouvement révolutionnaire
« Femme, vie, liberté » en Iran.

La violence de la République islamique, un régime corrompu qui réprime
son peuple et lui impose une culture de la mort, a conduit les Iraniens
à proclamer leur liberté, ouvrant un horizon d’espérance.

:download:`Pages 35 à 38 <pdfs/ESPRI_2304_0035.pdf>`
