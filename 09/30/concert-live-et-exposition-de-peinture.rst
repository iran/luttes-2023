.. index::
   pair: Concert ; 2023-09-30
   pair: Concert ; Arash Sarkechik (2023-09-30, annulé)
   ! Arash Sarkechik

.. _concert_live_2023_09_30:

====================================================================================================================================================
Samedi 30 septembre 2023 **Concert live et exposition de peinture** avec Arash Sarkechik (annulé), Ghazal Tahmasebi, Shayan Karimi, Marie Gilles
====================================================================================================================================================

- :ref:`iran_luttes:solidiran`
- :ref:`iran_luttes:solidiran_2023_09_30`
- :ref:`iran_exposition:exposition_iran`


Présentation
================

.. figure:: images/olivier_messiaen.png
   :align: center

A l'occasion du soulèvement Femme Vie Liberté

- Chanteur: :ref:`Arash Sarkechik <arash_sarkechik>`  (ANNULE)
- Chanteuse: Ghazal Tahmasebi
- piano : Shayan Karimi
- flute : Marie Gilles


Date
    Samedi 30 SEPTEMBRE 2023

Exposition de Peinture : 18h - 19h
Concert Live : 19h- 21h

Lieu
    SALLE OLIVIER MESSIAEN
    1 Rue du Vieux Temple 38000 Grenoble


.. raw:: html

   <iframe width="800" height="400" src="https://www.openstreetmap.org/export/embed.html?bbox=5.73139786720276%2C45.19256257333884%2C5.734938383102418%2C45.19409930410641&amp;layer=mapnik"
       style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=19/45.19333/5.73317">Afficher une carte plus grande</a></small>


.. figure:: images/annonce_olivier_messiaen.png
   :align: center


Le 16 septembre 2022, après trois jours de soins intensifs,
Mahsa Jina Amini a malheureusement succombé à une
blessure à la tête infligée par les forces des mœurs.

Afin de commémorer son anniversaire et de nous remémorer
les événements tragiques survenus il y a un an, où plus de 600
innocents ont été victimes de violences lors de manifestations
pacifiques, nous avons pris l'initiative d'organiser un concert.

**Notre principal objectif est d'apporter un soutien concret aux
familles des victimes et aux manifestants blessés en affectant
intégralement les bénéfices de ce concert à leur cause**.

Toutes les informations relatives à l'utilisation des fonds
collectés seront publiées sur le site :ref:`solidiran.fr <iran_luttes:solidiran>`

- Tarif super réduit : 5€
- Tarif réduit : 10€
- Tarif recommandé : 15€


.. _arash_sarkechik:

**Arash Sarkechik**
=====================

- https://www.sarkechikmusic.com/


Lien Nima Sarkechik ?
---------------------------

- :ref:`iran_2024:fraternite_20245_03_03`

- http://www.nimasarkechik.com/
