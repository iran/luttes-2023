.. index::
   pair: Documentaire ; Femme, vie, liberté - Une révolution iranienne
   pair: Réalisatrice ; Claire Billet
   pair: Réalisateur ; Mohamad Hosseini


.. _auposte_femme_vie_liberte_chowra_2023:

====================================================================================================================
2023-09-19 **Femme ! Vie ! Liberté ! Échos d’un soulèvement révolutionnaire en Iran** avec Chowra Makaremi
====================================================================================================================

- https://www.auposte.fr/femme-vie-liberteechos-dun-soulevement-revolutionnaire-en-iran-avec-chowra-makaremi_livre/


Introduction
==============

Anthropologue, iranienne d’origine, femme engagée, nos services connaissent
bien Chowra, déjà convoquée Au Poste il y a quelques mois.

Elle nous revient, :ref:`un livre sous le bras <livre_femme_vie_makaremi_2023>`, sur un événement majeur: le
soulèvement des femmes, et des hommes, en Iran. Une révolte qui ne cesse
pas, malgré la répression. C’est une joie de la re-recevoir.


Avec Chowra, **nous avons parlé de son travail, des “reves du futur”
qu’il faut documenter**, écrire, du retournement révolutionnaire de
l’égocentrisme d’Instagram, de qui et comment et quand la révolution
peut se faire.
**Passionnée, passionante**.

Présentation de son ouvrage par La Découverte
===================================================

- :ref:`livre_femme_vie_makaremi_2023`

Depuis septembre 2022 des femmes et des hommes, souvent jeunes, se sont
engagés en Iran dans un travail de conquête politique et d’ouverture des
possibles qui nous remue à un endroit précis : celui de la possibilité,
toujours, du soulèvement.

Voici la chronique à distance d’une révolte qui s’est installée dans la
durée avec surprise, audace et incertitude.
Ce long automne insurrectionnel convoque aussi d’autres séquences de
l’histoire iranienne, se trouve éclairé par d’autres mouvements, d’autres
mémoires de luttes et de violences.
Une histoire longue du pouvoir et de la résistance, que Chowra Makaremi
connaît par son passsé familial, par ses recherches également, en tant
qu’anthropologue attentive aux contre-archives et aux émotions collectives.

L’autrice donne aux événements une profondeur de champ qui permet d’en
identifier les genèses multiples, et de saisir le basculement révolutionnaire
irréfutable qu’ils représentent.

Elle compose une archive à la **lumière orange des feux de rue, devenus le
symbole d’une révolte qui se vit comme une combustion de colère, une
profanation, une contagion**.
