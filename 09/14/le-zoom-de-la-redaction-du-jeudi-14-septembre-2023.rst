.. index::
   pair: France-inter; 2023-09-14

.. _france_inter_2023_09_14:

================================================================================================
2023-09-14 **L’Iran se dévoile, quand le peuple se soulève** France-inter
================================================================================================

- https://www.radiofrance.fr/franceinter/podcasts/le-zoom-de-la-redaction/le-zoom-de-la-redaction-du-jeudi-14-septembre-2023-1757147


Préambule
============

**Il y a un an, suite à la mort de Mahsa Amini, arrêtée pour une mèche de
cheveux qui dépassait de son voile, le peuple iranien se soulevait.
La 35ème édition du festival "Visa pour l'Image", donne à voir de l’intérieur
ce mouvement grâce à des photos et des vidéos extraites des réseaux sociaux**.

Un haut-parleur d'où s'échappe la clameur d'un peuple en colère. Au mur,
une cinquantaine de photos, dont celle qui est devenue le symbole du
soulèvement iranien, l'affiche de la 35ᵉ édition de Visa pour l'image.

On y voit une jeune fille sur le toit d'une voiture, les cheveux lâchés,
les bras brandis en signe de victoire.
Face à elle, une foule à perte de vue. "Sur cette photo, on est au 40ᵉ jour
qui a suivi la mort de Mahsa Amini", commente Marie Sumala, rédactrice photo
au journal le Monde et commissaire de l’exposition.
"C’est un jour important dans la tradition musulmane puisqu'il clôt le deuil.

Et c'est aussi un moment que le régime attendait, en espérant que le
soulèvement s'affaiblisse un peu. Cette photo montre qu'il n'en est rien",
ajoute celle qui s'est battue pendant des mois pour donner à voir ce
qui se passait en Iran.

Un cliché anonyme, comme la plupart de ceux exposés ici. Marie Sumala et
sa co-commissaire, la journaliste franco-iranienne Ghazal Golshiri, du
service international du Monde, les ont débusqués sur les réseaux sociaux
dès les premiers rassemblements dans les rues de Téhéran, seule façon
d'éclairer un mouvement populaire dans un pays sous cloche.

"Pour une photo, à minima, il faut une source, un lieu, une date"
=====================================================================

"Depuis septembre 2022, WhatsApp et Instagram sont bloqués en Iran et il
y a eu des périodes de coupure totale d'Internet dans tout le pays",
explique Ghazal Golshiri.

Malgré ces difficultés, les deux journalistes n'ont rien lâché, trouvant
le moyen de vérifier chaque document. "Pour une photo, à minima, il faut
une source, un lieu, une date", développe Marie Sumala qui précise "j'ai
été en contact avec deux experts iraniens, expert d'images, de réseaux
sociaux et surtout de fact-checking.

Ces deux journalistes ont des réseaux partout, parfois des personnes sur
place sont même allées vérifier l'angle, le carrefour, une plaque
d'immatriculation, une météo d'un jour. C'était un ping-pong permanent."

**"C'est une victoire pour la mémoire collective"**
======================================================

Des difficultés qui, pour les deux journalistes, donnent encore plus
d’importance à cette exposition, notamment pour Ghazal Golshiri qui a
été correspondante à Téhéran de 2016 à 2019 :

"La République islamique d'Iran, a toujours eu cette volonté d'imposer
sa propre narration et donc c'est une victoire pour la mémoire collective",
explique-t-elle tandis que Marie Sumala précise qu’"une exposition dédiée
à la résistance des Iraniens par des photographies anonymes signifie
que nous écrivons l'Histoire ensemble, que ce sont des photos pour l'histoire."

Preuve de l’importance des témoignages de ces photos, jamais Visa pour
l'Image n’avait exposé des clichés d'anonymes. Si son directeur,
Jean-François Leroy, a fait une exception, c’est parce qu’"il n'y a pas
de photographe professionnel en Iran ou s'il y en a, ils ne peuvent pas
sortir leur appareil."

Alors oui, dans l’exposition, "il y a des photos mal cadrées parce
qu'elles ont été prises sous des vestes ou sous des foulards.

Il y a des photos de travers, il y a des photos qui ne sont pas nettes.
Mais l'info, elle est là."


