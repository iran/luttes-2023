.. index::
   pair: Bahareh Akrami; 2023-09-14

.. _femme_vie_liberte_baharh_akrami_2023_09_14:

====================================================================================================================
2023-09-14 **Vidéo. “Femme, vie, liberté” : Bahareh Akrami raconte la révolution iranienne en bande dessinée**
====================================================================================================================

- :ref:`iran_luttes:livre_femme_vie_satrapi_2023`
- https://www.courrierinternational.com/video/video-femme-vie-liberte-bahareh-akrami-raconte-la-revolution-iranienne-en-bande-dessinee

Préambule
============

Le roman graphique **Femme, vie, liberté**, conçu sous la direction de
Marjane Satrapi, sort en librairie ce jeudi 14 septembre 2023.

Un an après la mort de Mahsa Amini, l’ouvrage raconte l’année de révolution
que vient de vivre l’Iran.

Pour la dessinatrice Bahareh Akrami, que nous avons rencontrée, il est
crucial de ne pas laisser ce combat tomber dans l’oubli.



Résumé
=========

- https://www.decitre.fr/livres/femme-vie-liberte-9782378803780.html#resume

Joann Sfar - Coco - Mana Neyestani - Catel - Pascal Rabaté - Patricia Bolanos
Paco Roca - Bahareh Akrami - Hippolyte - Shabnam Adiban - Lewis Trondheim -
Deloupy - Touka Neyestani - Bee - Winshluss - Nicolas Wild -
Hamoun

Femme, vie, liberté : avoir vingt ans en Iran et mourir pour le droit
des femmes. Le 16 septembre 2022, en Iran, Mahsa Amini succombe aux coups
de la police des moeurs parce qu'elle n'avait pas "bien" porté son voile.

Son décès soulève une vague de protestations dans l'ensemble du pays, qui
se transforme en un mouvement féministe sans précédent.

Marjane Satrapi a réuni trois spécialistes : Farid Vahid, politologue,
Jean-Pierre Perrin, grand reporter, Abbas Milani, historien, et dix-sept
des plus grands talents de la bande dessinée pour raconter cet évenement
majeur pour l'Iran, et pour nous toutes et nous tous.


À propos de l'auteur
-----------------------

Marjane Satrapi

Biographie de Marjane Satrapi

Un livre dirigé par Marjane Satrapi. Marjane Satrapi est l'autrice de
Persépolis, une série autobiographique, vendue à plus de quatre millions
d'exemplaires et récompensée dans le monde entier.

Après avoir écrit deux autres albums (Broderies, nommé dans la catégorie
du meilleur album au Festival d'Angoulême, et Poulet aux prunes, couronné
par le prix du Meilleur Album d'Angoulême), elle se consacre ensuite au
cinéma puis à la peinture.

Le film Persépolis, coréalisé avec Winshluss, grand auteur de bande dessinée,
qui participe aussi au livre, reçoit deux Césars et est nominé aux Oscars.

Introduction
===============

“Le feu couve toujours sous la cendre : il suffirait d’une étincelle pour
que ça reparte.” Il y a près d’un an, le 16 septembre 2022, la jeune Mahsa
Jina Amini mourait à l’hôpital, trois jours après son interpellation par
la police des mœurs.

L’onde de choc causée par sa disparition déclenchait une révolution qui
dure toujours un an plus tard.

Pour la dessinatrice Bahareh Akrami, que Courrier international a rencontrée,
cet anniversaire est un moment crucial : l’occasion ou jamais d’amplifier
la voix des Iraniens.

C’est l’une des raisons qui l’ont poussée à contribuer à :ref:`l’ouvrage collectif
Femme, vie, liberté, dirigé par Marjane Satrapi, qui paraît ce jeudi
14 septembre 2023 (éditions L’Iconoclaste) <iran_luttes:livre_femme_vie_satrapi_2023>`
