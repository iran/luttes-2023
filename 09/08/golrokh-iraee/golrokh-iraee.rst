

.. index::
   pair: Militante; Golrokh Iraee

.. _golrokh_iraee_2023_09_08:

=======================================================================================================================================
**Golrokh Iraee** «Bien que le régime ait perdu sa légitimité, la position internationale de la République islamique se renforce»
=======================================================================================================================================

.. figure:: images/golrokh_iraee.png
   :align: center


Présentation
===============

Golrokh Iraee est écrivaine et militante pour l’abolition de la lapidation
en Iran. Elle a écrit cette lettre en juin 2023 à la prison d’Evin.

Les textes ont été traduits du persan par Chirinne Ardakani, Sepideh Farsi,
Javad Javaheri et Reihane Taravati.

Après une première condamnation de six ans pour « insulte au sacré » et
« propagande contre l’Etat », l’écrivaine iranienne de 43 ans a été
réincarcérée dans les premières semaines du mouvement ayant suivi la mort
de Mahsa Amini, en septembre 2022.

Ces protestations ont conduit à l’affaiblissement des forces populaires religieuses qui soutenaient le gouvernement
========================================================================================================================

« LA RÉALITE LA PLUS AMÈRE de l’année 2022 pour notre pays a été la
répression violente des protestations qui ont eu lieu dans les rues,
puis l’exécution de plusieurs d’entre nous.

A n’en pas douter, la rue est le seul chemin possible vers la révolution,
la fin de la dictature et du pouvoir autoritaire.

Il existe plusieurs raisons à l’essoufflement des manifestations : absence
d’offre de projet adéquat par les diverses forces politiques alternatives,
manque de structuration de ces forces conjugué à la présence, dans certains
groupes, de dirigeants incompétents et déconnectés de la réalité de la
société iranienne.

Bien que les revendications du mouvement n’aient pas été atteintes, **nous
sommes parvenus à délégitimer le régime qui s’était, au passage, discrédité
depuis longtemps**.


Le mois de septembre 2022 a été un tournant dans l’histoire de l’Iran.
J’ai été témoin des premiers jours de protestations. Le reste m’a été rapporté.

J’ai passé un certain temps à la prison de Qarchak, aux côtés des manifestants
arrêtés. Le profil des manifestants était très varié. Ils étaient issus
de toutes les classes sociales, avec des perspectives et des analyses
politiques différentes, parfois même contradictoires.

Cependant, ils étaient unanimes… unanimes dans leur rejet de la République
islamique. Tous s’accordaient sur des slogans et mots d’ordre simples et
audacieux. Ils faisaient preuve d’une conscience sociale accrue, ce qui
fait de ce mouvement le plus important depuis la fin des années 1990.

**Ces protestations ont conduit à l’affaiblissement des forces populaires
religieuses qui soutenaient le gouvernement**.

Le régime a été confronté à une génération différente de manifestants,
et les tabous de la société ont été brisés.
Les jeunes et les enfants étaient massivement présents dans les rues,
les opposants affrontaient les forces de sécurité de façon récurrente.

Le système était plus que jamais désorienté face à ces nouveaux profils.

Un pas a été franchi
=========================

En l’absence de débouchés politiques, ces manifestants, qui avaient renversé
le mur de la peur dans les rues, se sont trouvés impuissants une fois arrêtés.

Le gouvernement a, pour sa part, tenté de redorer son image au moyen d’amnisties
promotionnelles feintes de compassion.

Alors que l’incapacité des forces de sécurité à contrôler les rues était
évidente, l’absence de structuration politique du mouvement a amené à un
déclin des soulèvements de la rue qui se sont progressivement mués en
appel à célébrer la mémoire des victimes.

La répression a depuis longtemps empêché la formation de tout parti
politique dans le pays, étouffé l’émergence d’organisations populaires
et verrouillé les institutions.
Cela explique, en partie, la difficulté des forces politiques alternatives
à mener une lutte efficace et à développer une analyse pertinente de la
situation.
Les médias, à l’étranger, se sont progressivement fait l’écho de slogans
répétitifs proférés par des personnalités émergentes parfois
inexpérimentées – voire, dans certains cas, opportunistes –, de leaders
incompétents et étrangers à la réalité politique, qui se sont montrés
pendant les manifestations et ont aussitôt disparu.

Incontestablement, un pas en avant important a été franchi et le régime
a définitivement perdu toute légitimité.

Cependant, la structure du pouvoir reste intacte et, en dépit de cette
séquence d’une particulière cruauté, la position internationale de la
République islamique se renforce…


.. _trahison_inter_2023_09_08:

**Impardonnable trahison de la communauté internationale**
==============================================================

En dépit du massacre massif d’opposants dans les rues et des nombreuses
exécutions qui ont été dénoncés par le peuple ainsi que par de nombreux
avocats, **force est de constater que la communauté internationale poursuit,
avec ce régime, les négociations, les accords, les collaborations régionales
et mondiales**.

Cela s’illustre notamment par la facilitation des échanges financiers et
la nomination de représentants de la République islamique au forum social
du Conseil des droits de l’homme des Nations unies.

**Tout cela ne devrait pourtant pas arriver**.

**Le signal envoyé par la communauté internationale aux manifestants iraniens
est qu’ils sont seuls dans leur combat**.

Or, la mobilisation massive, dans la rue, est le seul chemin victorieux
vers la révolution, la fin de la tyrannie et du despotisme.


En ces jours où les équilibres régionaux et mondiaux connaissent des
bouleversements majeurs, **dans un Moyen-Orient en proie à l’instabilité,
au fascisme et au despotisme, inciter les gens à l’inertie, à quitter
la rue et à attendre passivement l’effondrement d’un régime – ce qui ne
sera pourtant possible que par l’action massive et concertée dans la rue –,
est une trahison impardonnable de la communauté internationale**.

Ce soutien implicite et coupable au régime, quelles que soient les
intentions, revient à faire le jeu des forces répressives et sécuritaires
du régime.

Une stratégie de victoire est possible et se dessine : en reconnaissant
nos faiblesses, en renforçant nos atouts, en rejetant les faux leaders
et en éliminant toute pensée hégémonique visant à dominer notre esprit,
en croyant en notre capacité à atteindre nos objectifs à travers les
outils, organisations et formations politiques que nous nous approprions
et qui sont l’expression de la volonté collective.

Quel que soit le chemin, qu’on se le tienne pour dit : nous sommes plus
déterminés que jamais à renverser les fondements de l’oppression. »



