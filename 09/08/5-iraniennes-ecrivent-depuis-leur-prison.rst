.. index::
   pair: Témoignages; 5 iraniennes


.. _temoignages_2023_09_08:

===============================================================================
2023-09-08 **Cinq Iraniennes écrivent depuis leur prison**
===============================================================================


:source: https://www.lemonde.fr/article-offert/omonpkocaxkg-6188463/cinq-iraniennes-ecrivent-depuis-leur-prison-nous-sommes-coupables-du-desir-de-vivre


Préambule
============

Tribune« Le Monde » publie cinq textes écrits et transmis clandestinement
par des militantes iraniennes des droits humains.

Quatre d’entre elles sont incarcérées à la prison d’Evin, la cinquième y
a été dans le passé.

Situé dans le nord de Téhéran, l’établissement a reçu de nombreux opposantes
et opposants du mouvement de protestation qui a suivi la mort de Mahsa Amini,
en septembre 2022.


Introduction
==============

Evin. Les dissidents iraniens appellent communément cette prison « l’université d’Evin »,
tant elle a vu passer, depuis plus de cinquante ans, d’intellectuels,
d’écrivains, d’acteurs, de réalisateurs, d’étudiants, de syndicalistes,
d’avocats et de militants politiques.

Située dans le nord de Téhéran, Evin représente des décennies de lutte
des Iraniens de tous horizons pour la démocratie.

A l’approche du premier anniversaire du mouvement de contestation survenu
après la mort, le 16 septembre 2022, de la jeune Mahsa Amini pendant sa
garde à vue pour son apparence jugée « pas assez islamique », Le Monde
publie les lettres de cinq prisonnières politiques d’Evin, acheminées
jusqu’à nous au fil d’un long parcours, malgré les risques.

Ces lettres témoignent du caractère inédit, par son étendue et par sa
continuité, du soulèvement dont le slogan est rapidement devenu « Femme, vie, liberté ».

S’il se poursuit sous de nouvelles formes, il a fait environ 500 victimes,
et des dizaines de milliers de manifestants ont été arrêtés.
Beaucoup sont encore derrière les barreaux.

Aujourd’hui, partout sur son territoire, la République islamique d’Iran
procède à des arrestations parmi les militants politiques et les familles
des victimes de la répression, afin de prévenir une nouvelle vague de
contestation.
La situation économique du pays est extrêmement difficile : l’inflation,
frôlant les 60 % par an, bat des records. Le pouvoir ne semble guère prêt
à lâcher du lest sur les libertés individuelles, parmi lesquelles l’obligation
du port du voile par les femmes, ajoutant au ras-le-bol généralisé de la
population.
Dans ce contexte volatil et explosif, le risque d’un nouveau mouvement
de contestation n’est pas négligeable.

Alors qu’à l’étranger **l’opposition iranienne est morcelée et sans projet
politique**, les lettres de ces femmes sont porteuses d’espoir et montrent
que la voie vers un Iran démocratique viendra non de l’extérieur, mais
de l’intérieur du pays.


.. toctree::
   :maxdepth: 3

   zeinab-jalalian/zeinab-jalalian
   narges-mahammadi/narges-mahammadi
   sepideh-gholian/sepideh-gholian
   niloufar-bayani/niloufar-bayani
   golrokh-iraee/golrokh-iraee
