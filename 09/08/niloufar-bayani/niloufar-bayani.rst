

.. index::
   pair: Luttes ; Ecologie
   pair: Militante écologiste; Niloufar Bayani
   pair: Militant Ecologiste ; Kavous Seyed-Emami

.. _niloufar_bayani_2023_09_08:

===============================================================================================================================
**Niloufar Bayani** «La répression contre les défenseurs de l’environnement se poursuit inlassablement en Iran»
===============================================================================================================================

.. figure:: images/niloufar_bayani.png
   :align: center


Présentation
===============

Niloufar Bayani est biologiste, spécialisée dans la préservation de
l’environnement. Elle a écrit cette lettre en juin 2023 à la prison d’Evin.


La militante écologiste et chercheuse de 37 ans a été condamnée en 2020
à dix ans de prison pour « espionnage », alors qu’elle travaillait pour
un programme des Nations unies en faveur de l’environnement.


.. _kavous_seyyed_emani_2023_09:

Dans cette lettre, je veux dénoncer la mort tragique de Kavous Seyyed-Emami (le 8 février 2018)
===================================================================================================

- https://www.lemonde.fr/proche-orient/article/2018/02/12/le-suicide-suspect-d-un-militant-ecologiste-emprisonne-en-iran_5255450_3218.html

« À PLUS DE MINUIT, IL ÉTAIT CONTRAINT de chanter et de danser sur une
chanson pop de variété. Combien étaient-ils à l’encercler ?
Cinq à dix interrogateurs, on ne le saura jamais, à se tenir autour de lui,
applaudissant, vociférant et le forçant à accomplir cette terrible danse,
dans une sordide salle d’interrogatoire. Il avait sans doute les yeux bandés,
tout comme son collègue qui se trouvait dans la pièce adjacente, pour
qu’il souffre, à son tour, d’entendre pareil supplice infligé à son ami.

Le collègue a survécu, mais pas lui. Ce fut la dernière fois que nous
obtenions de ses nouvelles. Quelques jours plus tôt, il avait été arrêté
devant un commissariat de police, et emmené la tête dans un sac plastique.

Puis on le vit, une dernière fois, lorsque son corps sans vie fut montré
à son épouse, à la morgue, deux semaines seulement après son arrestation.

A ce jour, la cause de son décès reste incertaine. Kavous Seyed-Emami a
tout donné, y compris sa vie, pour préserver la nature, qui était sa passion.

Professeur de sociologie politique, il a inspiré, à chaque étape de son
existence, de nombreuses personnes, qui sont devenues, grâce à lui, militants
écologistes.
Ce fut mon cas. J’ai trouvé ma vocation en devenant biologiste spécialisée
dans la préservation de l’environnement.

Aujourd’hui, je suis militante écologiste, incarcérée depuis cinq ans et
demi au sein de la prison d’Evin, à Téhéran. Il me reste plus de quatre ans
à purger.
Dans cette lettre, je veux dénoncer la mort tragique de Kavous Seyyed-Emami
(le 8 février 2018), directeur de la Persian Wildlife Heritage Foundation,
une ONG pour la protection de la faune, où je travaillais.

Je veux dénoncer la torture psychologique, les passages à tabac et le
harcèlement sexuel infligé à mes sept autres collègues condamnés injustement : Taher Ghadirian,
Houman Jowkar, Sepideh Kashani, Amir-Hossein Khaleghi, Abdolreza Kouhpayeh,
Sam Rajabi et Morad Tahbaz.

Je veux dénoncer également les mauvais traitements dont j’ai été moi-même victime.

Décourager, délégitimer
==========================

Je veux encore dénoncer le traitement brutal et inhumain réservé à nos
volontaires locaux issus de villages pauvres du sud de l’Iran : Hassan Ragh,
Arat Zareh et Mohammad Saleh-Ahmad.

J’ai été témoin des mauvais traitements infligés aux photographes animaliers
et aux cinéastes documentaristes Alireza Farhad-Zadeh et Morteza Arianejad,
qui ont été maintenus en confinement solitaire en même temps que moi et
que d’autres de nos collègues.
Ensemble, nous avons été condamnés à un total de soixante-huit ans de prison
et à 1 million de dollars [930 000 euros] d’amendes, sur la base d’un chef
d’accusation fantaisiste de « collaboration avec un Etat ennemi »
(comprenez : les Etats-Unis d’Amérique).

Quatre d’entre nous ont passé deux années, entre notre arrestation et le
verdict, dans la crainte d’une condamnation à mort.
Aucune preuve n’a jamais été présentée contre nous. Aucune investigation
n’a été jugée nécessaire, et un simulacre de procès a eu lieu pour
criminaliser notre mouvement écologiste.

Ce mouvement modeste mais croissant au sein de la société civile iranienne
a été présenté comme une menace pour la sécurité nationale.
Au moins quarante autres arrestations et détentions ont eu lieu dans l’unique
but de réduire au silence les militants écologistes, de décourager le mouvement
et de délégitimer le combat écologique.

Lorsque j’ai été arrêtée, je n’étais pas familière avec le terme de
« militant écologiste ». A présent, je sais que le funeste destin qui se
dessine n’est pas réservé à notre espèce ni à notre pays.

**Nous sommes au cœur de la sixième extinction massive d’espèces de l’histoire
de notre planète, et nous nous trouvons confrontés à un changement climatique
de plus en plus intense, tous deux causés par l’activité humaine.
Pourtant, à travers le monde, des experts environnementaux, des chercheurs,
des volontaires, des militants, des témoins et des manifestants qui luttent
contre la destruction de la nature sont poursuivis, intimidés, assassinés
ou tout simplement portés disparus**.

Menaces constantes
=======================

Selon Global Witness, entre 2012 et 2021, au moins 1 733 défenseurs de
l’environnement et de la planète ont été tués.

En Iran, en 2018, le militant écologiste Sharif Bajour et trois autres
écologistes sont morts dans des circonstances suspectes tandis qu’ils
luttaient contre des incendies de forêt dans le Kurdistan transfrontalier.

Bajour, membre de l’ONG Chia Green, avait été arrêté à trois reprises.
La même année, Farshid Hakki, professeur d’université et militant écologiste,
a été retrouvé mort à l’intérieur de sa voiture calcinée, à l’ouest de
Téhéran.

Selon Amnesty International, rien qu’en 2018 au moins soixante-trois militants
écologistes ont été arrêtés en Iran.


Pour de nombreux experts, les menaces constantes qui pèsent sur les écologistes
ne laissent d’autre choix que de quitter le pays. `Kaveh Madani <https://www.lemonde.fr/proche-orient/article/2018/04/19/un-universitaire-iranien-rentre-au-pays-pour-servir-l-etat-et-prend-la-fuite_5287886_3218.html>`_ en est
l’exemple le plus connu. Il avait quitté son prestigieux poste à l’Imperial
College de Londres pour un poste gouvernemental au département de
l’environnement iranien.
Ses efforts en faveur de la protection de l’environnement en Iran n’ont
pas pu se poursuivre : entravé et intimidé, il a été contraint de quitter
le pays, quelques mois plus tard à peine.

La répression contre les défenseurs de l’environnement se poursuit inlassablement
en Iran, à travers tout le pays. Les rapports officiels énumèrent ainsi :
dix personnes arrêtées en 2014 au Kurdistan, un nombre élevé de manifestants
arrêtés à Ispahan en 2021 alors qu’ils dénonçaient l’assèchement de la
rivière Zayandeh Rud, vingt-deux manifestants emprisonnés en 2022 en raison
de leur opposition au projet « non durable » de transfert d’eau à Charmahal
et Bakhtiari.
Alors que ces militants n’avaient utilisé, en guise de protestation, que
des chansons et des danses folkloriques.

Il ne s’agit là que de quelques exemples, d’une part infime de cette réalité.

L’urgence climatique et la détérioration de la biodiversité menacent les
droits des peuples à accéder aux ressources fondamentales : l’eau, la terre, l’air.

Sans parler de la négation des droits de tous les autres êtres vivants
sans lesquels la vie humaine serait impossible.

**La lutte pour la préservation de l’environnement n’est pas un luxe, ni un choix,
c’est une nécessité**.

C’est en ces temps cruciaux que j’incite tous ceux qui se soucient des
droits humains à agir pour protéger l’environnement et les personnes qui
risquent tout, y compris leur vie, pour le protéger. »



