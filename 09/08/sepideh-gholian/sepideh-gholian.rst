

.. index::
   pair: Militante; Sepideh Gholian

.. _sepideh_gholian_2023_09_08:

===============================================================================================================================
**Sepideh Gholian** «Le régime oppresseur et le peuple iranien savent tous deux que la démocratie finira par l’emporter»
===============================================================================================================================

.. figure:: images/sepideh_gholian.png
   :align: center


Présentation
===============

Sepideh Gholian est journaliste, militante féministe et écologiste.
Elle a écrit cette lettre en juin 2023 à la prison d’Evin.

Militante féministe et écologiste, la journaliste Sepideh Gholian a été
condamnée en 2018 pour avoir couvert les grèves ouvrières de la raffinerie
de sucre de Haft-Tappeh.
Brièvement libérée, la jeune femme de 28 ans a été réincarcérée en 2019
et condamnée à une peine de cinq ans de prison pour « atteinte à la sécurité nationale ».


« MON MESSAGE VOUS PARVIENT depuis les murs de la prison d’Evin
==================================================================

« MON MESSAGE VOUS PARVIENT depuis les murs de la prison d’Evin.
Un édifice vieux de plus de cinquante ans qui est, paradoxalement,
synonyme de torture et de répression, mais aussi un symbole des aspirations
à la liberté et à la justice pour notre pays, l’Iran. Un jour viendra,
dans un Iran libre, où le nom d’Evin nous rappellera, tout à la fois,
les jours les plus sombres de notre histoire et la valeur inaliénable
de la liberté.


J’ai été incarcérée pendant un certain temps au sein de la section 209 d’Evin, aux côtés de Cécile Kohler
------------------------------------------------------------------------------------------------------------

J’ai été incarcérée pendant un certain temps au sein de la section 209 d’Evin,
aux côtés de Cécile Kohler, militante syndicaliste française.
Cécile est une femme courageuse, emprisonnée uniquement pour avoir exprimé
son amitié envers des syndicalistes iraniens. Ses geôliers lui ont
interdit de rencontrer sa famille et son compagnon, ainsi que de parler
aux autres prisonniers.

Lors d’un de nos échanges, elle était submergée par une immense tristesse.

Alors, pour nous réconforter mutuellement, nous nous sommes prises dans
les bras. Son plus grand regret était les aveux forcés qui lui ont été
extorqués pour la télévision iranienne.

Elle m’a confié vouloir s’excuser auprès du peuple iranien. Elle ne
connaissait pas les affres de la République islamique. Ce jour-là, ils
l’ont transférée ailleurs.
Dans un endroit où même pleurer allait lui être interdit.

Evin n’est pas l’unique prison où sont enfermés les dissidents iraniens.
Il y a des dizaines d’autres prisons qui enferment des prisonniers
d’opinion, par milliers.
Le régime oppresseur et le peuple iranien savent tous deux que la démocratie
finira par l’emporter grâce à l’ampleur des mouvements protestataires qui
ont jailli à l’intérieur du pays.
Le vrai cauchemar du tyran iranien, c’est le peuple iranien lui-même.
Le mouvement ouvrier constitue une figure de proue du mouvement social
en Iran, mais il est aussi le plus réprimé. Ces dernières années, malgré
la répression et l’interdiction des syndicats, les ouvriers iraniens
ont réussi à organiser des centaines de grèves, suivies, dans différents
secteurs.

Pas un phénomène passager
=============================

Le mouvement « Femme, vie, liberté » a été une grande expérience de mobilisation,
bâtie sur le socle des luttes précédentes, qui démontre la volonté unanime
du peuple iranien de renverser le régime actuel.

**Ce mouvement ne doit pas être considéré comme un phénomène passager**.

Les mouvements ouvriers, les soulèvements et les manifestations pour
protester et revendiquer ses droits ne doivent être ni dangereux ni
interdits ; ils constituent un droit fondamental.
La convergence de ces deux mouvements a suscité l’adhésion de pans entiers
de la population iranienne.


Le mouvement ouvrier figure parmi les plus progressistes du pays.
La présence marquée des femmes, plus particulièrement ces dernières
années, la solidarité quasi explicite avec la communauté LGBTQI +, le
soutien au mouvement pour la défense des droits des minorités ethniques
et les revendications des enseignants démontrent que le mouvement ouvrier
a conscience de la transversalité des luttes et croit à l’implication
de toutes les classes et groupes sociaux.

Je me joins à mes codétenus pour vous transmettre mes salutations
fraternelles depuis la prison d’Evin.
J’espère que notre union pour nous opposer aux régimes tyranniques, partout
dans le monde, et en particulier en Iran, aboutira bientôt à des changements
majeurs au profit des peuples du monde.

En tant que femme dont une partie de la vie a été consumée par la prison,
l’exil et la torture, uniquement pour avoir exigé la justice, je peux
dire que **l’espoir est mon bien le plus cher**.
Gardons-le toujours dans nos cœurs… Avec l’espoir de la victoire du peuple iranien.


