

.. index::
   pair: Militante; Narges Mahammadi

.. _narges_mohammadi_2023_09_08:

=======================================================================================================================================================================================
**Narges Mahammadi** « « En Iran, chaque individu, à tout moment de sa vie et en tout lieu, est coupable du désir de vivre. Il encourt, pour ce crime, les pires sanctions » »
=======================================================================================================================================================================================

.. figure:: images/narges_mohammadi.png
   :align: center


Présentation
===============

Narges Mohammadi est vice-présidente de l’association Defenders of Human
Rights Centre, dirigé par l’avocate Prix Nobel de la paix Shirin Ebadi.
Elle a écrit cette lettre en juin 2023 à la prison d’Evin (nord de Téhéran).


La journaliste de 54 ans a été condamnée en mai 2016 à seize ans de prison
pour son activisme en faveur des droits humains.
En août 2023, cette militante pour l’abolition de la peine de mort en Iran
a écopé d’une nouvelle peine, qui rallonge son temps de détention, ainsi
que de 154 coups de fouet pour avoir écrit au secrétaire général de l’ONU.

Introduction
===============

« L’OBJET DE MON PROPOS EST DE DONNER UN VISAGE aux êtres humains qui,
partout dans le monde, font l’objet d’un enfermement, qu’ils soient cernés
par des murs d’acier ou par les murs de l’oppression, mais qui, envers et
contre tout, aspirent à faire tomber ces « murs » : ceux de l’ignorance,
de l’exploitation, de la pauvreté, de la privation et de l’isolement.

**Entendez-vous, en Iran, le bruit sourd du mur de la peur qui se fissure ?**

Bientôt, nous entendrons celui de son écroulement grâce à la volonté
implacable, la puissance et la détermination sans faille des Iraniens.

En tant que femme, et comme des millions d’autres femmes iraniennes,
j’ai toujours été confrontée à l’enfermement de la culture patriarcale,
au pouvoir religieux et autoritaire, aux funestes lois discriminatoires
et oppressives, et à toutes sortes de restrictions dans tous les domaines
de ma vie.

Notre enfance n’a pas échappé à cet enfermement culturel.
« Ils » ne nous ont pas permis de vivre notre jeunesse et, en un mot,
notre vie. La triste vérité, au fond, est que le gouvernement autoritaire,
misogyne et religieux de la République islamique nous a volé notre vie.

De part et d’autre des murs de fer d’Evin, où l’on nous a emprisonnées,
nous ne sommes pas restées immobiles. En tant que femmes, parfois seules
et sans soutien, souvent au milieu de flots d’accusations et des humiliations,
nous avons brisé une par une nos chaînes jusqu’à ce que surgisse le mouvement
révolutionnaire « Femme, vie, liberté ».
Nous avons alors montré notre force au monde entier.


Mouvement, écho et vitalité
======================================

Au lycée, j’ai étudié les mathématiques et la physique, puis j’ai poursuivi
à l’université des études de physique appliquée. J’ai obtenu le titre
d’ingénieur en maîtrise d’ouvrage ; cependant, en raison de mon engagement
en faveur des droits humains, ma formation et ma carrière se sont heurtées
au mur de l’empêchement.
J’ai exercé le métier de journaliste, mais sur ordre du Guide suprême
de la République islamique, et après la fermeture massive des médias
indépendants, nos journaux et nos magazines ont fait face au mur de
la censure et notre liberté d’expression a été muselée.

Je suis devenue porte-parole de [l’association] Defenders of Human Rights
Centre, pour participer à la formation, en Iran, d’un grand mouvement
associatif et essayer de donner corps à une société civile organisée,
réelle et puissante.
Hélas, ces organisations se sont heurtées au rideau de la fermeture
administrative, à la suite des attaques répétées des forces de sécurité,
sous l’égide du ministère des renseignements iranien et du corps des
gardiens de la révolution. J’ai protesté et lutté contre les politiques
destructrices et répressives, aux côtés de milliers de manifestants et
opposants qui ont, eux aussi, été cernés par les murs de la prison,
de l’isolement et de la torture.


Enfin, je suis devenue « mère », mais il y a longtemps qu’entre mes enfants
et moi, s’est dressé le mur de l’émigration et de l’exil forcé, à l’instar
de centaines de milliers d’autres mères qui souffrent de l’éloignement de
leurs enfants. Les mots me manquent pour décrire cette maternité restée
derrière le mur de la cruauté et de la violence.

Malgré cette prison qui est la nôtre, nous n’avons jamais cessé de nous
battre. Nous sommes devenus des mères et des pères universels, nous avons
conservé nos valeurs, notre enthousiasme, notre amour, notre force et
notre vitalité, nous avons recréé la vraie vie.

Bien qu’entravés par tous ces verrous, nous avons été capables de faire
émerger le pouvoir des contestataires et la force de la contestation.
Notre élan nous a hissés plus haut que les murs qui nous oppressent et
nous sommes plus puissants et plus solides qu’eux.
Si nos barreaux sont immobilité, silence et mort, nous sommes mouvement,
écho et vitalité, et c’est là que se dessine la promesse de notre victoire.


Soyez notre voix
=====================

Le gouvernement de la République islamique nie les droits fondamentaux
tels que le droit à la vie, à la liberté de penser, à la liberté d’expression
et de croyance, ainsi que le droit à pratiquer la danse, la musique, et
même le droit à l’amour.

Si vous regardez attentivement la société iranienne, vous verrez que
chaque individu, à tout moment de sa vie et en tout lieu, **est coupable
du désir de vivre**. Il encourt, pour ce crime, les pires sanctions, châtiments,
humiliations, arrestations, et peut être emprisonné, voire exécuté, pour cela.

Chacun d’entre nous est donc devenu un opposant au régime. Le monde est
témoin des cycles répétés de protestations en Iran et de la créativité
du mouvement social dans son ensemble, qui invente chaque jour des
nouvelles formes de mobilisation.

Ce mouvement amène à une transition qui éloigne chaque jour la République
islamique et nous mène tout droit vers la démocratie, l’égalité et la liberté.

**Le rôle des médias libres, des sociétés civiles, des organisations des
droits humains, partout dans le monde, est crucial dans cette lutte**.

Cher lecteur, chère lectrice, la publication de cette lettre démontre à
elle seule que notre voix était suffisamment puissante pour vous parvenir.

Soyez aussi notre voix, relayez notre message d’espoir, dites au monde
que nous ne sommes pas derrière ces murs pour rien et que nous sommes à
présent plus forts que nos bourreaux qui emploient tous les moyens
possibles pour faire taire notre société.

Cette voix retentira dans le monde. Cet horizon nous motive et nous réjouit.

Nous triompherons ensemble. En espérant voir arriver très bientôt ce jour. »



