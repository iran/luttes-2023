

.. index::
   pair: Militante; Zeinab Jalalian

.. _zeinab_jalalian_2023_09_08:

===============================================================================================================================
**Zeinab Jalalian** « Derrière ces murs, je vis en attendant le jour où je viendrai vers vous, les bras chargés de fleurs »
===============================================================================================================================


.. figure:: images/zeinab_jajalian.png
   :align: center


Présentation
===============

Zeinab Jalalian est une activiste féministe kurde qui se bat pour les
droits des minorités ethniques et contre la ségrégation de genre en Iran.
Elle a écrit cette lettre en juin 2023 à la prison de Yazd (centre de l’Iran).


Incarcérée depuis 2008, la militante féministe kurde de 41 ans est la
plus ancienne prisonnière d'Iran et l'unique détenue politique condamnée
à perpétuité pour « crime de belligéance ».

Malgré la torture, elle refuse de se soumettre aux aveux forcés.


JE SUIS UNE FEMME KURDE TÉMOIN DE NOMBREUX CRIMES
===================================================

« JE SUIS UNE FEMME KURDE TÉMOIN DE NOMBREUX CRIMES dans les prisons de
la République islamique au cours de mes seize années de détention.

J’ai été témoin d’humiliations, de fausses accusations, d’insultes et
de tortures infligées aux prisonniers, et surtout, j’ai été témoin de
l’exécution de mes codétenus.

**Existe-t-il une douleur plus grande que celle-ci ?**

Malgré cela, c’est à moi qu’il est demandé, par mes bourreaux, de me repentir.
Moi qui ai vu tant d’injustices de mes propres yeux, est-ce à moi d’exprimer
des regrets ?

J’ai été arrêtée en février 2008 à Kermanshah. Plus de deux mois après
mon arrestation, mes proches ne connaissaient toujours pas mes conditions
de détention, ni même l’endroit où j’étais détenue.
J’ai passé trois mois dans un centre de détention secret appelé « centre
de renseignements de Kermanshah » au milieu d’un champ pétrolier, en isolement.

Pendant cette période, j’ai été soumise à des tortures physiques et
psychologiques, et j’ai même été menacée de viol.

Mes tortionnaires déchiraient mes vêtements, me ligotaient les mains et
les pieds, m’enchaînaient à un lit métallique, alors que j’avais les
yeux bandés, pour m’interroger. Ils me frappaient régulièrement la plante
des pieds avec un câble, au point que je perdais connaissance.
Une fois, sous la pression de la torture, j’ai perdu toute sensation et
le contrôle de mon corps pendant un certain temps.
Ma tête ayant violemment heurté le mur, j’ai eu une fracture crânienne
et des hématomes aux yeux. Les mains entravées par des menottes, j’ai été
traînée dans toutes les directions, de sorte que j’en porte encore les
stigmates.

Neuf mois après mon arrestation, en décembre 2008, j’ai été jugée en
moins de quelques minutes et condamnée à mort pour « crime de belligérance ».
Privée du droit d’avoir un avocat, je n’ai pas eu l’autorisation de me
défendre au tribunal.

En février 2010, après le procès et la condamnation, j’ai été transférée
de Kermanshah à la section 209 de la prison d’Evin, gérée par les services
de renseignement, pour l’exécution de la peine de mort.
J’ai fait l’objet de nouvelles interrogations et pressions pour me forcer
à avouer ce que je n’avais pas commis. Après cinq mois de torture et de
menaces quotidiennes d’exécution, j’ai été renvoyée d’Evin à Kermanshah.

De 2014 à 2020, j’ai été transférée de prison en prison. En 2014, à la
prison de Khoy, puis le 13 avril 2020 à la prison de Qarchak, le 5 juillet
2020 à la prison de Kerman, puis le 3 octobre de la même année à la prison
de Dizelabad à Kermanshah.
Enfin, en novembre 2020, j’ai été conduite à la prison de Yazd.
Ces prisons se trouvent à des centaines de kilomètres de ma famille et
de ma ville de résidence.

Ainsi va notre vie
=====================

Parfois, je me demande dans quel espace-temps je suis bloquée, et dont
je ne parviens pas à m’échapper… Pourquoi devrais-je être témoin de la
destruction de mes semblables et de mes proches ?

Peut-on ainsi continuer à être le témoin silencieux de telles injustices ?

La guerre, la torture, les meurtres et les massacres se perpétuent, et
les oppresseurs trouvent toujours des justifications à leurs crimes.
C’est douloureux, mais ainsi va notre vie. Malgré cela, je n’ai jamais
souhaité la mort de mes oppresseurs. J’aimerais que nous luttions ensemble
pour les jeter hors de nos terres, pour ne pas avoir honte devant les
générations futures.

Depuis mon arrestation, j’ai vu des centaines de personnes arrêtées et
incarcérées, lors des différentes protestations.
Entre autres, en 2009, lors du « mouvement vert », puis depuis 2022,
pendant le mouvement « Femme, vie, liberté ».

Le régime islamique a utilisé la répression et les balles face au soulèvement
populaire. Il a qualifié les dissidents de perturbateurs et d’ennemis
de l’Etat.
Je compatis avec chacun d’entre vous qui êtes descendus dans les rues
pendant toutes ces années, vous qui êtes arrêtés et torturés, ou qui avez
perdu vos proches.


Je suis enfermée depuis plus de seize ans pour avoir réclamé la liberté,
la justice et l’égalité.

Ma vraie douleur ne réside pas dans le fait d’être emprisonnée, mais dans
la perte de tous ces êtres chers tombés pour la liberté, ceux dont nous
n’entendrons plus la voix.
Ceux qui n’ont pas reculé face au régime sanguinaire et ont fièrement
sacrifié leur vie sur le chemin de la lutte pour la liberté.
Tant que je serai en vie, je m’efforcerai de défendre leur martyre.

Derrière ces murs, je vis en attendant le jour où je viendrai vers vous,
les bras chargés de fleurs, et je m’inclinerai humblement devant vous,
offrant un bouquet de jasmin à vos cœurs généreux.

Debout jusqu’à la victoire, je me tiendrai toujours du côté des opprimés
et des combattants qui luttent contre l’oppression et la tyrannie. »
