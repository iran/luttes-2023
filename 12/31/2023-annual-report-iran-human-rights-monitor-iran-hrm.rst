.. index::
   pair: Iran IHRM ; 2023 Annual report

.. _iran_hrm_2023_12_31:

==========================================================================
2023-12-31 **2023 Annual Report, Iran Human Rights Monitor (Iran HRM)**
==========================================================================

- https://iran-hrm.com/2024/01/01/2023-annual-report-iran-human-rights-monitor-iran-hrm/

.. tags:: Iran HRM

Introduction
===============


Iran Human Rights Monitor (Iran HRM) has made an effort in its annual report
to provide an overview of the human rights situation in Iran. Given that
the judiciary in Iran extensively employed executions as the most ruthless
and inhumane form of punishment in 2023, a significant portion of the 2023
annual report focuses on executions. Another section of the report addresses
the investigation into the suppression of protesters during the nationwide
protests of 2022-2023.

The Iranian regime, in an attempt to suppress the nationwide protests, targeted
the protesters and sought to silence any dissenting voices. They issued death
sentences, along with severe and unjust verdicts, and employed brutal forms of
torture to stifle the voice of every protester.  The policy of “creating fear
by using heavy punishments and issuing death sentences” has become the official
policy of Iran’s judiciary, and in this regard, it has the full support of
Ebrahim Raisi as the president, especially since Raisi has the experience of
being a member of the Death Commission in the massacre of the summer of 1988.


.. figure:: images/850_morts.webp
