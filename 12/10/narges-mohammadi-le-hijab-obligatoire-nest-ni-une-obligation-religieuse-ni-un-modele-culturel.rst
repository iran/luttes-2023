.. index::
   pair: Narges Mohammadi; Le hijab obligatoire n’est ni une obligation religieuse ni un modèle culturel (2023-12-10)
   pair: Narges Mohammadi; Ali et Kiana

.. _mohammadi_2023_12_10:

==================================================================================================================
2023-12-10 **Narges Mohammadi : Le hijab obligatoire n’est ni une obligation religieuse ni un modèle culturel**
==================================================================================================================

- https://kurdistan-au-feminin.fr/2023/12/10/narges-mohammadi-le-hijab-obligatoire-nest-ni-une-obligation-religieuse-ni-un-modele-culturel/

.. figure:: images/kiana_et_ali.jpg

   Les jumeaux de Narges Mohammadi Ali et Kiana

   
La militante iranienne emprisonnée Narges Mohammadi a envoyé un message par
l’intermédiaire de ses enfants lors de la cérémonie du prix Nobel de la
paix à Oslo, condamnant le "régime religieux cruel et misogyne" en Iran.

Narges Mohammadi est détenue à la prison d’Evin à Téhéran depuis 2021
pour sa lutte **contre le hijab obligatoire et la peine de mort en Iran**.

Narges Mohammadi, qui a reçu le prix Nobel de la paix début octobre, n’a
pas été autorisée à se rendre à Oslo pour recevoir son prix.

Lors de la cérémonie à la mairie d’Oslo, ses enfants jumeaux Ali et Kiana,
17 ans, exilés en France depuis 2015, ont lu en français le message que
Narges Mohammadi a réussi à délivrer depuis sa cellule.

"Je suis une femme du Moyen-Orient, d’une région qui, bien qu’héritière
d’une riche civilisation, est désormais prise au piège de la guerre et
victime des flammes du terrorisme et de l’extrémisme", a déclaré Narges
Mohammadi, ajoutant qu’elle a écrit ce message "derrière les murs hauts
et froids d’une prison".

Cette militante de 51 ans, arrêtée et condamnée à plusieurs reprises ces
dernières années, était l’un des visages marquants du soulèvement "Jin,
Jiyan, Azadi" [slogan féministe kurde signifiant "Femme, Vie, Liberté"]
en Iran déclenché par le meurtre brutal de Jina Mahsa Amini en septembre 2022.

"Le hijab obligatoire imposé par le gouvernement n’est ni une obligation
religieuse ni un modèle culturel, mais plutôt un moyen de contrôler et
d’assujettir l’ensemble de la société", a déclaré Narges Mohammadi
dans son message.

Qualifiant l’obligation pour les femmes iraniennes de porter le foulard de
"honte pour le gouvernement", Narges Mohammadi a condamné le "régime
religieux cruel et misogyne", dressant le portrait d’une République
islamique "essentiellement étrangère à son ‘peuple’".

"Le peuple iranien surmontera la répression et l’autoritarisme avec
détermination", a déclaré Narges Mohammadi, condamnant notamment la
répression, l’asservissement du pouvoir judiciaire, la propagande et
la censure, le népotisme et la corruption : "N’en doutez pas, c’est
certain".

Dans l’histoire de plus de 100 ans du prix Nobel, Narges Mohammadi est la
cinquième personne à recevoir le prix de la paix alors qu’elle était en
prison, après l’Allemand Carl von Ossietzky, la Birmane Aung San Suu Kyi,
le Chinois Liu Xiaobo et le Biélorusse Ales Beliatski.

"La lutte de Narges Mohammadi (…) est comparable à celle d’Albert Lutuli,
Desmond Tutu et Nelson Mandela (tous lauréats du prix Nobel), qui a duré plus
de 30 ans avant la fin de l’apartheid en Afrique du Sud. (…) Les femmes
iraniennes luttent contre la discrimination depuis plus de 30 ans. Leur rêve
d’un avenir meilleur finira par se réaliser.", a déclaré le président
du Comité Nobel, Berit Reiss-Andersen.

Les jumeaux de Narges Mohammadi, séparés de leur mère depuis plus de huit
ans, disent ne pas savoir s’ils la reverront un jour vivante.

"Personnellement, je suis assez pessimiste", a déclaré samedi sa fille
Kiana, tandis que son frère Ali s’est dit "très, très optimiste".
