.. index::
   pair: Otage; Johan Floderus

.. _floderus_2023_12_10:

============================================================================================================
2023-12-10 Un diplomate suédois de l’UE détenu à Téhéran accusé de coopérer avec Israël contre l’Iran
============================================================================================================

- https://fr.timesofisrael.com/un-diplomate-suedois-de-lue-detenu-a-teheran-accuse-de-cooperer-avec-israel-contre-liran/


Résumé
=========

**Le diplomate a été arrêté alors qu’un Iranien, Hamid Noury, a été
condamné en Suède à la prison à perpétuité pour son rôle dans des
exécutions de masse de milliers d’opposants par le régime iranien en 1988**.


Texte
======


Johan Floderus, 33 ans, avait été arrêté le 17 avril 2022 à l’aéroport
de Téhéran, alors qu’il s’apprêtait à rentrer chez lui après un voyage
en compagnie d’amis.

Ce Suédois, qui travaille pour le service diplomatique de l’Union européenne,
est depuis détenu à la prison d’Evin à Téhéran, où sont incarcérés
de nombreux opposants au régime iranien.

La Suède a dénoncé comme "arbitraire " la détention de M. Floderus
tandis que l’UE a réclamé sa libération.

Le ministre suédois des Affaires étrangères Tobias Billstrom a qualifié 
d'"arbitraire" sa détention – à la prison d’Evine, à Téhéran – et
en jugeant "sans fondement " un procès.

"L’UE continue d’exiger la libération immédiate de Johan Floderus ",
a déclaré Josep Borrell, dans un communiqué, en affirmant également qu’il
est "innocent " et qu' "il n’y a absolument aucune raison de le maintenir
en détention.

"Je soulève son cas à chaque occasion et contact avec les autorités
iraniennes depuis sa détention, demandant sa libération ", a souligné
M. Borrell.

"Nous leur demandons des éclaircissements et davantage d’informations,
en étroite coordination avec les autorités suédoises qui assument la
responsabilité consulaire", a-t-il ajouté.

**Le diplomate a été arrêté alors qu’un Iranien, Hamid Noury, a été
condamné en Suède à la prison à perpétuité pour son rôle dans des
exécutions de masse de milliers d’opposants par le régime iranien en 1988**.

