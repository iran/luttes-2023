

.. _afp_2023_12_12:

===============================================================================================================
2023-12-12 **Prix Sakharov: Jina Mahsa Amini, "symbole de liberté", honorée à titre posthume** (AFP)
===============================================================================================================

Strasbourg, 12 déc 2023 (AFP) — Le Parlement européen a remis mardi le
prix Sakharov à titre posthume à l'Iranienne Jina Mahsa Amini -- dont le nom
est devenu un "symbole de liberté" selon sa famille, tenue à l'écart de la
cérémonie par Téhéran.

**Plus haute distinction de l'Union européenne pour les droits humains**, ce
prix a été décerné à la jeune Kurde iranienne, décédée à l'âge de
22 ans le 16 septembre 2022, trois jours après avoir été arrêtée pour
non-respect du strict code vestimentaire imposé aux femmes en Iran.

Sa mort a entraîné des mois de manifestations contre les dirigeants politiques
et religieux iraniens, Jina Mahsa Amini devenant le symbole de la lutte contre
l'obligation du port du voile. La répression de ce mouvement a provoqué des
centaines de morts et des milliers d'arrestations.

La famille de Jina Mahsa Amini avait prévu d'assister à la remise du prix Sakharov
au Parlement européen à Strasbourg, mais a été frappée au dernier moment
par une interdiction de quitter le territoire iranien.

"J'aimerais pouvoir être présente dans votre honorable assemblée, pour
représenter toutes les femmes de mon pays et exprimer ma gratitude pour
l'attribution du prix Sakharov" a écrit la mère de Jina Mahsa Amini, Mojgan
Eftekhari, dans un message lu par son avocat, Saleh Nikbakht, qui a reçu le
prix Sakharov au nom de la famille.

"Malheureusement, cette opportunité nous a été refusée, en violation de
toutes les normes juridiques et humaines", a-t-elle ajouté.

Evoquant sa fille, Mojgan Eftekhari l'a comparée à Jeanne d'Arc, soulignant
que "sa vie a été injustement enlevée".

"Je crois fermement que son nom, au côté de celui de Jeanne d'Arc, restera
un symbole de liberté", a-t-elle estimé.

Condamnant la décision du régime iranien d'empêcher les proches de Mahsa
Amini de se rendre en France, la présidente du Parlement européen Roberta
Metsola a déclaré que "la façon dont ils ont été traités est un nouvel
exemple de ce à quoi le peuple iranien est confronté au quotidien".

"Le courage et la résilience des femmes iraniennes dans leur lutte pour la
justice, la liberté et les droits humains ne seront pas stoppés. Leurs voix
ne peuvent pas être réduites au silence", a martelé Mme Metsola.

- "Marre du régime iranien" -

Plus d'une centaine d'eurodéputés avaient signé une lettre ouverte pour
dénoncer la décision des autorités iraniennes, qui vise à leurs yeux à
"réduire au silence" la famille de Jina Mahsa Amini "en l'empêchant de dénoncer
la répression scandaleuse des droits des femmes, des droits humains et des
libertés fondamentales par la République islamique en Iran".

Considérant qu'il n'y a "aucune base légale" justifiant cette mesure,
l'avocat de la famille, Saleh Nikbakht, compte la contester une fois retourné
à Téhéran... "S'ils m'en donnent le temps, car dès mon retour en Iran, je
pourrais être arrêté et emprisonné", a-t-il dit à l'AFP. L'avocat a en effet
été condamné en octobre dernier à un an de prison pour "propagande" contre
l'Etat après s'être entretenu avec des médias sur l'affaire Jina Mahsa Amini.

La remise du prix Sakharov est intervenue deux jours après celle du prix
Nobel de la Paix à la militante Narges Mohammadi, qui n'a pu se rendre à
Oslo recevoir cette récompense car elle est détenue depuis 2021 dans la
prison d'Evin de Téhéran.

Mardi au Parlement de Strasbourg, deux militantes iraniennes ont représenté
le mouvement "Femme Vie Liberté", lui aussi récompensé par le prix Sakharov.

Il s'agit d'Afsoon Najafi, dont la soeur Hadis a été tuée à 22 ans lors
d'une manifestation en honneur de Jina Mahsa Amini, en septembre 2022, et de
Mersedeh Shahinkar, blessée à l'oeil lors d'une manifestation contre le
régime iranien en octobre 2022.

Elles ont toutes les deux quitté l'Iran en 2023.

"Nous sommes ici au nom de toutes les femmes, nous en avons marre du régime
iranien" a déclaré lors d'une conférence de presse Mersedeh Shahinkar,
qui vit désormais en Allemagne.

Afsoon Najafi a appelé la communauté internationale à mettre davantage de
pression sur le régime iranien.

Le Parlement européen a adopté plusieurs résolutions non contraignantes
pour condamner la répression des manifestants par le régime iranien.
