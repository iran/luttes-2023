

.. _narges_2023_12_06:

=======================================================================================================================================================
2023-12-06 **Narges Mohammadi, prix Nobel iranienne de la paix : "Les murs de cette prison ne pourront pas étouffer ma voix"** par Rachida El-Azzouzi 
=======================================================================================================================================================

- https://www.mediapart.fr/journal/international/051223/narges-mohammadi-prix-nobel-iranienne-de-la-paix-les-murs-de-cette-prison-ne-pourront-pas-etouffer


Préambule
============

Depuis la sinistre prison iranienne d’Evin, Narges Mohammadi, prix Nobel 
de la paix 2023 pour son combat pour les femmes, livre un entretien 
exclusif à Mediapart. 
Une leçon de liberté, de courage, de féminisme.


Introduction
===============

C’est un entretien exceptionnel parce qu’il a traversé
clandestinement les murs d’une des pires prisons au monde : celle d’Evin, en
Iran, que l’on surnomme avec ironie "l’université", car la République
islamique y entasse un grand nombre d’intellectuel·les, d’étudiant·es,
de journalistes, d’artistes.

C’est un entretien exceptionnel parce qu’il donne à entendre la voix
d’une personnalité hors norme, une femme qui va recevoir dimanche 10
décembre 2023 à Oslo (Norvège), non pas en main propre mais dans celles de sa
famille à laquelle elle a été arrachée, le prix Nobel de la paix 2023 
"pour son combat contre l’oppression des femmes en Iran et sa lutte pour la
promotion des droits humains et la liberté pour tous" : la journaliste et
militante Narges Mohammadi.

Pour des raisons de sécurité, nous ne détaillerons pas les conditions
de la réalisation de cet entretien exclusif donné à Mediapart, mais nous
remercions celles et ceux qui l’ont permise. 

Car la voix de Narges Mohammadi doit porter hors des barreaux d’Evin. 

Elle le dit elle-même à Mediapart : "Les murs de cette prison ne pourront 
pas étouffer ma voix, elle portera jusqu’au reste du monde."

Infatigable militante de la cause des femmes, figure emblématique du
soulèvement Femme, vie, liberté violemment réprimé, vice-présidente du
Centre des défenseurs des droits humains fondé par l’avocate Shirin Ebadi,
elle aussi Prix Nobel en 2003, Narges Mohammadi, 51 ans, est dans le viseur
de la dictature des mollahs depuis des décennies.

Arrêtée et condamnée à de multiples reprises à diverses peines, dont
plus de trente et un ans de prison au total, ainsi qu’à des traitements
cruels (notamment 154 coups de fouet), à nouveau incarcérée depuis 2021,
elle paie cher son engagement contre l’apartheid de genre en Iran, contre
le voile obligatoire, contre la peine de mort.

Leçon de courage, de liberté, de féminisme, d’humilité, Narges Mohammadi
n’a jamais abdiqué malgré les sacrifices immenses, à commencer par la
séparation forcée d’avec les siens, d’avec son mari, le journaliste
et écrivain Taghi Rahmani, exilé à Paris depuis 2012 après plus d’une
décennie en prison, et d’avec leurs deux enfants qu’elle ne peut voir
grandir, Kiana et Ali, 17 ans, qui ont rejoint leur père en 2015. "Être
loin de mes enfants est ce qu’il y a de plus dur et de plus douloureux dans
l’enfermement. C’est l’une des tortures les plus insupportables",
témoigne-t-elle auprès de Mediapart.


Narges Mohammadi

    Pour celui qui met un pied dans l’arène de la lutte, la liberté devient
    palpable, imaginable.

Elle résiste aux souffrances, aux tortures, parce que son combat, ses combats
ont "du sens", dit-elle, "convaincue que nous obtiendrons la démocratie,
la liberté et l’égalité, que nous serons victorieux". 

Même en prison, elle continue de résister, de manifester, de se battre pour les droits des
femmes au prix de nouvelles condamnations. Début novembre, elle a fait une
grève de la faim pour avoir le droit d’être admise à l’hôpital sans
porter le voile et y être soignée.

Elle raconte à Mediapart son engagement, le quotidien derrière les barreaux,
rend hommage à Mahsa Amini − dont la mort sous les coups de la police des
mœurs en septembre 2022 a impulsé le puissant mouvement Femme, vie, liberté
− ainsi qu’à la lycéenne Armita Garavand, morte elle aussi un an plus tard
pour n’avoir pas respecté l’obligation de port du voile. 

Narges Mohammadi dit aussi son espoir pour l’Iran, interpelle la 
communauté internationale et nous toutes et tous.


Comment surmontez-vous le fait d’être séparée de votre famille ?
===================================================================

Mediapart

	Vous n’êtes pas autorisée à recevoir une photo de vos enfants,
	âgés de 17 ans, ni de votre mari, encore moins à leur parler. Comment
	surmontez-vous le fait d’être séparée de votre famille ?

Narges Mohammadi : Être loin de mes enfants est ce qu’il y a de plus dur
et de plus douloureux dans l’enfermement. C’est l’une des tortures les
plus insupportables. La première fois que Kiana et Ali m’ont rendu visite,
c’était juste après leur anniversaire, ils avaient 5 ans et 3 mois. 

À l’époque, j’étais en cellule d’isolement dans la section de sécurité
maximale de la prison, les conditions y étaient très différentes comparées
à la section de droit commun. Là-bas, il n’y avait pas la possibilité
de passer des appels ou d’avoir des visites, j’étais complètement sans
nouvelles de mes enfants.

Je ne pense pas avoir les mots pour décrire pleinement le déchirement que
cela était d’être séparée de Kiana et Ali. Quand je repense à ces jours
sombres, je ne sais pas comment j’ai pu les surmonter. La deuxième fois que
j’ai été mise en cellule d’isolement, Ali et Kiana avaient 5 ans et 5
mois, Taghi venait de quitter le pays. Seule depuis ma cellule, il m’était
insupportable de penser à la solitude de Kiana et Ali, si petits et inoffensifs.

Les prisonnières politiques sont d’horizons divers, mais leur but est le même : mettre fin à la République islamique
=========================================================================================================================

Ce qui m’a permis de tenir bon et de persévérer, encore aujourd’hui,
c’est ma profonde conviction du droit à la liberté de chaque individu. 

Le rêve de la liberté est un grand rêve pour celui qui ne peut que la penser,
la liberté est une réalité distante, mais pour celui qui met un pied dans
l’arène de la lutte, la liberté devient palpable, imaginable. 

Je suis convaincue que nous obtiendrons la démocratie, la liberté et l’égalité,
que nous serons victorieux.

Sur le long chemin qui y mène, les souffrances humaines trouvent un sens. 
Le poids de la souffrance n’en est pas diminué, mais il mène à un but. 
Donner un sens à ces émotions est une manière pour l’humain de se donner un
but. Même au regard de ces expériences douloureuses et difficiles, je ne
peux pas me plaindre.

Comment affrontez-vous l’enfermement, le quotidien en prison où vous subissez la torture ?
=============================================================================================

Les premières années d’incarcération sont totalement différentes de
celles qui ont suivi. En gagnant de l’expérience, en prenant de plus en plus
conscience, en comprenant les effets de l’enfermement, tout cela influence
l’espoir et les causes qui nous animent et nous font vivre la lutte.

Mon emploi du temps quotidien est très resserré. Je lis, j’échange avec
mes codétenues, je fais du sport, j’accomplis des tâches du quotidien,
et à travers cela j’ai l’impression que la vie continue. 
Après le début de la révolution Femme, vie, liberté, nous avons mené beaucoup
d’actions collectives dans le quartier des femmes.  

À cause de ces rassemblements et des tracts que j’ai écrits depuis la
prison, six nouvelles charges ont été ajoutées à mon dossier. 
J’ai été condamnée à deux reprises, d’abord à vingt-sept mois d’enfermement
puis à quatre mois additionnels. 
L’un de mes dossiers a été confié au tribunal révolutionnaire pour jugement.

Lundi 4 décembre 2023, j’ai reçu comme peine la privation de visites et
de communication avec l’extérieur pour avoir enfreint les règles de la
prison. Mais je reste déterminée à continuer le combat, et je suis certaine
que les murs de cette prison ne pourront pas étouffer ma voix, qu’elle
portera jusqu’au reste du monde.

À quoi ressemble la solidarité entre prisonnières ?
========================================================

Après toutes ces années en prison, je crois pouvoir dire que la solidarité,
l’empathie et l’esprit de la lutte sont les plus forts dans le quartier
des femmes. 
Les prisonnières politiques sont d’horizons divers, mais leur
but est le même : mettre fin à la République islamique. 
Ce nouveau chapitre de la lutte a donné naissance à de nouvelles collaborations 
et à des formes d’entraide entre nous.

Les prisonnières s’occupent et aident particulièrement les mères
prisonnières séparées de leurs enfants. 
Parmi les 86 prisonnières du quartier des femmes, quatre ont plus de 70 ans, 
elles sont pour toutes les prisonnières des mères qu’elles chérissent 
et respectent. 
Parmi nous, six femmes ont moins de 25 ans, elles sont choyées. 
Nous sommes une grande famille, nous partageons ensemble nos joies et 
nos tristesses.


Que représente le féminisme pour vous ?
==========================================

Mediapart

	Vous êtes une figure de la lutte des droits des femmes et des droits humains,
	un modèle à travers le monde pour de nombreuses femmes. Que représente le
	féminisme pour vous ?

L’oppression des femmes est l’une des discriminations les plus anciennes,
les plus étendues. Elle est celle qui a les racines les plus profondes dans nos
sociétés et elle est révélatrice des autres systèmes d’oppression. 
Lorsque l’oppression des femmes s’exprime à travers les normes religieuses,
la violence faite aux femmes se transforme et atteint des niveaux insoutenables.

Cela perdure tant qu’une société ne se saisit pas de la cause des femmes,
et du respect des droits humains. 

En Iran, le régime religieux et dictatorial opprime les femmes à travers 
un système d’apartheid de genre. 
Le voile obligatoire a fait partie dès le commencement du projet théologique et
politique de la République islamique, non pas comme un objet religieux ou
culturel, mais comme un outil d’emprise politique à travers lequel ce
régime asservit et opprime la société tout entière.

Si une société ne porte pas le combat du droit des femmes en son sein,
parler de démocratie et de respect des droits humains n’a pas de sens.


Quel rôle ont joué vos parents dans votre engagement ?
===============================================================

Ma mère a toujours été mon plus grand soutien, elle m’a encouragée à
poursuivre mes études, trouver un emploi et m’investir pleinement dans la
société. 
C’était une femme indépendante et engagée dans le combat des
femmes, j’ai été témoin de ses luttes et de ses déceptions.

En nous soutenant financièrement, mon père nous a offert à moi et mes
sœurs une forme de protection. Dans notre famille, l’entrée des filles
à l’université était un moment de célébration. Je pense que le soutien
qu’offrent les mères joue un réel rôle social en encourageant les femmes
à poursuivre des études et à aller à l’université.

Parvenez-vous à envisager le futur de l’Iran depuis les geôles du régime ?
==============================================================================

Le mouvement révolutionnaire Femme, vie, liberté est le fruit de la solidarité
de multiples groupes issus de différents milieux et horizons. 
La société iranienne est animée par de nombreux mouvements, des mouvements sociaux
et contestataires, le mouvement des femmes, des étudiants, des jeunes, des
enseignants, des travailleurs, etc. 
Cela a beaucoup joué dans la construction de ce mouvement révolutionnaire.

En élargissant les cercles d’action et en mobilisant massivement à travers
la désobéissance civile, ce mouvement a su contrer la répression massive
dans les rues. 

Le mouvement Femme, vie, liberté est un élan démocratique, libertaire et 
égalitaire pour le pays, sans équivalent, qui a articulé toutes les demandes 
politiques et sociales et leur a donné un sens nouveau.

Ce mouvement a fondamentalement changé le paysage politique du pays et
a influencé jusque dans les sphères religieuses. 
Ces changements sont profondément ancrés dans l’histoire de la lutte 
dans notre pays. Cette nouvelle conscience est un point de non-retour.

J’ai beaucoup d’espoir quant à l’avenir de l’Iran. 

J’espère que la communauté internationale est sensible et a conscience 
de ces changements profonds et de l’essor du mouvement prodémocratie à 
travers les couches de la société iranienne. 
Une sensibilité nouvelle au respect des droits humains exige de la classe 
dirigeante l’application des valeurs démocratiques.

Mediapart

	En septembre 2022, la mort de Mahsa Amini, pour n’avoir pas porté 
	"correctement" le voile obligatoire, a provoqué un soulèvement populaire
	inédit, mais pas celle de la lycéenne Armita Garavand, agressée un an plus
	tard, en octobre 2023, selon les ONG, par la police des mœurs car elle ne
	portait pas de foulard. Comment l’expliquez-vous ?

D’après moi, il y a plusieurs facteurs. Le mouvement Femme, vie,
liberté a fait face à une répression brutale. Un an après, lors de la
mort d’Armita Garavand, la répression sourde du gouvernement a vidé les
rues de la contestation. Cet événement aurait pu être une étincelle, mais
l’appareil répressif du régime a œuvré, en détenant les journalistes
impliqués dans l’affaire, en contrôlant les caméras de sécurité du
métro, en menaçant et en surveillant l’hôpital, l’école et la famille
afin qu’aucune information ne puisse filtrer.

Pour Mahsa Amini, la couverture médiatique de sa mort et les confirmations 
apportées par la famille ont permis à un mouvement populaire de s’organiser. 

Dans le cas d’Armita Garavand, le public était maintenu dans le doute à 
cause des informations mensongères du gouvernement. 
À travers des mensonges et des menaces, le gouvernement a pu faire douter 
de tout, jusqu’à la nouvelle même de la mort d’Armita. 
Cela était extrêmement douloureux pour moi, je le vois comme un moment 
où on a enterré la vérité.

Interview par Rachida El-Azzouzi (https://www.mediapart.fr/biographie/rachida-el-azzouzi)
