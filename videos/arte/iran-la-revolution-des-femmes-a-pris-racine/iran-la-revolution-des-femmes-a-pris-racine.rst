.. index::
   pair: Video ; Iran la révolution des femmes a pris racine avec Chowra Makaremi

.. _video_chowra_arte_2023_09:

======================================================================
**Iran : la révolution des femmes a pris racine** avec Chowra Makaremi
======================================================================


- https://www.arte.tv/fr/videos/113043-101-A/iran-la-revolution-des-femmes-a-pris-racine/

Caractéristiques du film
=========================

:Durée: 10 min
:Disponible: Du 01/09/2023 au 01/09/2024
:Pays: France
:Année: 2023
:Genre: https://www.arte.tv/fr/videos/documentaires-et-reportages/

Montage Pascal Bach
-----------------------

- Pascal Bach

Graphisme Anne Mangin et Mikaël Giummely
----------------------------------------------

- Anne Mangin
- Mikaël Giummely

Journaliste David Zurmely
------------------------------

- David Zurmely

Documentaliste Véronique Abonneau
----------------------------------------

- Véronique Abonneau



Description
================

Le 16 septembre 2022, la mort d'une jeune femme, Jina Mahsa Amini, provoque
le soulèvement de la population iranienne.
Accusée de mal porter son voile, elle avait été arrêtée et battue à mort
par la police des moeurs de Téhéran.

La répression du mouvement a été féroce, causant la mort de plus de 500
personnes et des dizaines de milliers d'arrestations.

A l'approche du premier anniversaire de ces événements, l'anthropologue
Chowra Makaremi analyse pour nous comment ce mouvement s'inscrit dans
l'histoire longue de la République islamique, et pourquoi il marque une
rupture essentielle pour l'avenir de la société iranienne.

Chowra Makaremi développe son analyse dans :ref:`un ouvrage paru le 7 septembre 2023,
"Femme ! Vie ! Liberté !" aux éditions La Découverte <livre_femme_vie_makaremi_2023>`.
