.. index::
   pair: Arte ; Un an après la mort de Mahsa Amini : où est passée la révolution iranienne ?

.. _28_minutes_2023_09_14:

========================================================================================================================================
**Un an après la mort de Mahsa Amini : où est passée la révolution iranienne ?** disponible: sur arte du 14/09/2023 au 13/09/2024
========================================================================================================================================

- https://www.arte.tv/fr/videos/116611-002-A/un-an-apres-la-mort-de-mahsa-amini-ou-est-passee-la-revolution-iranienne/


.. figure:: images/jina_mahsa_amini.png
   :align: center

Caractéristiques du film
=========================

:Durée: 25 min
:Disponible: du 07/07/2023 au 08/07/2026
:Année:  2023
:Genre: https://www.arte.tv/fr/videos/emissions/


Description
===============

À près d’un an de la mort de Mahsa Amini, quel bilan tirer de la révolution
iranienne ?

Si la résistance ne prend plus la forme de manifestations de rue, elle
s’incarne dans des actes de rébellion individuels, comme le fait de ne
pas porter le voile en public.

On en débat avec Azadeh Kian, Farid Vahid et Georges Malbrunot.
