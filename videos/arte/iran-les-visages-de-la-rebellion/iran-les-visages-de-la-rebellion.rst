.. index::
   pair: Arte ; Iran : les visages de la rébellion
   ! Hengameh Ghaziani

.. _arte_visages_rebellion_2023:

========================================================================================================================================
**Iran : les visages de la rébellion** disponible: sur arte du 08/03/2023 au 07/03/2024 (3 minutes)
========================================================================================================================================

- https://www.arte.tv/fr/videos/113946-000-A/iran-les-visages-de-la-rebellion/

.. _hengameh_ghaziani:

Hengameh Ghaziani
====================

.. figure:: images/hengameh_ghaziani.png
   :align: center

   Hengameh Ghaziani

Caractéristiques du film
=========================

:Durée: 3 min
:Disponible: 08/03/2023 au 07/03/2024
:Année:  2023
:Genre: https://www.arte.tv/fr/videos/documentaires-et-reportages/
:Pays: France et Allemagne

Journaliste: Barbara Lohr
-----------------------------

- Barbara Lohr

Montage Ludovic Mingot
-------------------------

- Ludovic Mingot


Description
===============

Les Iraniennes se révoltent contre le régime des Mollahs, malgré une
répression massive.

Depuis le début des manifestations en septembre dernier, plus de 500
manifestants ont été tués.

**Qui sont ces femmes qui ont décidé de se battre pour leurs droits ?**
