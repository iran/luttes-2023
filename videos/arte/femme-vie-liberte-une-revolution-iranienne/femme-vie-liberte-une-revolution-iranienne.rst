.. index::
   pair: Video ; Femme, vie, liberté - Une révolution iranienne
   pair: Réalisatrice ; Claire Billet
   pair: Réalisateur ; Mohamad Hosseini

.. _arte_femme_revol_2023:

==========================================================================================================
**Femme, vie, liberté - Une révolution iranienne** sur Arte disponible: du 12/09/2023 au 24/12/2023
==========================================================================================================

- https://www.arte.tv/fr/videos/113185-000-A/femme-vie-liberte-une-revolution-iranienne/


Caractéristiques du film
=========================


:Durée: 53 min
:Disponible: Du 12/09/2023 au 24/12/2023
:Pays: France
:Année:  2023
:Genre: https://www.arte.tv/fr/videos/documentaires-et-reportages/

Réalisation Claire Billet et Mohamad Hosseini
-----------------------------------------------

- Claire Billet
- Mohamad Hosseini

Description
===============

Un an après l'assassinat qui a embrasé l'Iran, ce documentaire relate,
à l'aide d'images tournées clandestinement et de témoignages, une insurrection
féministe et populaire à l’immense impact.

"Lorsque j'ai appris le meurtre de Mahsa, ma première réaction a été la rage",
témoigne Narges Mohammadi, qui a suivi les événements de la prison d'Evin
où elle est détenue.

Dans une missive bouleversante, lue par l'actrice iranienne exilée
Golshifteh Farahani (qui témoigne dans le film), cette militante des
droits de l'homme évoque aussi son combat pacifique contre un
"régime religieux, misogyne et tyrannique" et ses dures conditions
de détention.

Le 16 septembre 2022, à Téhéran, le meurtre par la police de la jeune
Mahsa Amini, arrêtée pour "port du voile non conforme à la loi",
déclenche une insurrection sans précédent.

**En quelques heures, un mouvement spontané se forme autour du cri de
ralliement : “Femme, vie, liberté.”**

Pour la première fois, des femmes, rejointes par les hommes et les étudiants,
en sont à l’initiative : elles descendent en masse dans la rue et retirent
leur voile, symbole honni de la République islamique.

La population iranienne, toutes régions et catégories sociales confondues,
se soulève.
Les réseaux sociaux s’enflamment.
La diaspora (de 5 millions à 8 millions d'Iraniens) prend le relais et
le monde entier découvre l'ampleur de cette mobilisation : se pourrait-il,
cette fois-ci, que le régime théocratique soit renversé ?

Colère et désobéissance civile
-----------------------------------

À cette révolte les mollahs répliquent de manière féroce, emprisonnant
des dizaines de milliers de personnes et causant plus de 400 morts.

Mais la peur suscitée par cette répression s'accompagne désormais d'une
rage tenace et d'une propension grandissante à la désobéissance civile.

Les exécutions publiques, le gazage des écoles de filles sanctionnant
l'activisme des élèves, les tirs dans la foule et les arrestations
n'entravent plus la détermination du peuple iranien.

La contestation perdure, et des dissensions surgissent chez les Gardiens
de la révolution, le rempart du régime.

Ce film retrace ce soulèvement de l'intérieur en puisant dans l'avalanche
de vidéos publiées sur les réseaux sociaux durant les émeutes et dans
les images tournées clandestinement et courageusement sur place par une
équipe iranienne.

Tout en préservant leur anonymat, il recueille les témoignages de
manifestants et d’activistes, éclairage complété par celui d'opposants
au régime en exil.
Il rappelle l'extrême pauvreté du pays et les fondations fragiles d'un
pouvoir verrouillé, miné par la corruption et l'autoritarisme : le guide
suprême Ali Khamenei, ce "dictateur" dont la jeunesse en colère a
déchiré maints portraits, contrôle justice, élections et médias, tandis
que des **gangs mafieux** font tourner l'économie.

Dans le feu des témoignages et des images, parfois d'une grande violence,
documentant cette révolte historique, au fil de cette immersion instructive
et poignante, une question demeure : quel épisode décisif mettra fin à la dictature ?
