.. index::
   pair: Arte ; Iran : une contestation étouffée - Azadeh Kian

.. _arte_contestation_2023_07:

=====================================================================================================
**Iran : une contestation étouffée - Azadeh Kian** disponible: sur arte Du 07/07/2023 au 08/07/2026
=====================================================================================================

- https://www.arte.tv/fr/videos/112333-027-A/iran-une-contestation-etouffee-azadeh-kian/


Caractéristiques du film
=========================

:Durée: 16 min
:Disponible: du 07/07/2023 au 08/07/2026
:Pays: Allemagne
:Année:  2023
:Genre: https://www.arte.tv/fr/videos/documentaires-et-reportages/
:Auteur·e: Emilie Aubry
:Interview et réalisation: Émilie Aubry assistée de : Marion Le Bourthe
:Montage: Etienne Migaise, Martin Saison
:Production: Angèle Le Névé, Juliette Droillard, Rudy Chambard

Iconographie : Tous droits réservés
© ARTE France - Juillet 2023

Description
===============

Voilà plus de neuf mois que la mort de la jeune Mahsa Amini pour avoir
mal porté son voile a fait l’effet d’un véritable séisme en Iran.

La population s’est alors révoltée contre un régime dans lequel elle ne
se reconnaît plus.
Pourtant, cette révolution semble avoir aujourd’hui disparu de nos écrans.

La faute, peut-être, à une répression féroce qui paraît l’avoir étouffée :
cinq cent quarante manifestants tués et près de vingt mille personnes
arrêtées et emprisonnées.

La fracture entre le régime des mollahs et la société iranienne semble
néanmoins bel et bien actée.
Pour éclairer et mieux comprendre cette situation, Émilie Aubry s’entretient
avec Azadeh Kian, professeure franco-iranienne de sociologie à
l’Université Paris-Cité.

Elle analyse les conséquences de la contestation et les transformations
en cours dans la société iranienne.

Le régime islamiste a-t-il réellement été ébranlé par le mouvement populaire ?
À quoi ressemble la vie des Iraniennes aujourd’hui ?
En quoi cette révolte est-elle différente des précédents mouvements ?
Quelles sont les conséquences sur la scène internationale ?

Une Leçon de Géopolitique du Dessous des cartes pour mieux comprendre
les grands enjeux de notre époque.

