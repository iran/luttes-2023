.. index::
   pair: Video ; Tracks East Les filles d'Iran en colère
   pair: Chanteuse ; Liraz  Charhi
   pair: Album ; Roya

.. _video_tracks_east:

================================================================================================
**Tracks East Les filles d'Iran en colère** disponible: sur arte du 15/09/2023 au 13/09/2024
================================================================================================

- https://www.arte.tv/fr/videos/112352-021-A/tracks-east/


:Durée: 32 min
:Disponible: Du 15/09/2023 au 13/09/2024
:Pays: Allemagne
:Année:  2023
:Genre: https://www.arte.tv/fr/videos/emissions/

Description
===============

En septembre 2022, un voile "mal" ajusté a déclenché un mouvement de
révolte en République islamique d’Iran.

Dans ce pays dirigé d’une main de fer, une vague d’exécutions a ébranlé
la société civile. Mais quelque chose a changé : la génération Z,
ultra-connectée, ne se laisse plus impressionner par la vieille garde
du patriarcat au pouvoir.

"Tracks East" a suivi ces jeunes qui manifestent leur révolte à travers
l’art et la musique.

Les artistes Sarina Panahida, Liraz Charhi et Faravaz ont été contraintes
à l’exil.

L’actrice Sarina Panahida
---------------------------

L’actrice Sarina Panahida aborde la situation des droits humains dans ses
performances.


**L’actrice et chanteuse israélo-iranienne Liraz  Charhi**
--------------------------------------------------------------

- https://en.wikipedia.org/wiki/Liraz_Charhi
- https://www.youtube.com/@liraz3725/videos
- https://en.wikipedia.org/wiki/Iranian_Jews_in_Israel
- https://en.wikipedia.org/wiki/Persian_Jews
- https://www.thejc.com/life-and-culture/all/liraz-charhi-is-singing-for-her-sisters-1aphuJQEogstvaztktv9zb
- https://www.arte.tv/fr/videos/112389-000-A/liraz-charhi-la-bande-son-des-manifestations-en-iran/


.. figure:: images/liraz_charhi.png
   :align: center

L’actrice et chanteuse israélo-iranienne Liraz  Charhi écrit des textes
politiques qui sont devenus des hymnes pour de nombreux contestataires
en Iran.


2022
++++++++

- https://www.youtube.com/watch?v=sYYGpKViZWs (Roya women version)

.. youtube:: sYYGpKViZWs


2019
+++++++++

- https://fr.timesofisrael.com/liraz-charhi-chanteuse-israelienne-originaire-diran-jouera-a-rennes-et-paris/

Liraz, qui est aussi actrice, a sorti trois albums depuis 2005, dont le
dernier, « Naz », en 2019, chez Dead Sea Recordings.
Un disque dans lequel la question du rôle de la femme dans la société
est omniprésente.
Son titre phare « Nozi Nozi » fait ainsi référence à l’archétype iranien
de l’épouse parfaite.

La famille de la chanteuse a quitté l’Iran au début des années 1970 avant
la Révolution islamique. Son style musical est influencé par la culture
persane et la musique traditionnelle iranienne.
Elle chante pour se « reconnecter à ses racines perses », indique sa fiche
de présentation. Ainsi, son répertoire, chanté en farsi, « mêle morceaux
originaux et reprises d’artistes iraniens », et notamment les célèbres
chanteurs Gougoush et Dariush.

L’interprète a également été influencée par les sonorités contre-révolutionnaires ??
de l’importante communauté iranienne à Los Angeles.


Faravaz
----------

- https://www.instagram.com/faravazmusic/

Faravaz, quant à elle, fustige les dirigeants dans ses textes : "Je veux
baiser avec le mollah ; mollah mollah, deviens mon esclave !".

Condamnée par contumace à un an d’emprisonnement, elle n’est jamais retournée
dans son pays.

À l’époque de la révolution islamique de 1979, des femmes s’étaient déjà
mobilisées pour préserver leurs libertés.

Le réalisateur Sepehr Atefi, minorité religieuse baha’ie
----------------------------------------------------------

Le réalisateur Sepehr Atefi a tourné un documentaire sur dix femmes appartenant
comme lui à la minorité religieuse baha’ie qui, il y a 40 ans, ont été
exécutées pour s’être rebellées contre ce régime théocratique.

