.. index::
   pair: Guerrière de la Paix ; Hanna Assouline
   pair: Guerrière de la Paix ; Shirin Ebadi
   pair: Nobel de la Paix 2003; Shirin Ebadi
   ! Forum des Femmes pour la Paix à Essaouira


.. _femmes_paix_essaouira_2023_03_07:

==================================================================
2023-03-07 **Le Forum des Femmes pour la Paix à Essaouira**
==================================================================

- https://www.lesguerrieresdelapaix.com/actions/le-forum-des-femmes-pour-la-paix/
- https://www.lesguerrieresdelapaix.com/
- https://www.lesguerrieresdelapaix.com/forum-mondial-des-femmes-pour-la-paix/
- https://www.instagram.com/guerrieres_paix_mvt/
- https://linktr.ee/lesguerrieresdelapaix
- https://www.instagram.com/hanna.assoulinee/
- :ref:`chinsky:guerrieres_paix_2023_11_05`
- https://www.youtube.com/watch?v=wPM7QgotXR0 (Les guerrières de la Paix, sur public sénat)

Introduction
===============

Ces 7 et 8 mars 2023 ont marqué  la naissance du **1er Forum Mondial des
Femmes pour La Paix** réunissant des femmes activistes du monde entier.

« Dans un contexte d’urgence », nous avons lancé un appel international
pour la Paix à l’occasion de la Journée internationale des droits des femmes.

**Nous formons ensemble la plus belle des armées, celle qui se bat pour
la vie**, Hanna Assouline, réalisatrice, militante et co-fondatrice des
Guerrières de la Paix.

Quelques guerrières
=====================

Shirin Ebadi, Shakiba Dawod, Sonia Terrab, Jessica Mwiza, Ken Bugul, Grace Towahh Jarsor, Nurit Hagraph, Reem Alhajajra
-----------------------------------------------------------------------------------------------------------------------------

.. figure:: images/quelques_femmes.jpg

   https://www.lesguerrieresdelapaix.com/forum-mondial-des-femmes-pour-la-paix/


Samia Armosh, Huda Abu Arqoub, Yara Amayra, Noa Gur Golan, Dilnur Reyhan, Chahla Chafiq, Oxana Melnychuk, Zalina Steve
---------------------------------------------------------------------------------------------------------------------------


.. figure:: images/quelques_femmes_2.jpg

   https://www.lesguerrieresdelapaix.com/forum-mondial-des-femmes-pour-la-paix/


Robi Damelin, Laila Alshekh
------------------------------------------------------------


.. figure:: images/quelques_femmes_3.jpg

   https://www.lesguerrieresdelapaix.com/forum-mondial-des-femmes-pour-la-paix/


Citations de Shirin Ebadi, Huda Abu Arqoub, femmes militantes
===============================================================


.. _shirin_Ebadi_2023_03_07:

**Shirin Ebadi, avocate iranienne Nobel de la Paix en 2003**
---------------------------------------------------------------

« Je suis heureuse d’être parmi des femmes de différentes cultures et
religions dans une ville connue pour sa tolérance et ses valeurs de coexistence« ,
s’est félicitée, au micro de l’AFP, Shirin Ebadi, avocate iranienne nobélisée en 2003.

.. _huda_arqoub_2023_03_07:

**Huda Abu Arqoub, directrice palestinienne de l’Alliance pour la paix au Moyen-Orient (ALLMEP)**
----------------------------------------------------------------------------------------------------

« Ce rassemblement réunit des femmes du monde entier. Chacune d’elles porte
des histoires aussi difficiles les unes que les autres. C’est important
aujourd’hui de reconnaître leur force« , a dit à l’AFP, Huda Abu Arqoub,
directrice palestinienne de l’Alliance pour la paix au Moyen-Orient (ALLMEP).

Sur deux jours, ces femmes de terrains (iraniennes, israéliennes,
palestiniennes, afghanes …) qui se battent chacune pour des causes
différentes se sont rencontrées pour la première fois et ont réfléchi
ensemble à des actions concrètes, des solutions communes à mettre en
place dans un futur proche.

Elles ont partagé également leurs expériences, leurs vécus et leurs points
de vue lors de conférences et débats avant de réaliser ensemble une
marche pour la paix sur la plage Essaouira le mercredi 8 mars 2023.

Parmi elles, il y avait,  Shirin EBADI – Prix Nobel de la Paix,
Jessica MWIZA – Militante de la Mémoire, et aussi Huda ABU ARQUOB –
Présidente de l’Alliance for Middle East Peace.

Les sujets qui ont été abordé : comment créer une solidarité internationale
grâce à ce forum, la place de la femme sur les lieux de conflits (résolution 13-25 de l’ONU),
la montée de la discrimination, du racisme, de l’antisémitisme, la haine
des musulmans ou encore le sexisme …

Ce rendez-vous a marqué le point de départ du Forum Mondial des Femmes
pour La Paix sous le haut patronage de l’UNESCO et des Nations Unies.

Il s’agit là d’INTERPELLER le monde avant de présenter la suite des actions
et de faire entendre la voix de ces femmes.


Autres liens
==============

- https://www.youtube.com/watch?v=xnRzwzczf6U
- https://www.lecourrierdelatlas.com/essaouira-se-mobilise-pour-la-paix/
- https://www.arabnews.fr/node/355716/monde-arabe
- https://lematin.ma/express/2023/lancement-essaouira-forum-mondial-femmes-paix/387488.html
- https://www.libe.ma/Des-Femmes-militantes-du-Maroc-et-d-ailleurs-marchent-pour-la-paix-a-Essaouira_a137731.html
- https://www.mapexpress.ma/actualite/societe-et-regions/essaouira-femmes-militantes-du-maroc-dailleurs-marchent-paix/
- https://www.yabiladi.com/articles/details/137744/femmes-militantes-maroc-d-ailleurs-marchent.html

Retrouvez ci-dessous des articles de presse relayant l’évènement :

- `Reportage i24NEWS Français: A Essaouira, des Femmes pour la Paix <https://www.youtube.com/watch?v=xnRzwzczf6U>`
- `Le Courrier de l’Atlas : Essaouira se mobilise pour la Paix <https://www.lecourrierdelatlas.com/essaouira-se-mobilise-pour-la-paix/>`_
- `ArabNews : Des femmes du monde entier réunies au Maroc lancent un appel pour la paix <https://www.arabnews.fr/node/355716/monde-arabe>`_

womenwagepeace
-----------------

- https://www.womenwagepeace.org.il/en/feed/
- https://www.womenwagepeace.org.il/en/wwp-representatives-at-womens-world-peace-forum-in-morroco/

On March 7-8, 2023, a special event took place in the city of Essaouira
in Morocco: Women's World Peace Forum.

The event was organized by Hana Assoulin, who in 2017 created a film about
Women Wage Peace, entitled "Peace Fighters" and later founded a movement
in France with that name.

Together with Hana, UNESCO and Azoulay André are also partners in the
organization and financing of the event.

The event was attended and spoken by women from all over the world,
peace activists and women working for freedom.
These women conveyed a common message, a call for peace, to the whole world.

Women Wage Peace were represented by members of the movement, Nurit Hajag
and Samia Armosh, who shared with the participants of the event their
personal journey and our special journey as a movement.

The Palestinian movement, Women of the sun also sent a delegation


