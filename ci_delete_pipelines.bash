#!/bin/bash
for id in `glab pipeline list --sort asc -P 20| awk '{print substr($3,2)}'| tail -n+2`; do
    glab pipeline delete $id;
done
