.. index::
   pair: La dernière place; Livre
   pair: La dernière place; PS752
   pair: Négar Djavadi; Ecrivaine


.. _la_derniere_place:

====================================================================================================
**La dernière place** de Négar Djavadi
====================================================================================================

- https://www.editions-stock.fr/livres/la-derniere-place-9782234093942
- https://www.librairielesquare.com/

.. figure:: images/recto_la_derniere_place.png
   :align: center

Description
=============

Le 8 janvier 2020, le vol  752 d’Ukraine International Airlines reliant
Téhéran à Kiev s’écrase six minutes après le décollage entraînant la mort
des 176 passagers et membres d’équipage.

Ce crash survient dans un contexte de tensions extrêmes entre l’Iran
et les États-Unis.

À travers l’histoire de sa cousine Niloufar Sadr, présente sur ce vol,
Négar Djavadi relate cette tragédie.

Traumatisme national, la chute du PS752 est l’un des événements qui
annoncent le mouvement révolutionnaire qui s’est emparé de l’Iran à
l’automne  2022.


Négar Djavadi
===============

Scénariste et écrivaine, Négar Djavadi est née en Iran.

Elle est l’auteure de Désorientale (Prix de l’Autre Monde, Prix du style
2016, Prix Emmanuel-Roblès, Prix Première, Prix littéraire de la Porte
Dorée, Prix du roman News) et Arène (Prix Millepages 2020) aux
éditions Liana Levi.


