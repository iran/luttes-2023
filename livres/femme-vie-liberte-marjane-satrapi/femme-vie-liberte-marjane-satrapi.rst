.. index::
   pair: Femme Vie Liberté (Marjane Satrapi) ; Livre


.. _livre_femme_vie_satrapi_2023:

====================================================================================================
Bande dessinée **Femme Vie Liberté** de Marjane Satrapi
====================================================================================================

- https://www.placedeslibraires.fr/livre/9782378803780-femme-vie-liberte-collectif/
- https://www.librairielesquare.com/


.. figure:: images/recto_livre.png
   :align: center


.. figure:: images/verso.png
   :align: center

.. figure:: images/direction_marjane_satrapi.png
   :align: center

Résumé
=========

- https://www.decitre.fr/livres/femme-vie-liberte-9782378803780.html#resume

Joann Sfar - Coco - Mana Neyestani - Catel - Pascal Rabaté - Patricia Bolanos
Paco Roca - Bahareh Akrami - Hippolyte - Shabnam Adiban - Lewis Trondheim -
Deloupy - Touka Neyestani - Bee - Winshluss - Nicolas Wild -
Hamoun

Femme, vie, liberté : avoir vingt ans en Iran et mourir pour le droit
des femmes. Le 16 septembre 2022, en Iran, Mahsa Amini succombe aux coups
de la police des moeurs parce qu'elle n'avait pas "bien" porté son voile.

Son décès soulève une vague de protestations dans l'ensemble du pays, qui
se transforme en un mouvement féministe sans précédent.

Marjane Satrapi a réuni trois spécialistes : Farid Vahid, politologue,
Jean-Pierre Perrin, grand reporter, Abbas Milani, historien, et dix-sept
des plus grands talents de la bande dessinée pour raconter cet évenement
majeur pour l'Iran, et pour nous toutes et nous tous.


À propos de Marjane Satrapi
--------------------------------

Marjane Satrapi

.. figure:: images/marjane_satrapi.png
   :align: center

   Biographie de Marjane Satrapi

Un livre dirigé par Marjane Satrapi. Marjane Satrapi est l'autrice de
Persépolis, une série autobiographique, vendue à plus de quatre millions
d'exemplaires et récompensée dans le monde entier.

Après avoir écrit deux autres albums (Broderies, nommé dans la catégorie
du meilleur album au Festival d'Angoulême, et Poulet aux prunes, couronné
par le prix du Meilleur Album d'Angoulême), elle se consacre ensuite au
cinéma puis à la peinture.

Le film Persépolis, coréalisé avec Winshluss, grand auteur de bande dessinée,
qui participe aussi au livre, reçoit deux Césars et est nominé aux Oscars.

Les autrices et auteurs
--------------------------

.. _farid_vahid_satrapi:
.. _jean_perrin_satrapi:

Farid Vahid, Jean-Pierre Perrin
++++++++++++++++++++++++++++++++++

.. figure:: images/farid_vahid_jean_pierre_perrin.png
   :align: center

   Farid Vahid Jean-Pierre Perrin

Abbas Milani, Alba Beccaria
++++++++++++++++++++++++++++++++++

.. figure:: images/abbas_milani_alba_beccaria.png
   :align: center

   Abbas Milani et Alba Beccaria

Shabnam Adibam, Bahareh Akrami et Bee
++++++++++++++++++++++++++++++++++++++++

- :ref:`akrami_iran:dessins_bahareh_akrami`

.. figure:: images/shabnam_adibam_bahareh_akrami_bee.png
   :align: center

   Shabnam Adibam, Bahareh Akrami Bee


Message de Bahareh Akrami sur mastodon
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- https://mastodon.social/@bahareh_akrami/111080416683588226

Le 14 septembre 2023 est sorti en librairie le roman graphique « Femme, Vie, Liberté »
auquel j’ai eu l’honneur de contribuer au côté de très grands dessinateurs
et sous la direction artistique de Marjane Satrapi !

Un an après l’assassinat de Mahsa Jina Amini et le début de la révolution
ce livre rend hommage à toutes les iraniennes et les iraniens qui se
battent avec un courage incroyable pour un Iran libre !

Nous ne vous oublions pas, nous relayons vos voix ✊🏽

 #Iran #MahsaAmini #IranRevoIution

.. figure:: images/pouet_akrami.png
   :align: center

   https://mastodon.social/@bahareh_akrami/111080416683588226


Patricia Bolanis, Catel Coco
++++++++++++++++++++++++++++++++++++++

.. figure:: images/patricia_bolanis_catel_coco.png
   :align: center

   Patricia Bolanis, Catel Coco


Deloupy, Hippolyte, Mana Neyestani
++++++++++++++++++++++++++++++++++++

.. figure:: images/deloupy_hippolyte_mana_neyestani.png
   :align: center

   Deloupy, Hippolyte, Mana Neyestani


Touka Neyestami, Pascal Rabate, Rahi Rezvani
+++++++++++++++++++++++++++++++++++++++++++++++++++

.. figure:: images/touka_neyestami_pascal_rabate_rahi_rezvani.png
   :align: center

   Touka Neyestami, Pascal Rabate, Rahi Rezvani


Paco Roca, Joann Sfar Lewis Trondheim
+++++++++++++++++++++++++++++++++++++++++

.. figure:: images/paco_roca_joann_sfar_lewis_trondheim.png
   :align: center

   Paco Roca, Joann Sfar Lewis Trondheim



Introduction
================

.. figure:: images/intro_page_1.png
   :align: center

.. figure:: images/intro_page_2.png
   :align: center


Sommaire
===========

.. figure:: images/sommaire_page_1.png
   :align: center

.. figure:: images/sommaire_page_2.png
   :align: center

.. figure:: images/sommaire_page_3.png
   :align: center

.. figure:: images/sommaire_page_4.png
   :align: center


Autres liens
==============

2023-09-14 Faut-il être stupide pour avoir de l’espoir pour l’Iran ?
-------------------------------------------------------------------------------------

- https://www.radiofrance.fr/franceinter/podcasts/la-question-qui-fache/la-question-qui-fache-du-jeudi-14-septembre-2023-8682272
