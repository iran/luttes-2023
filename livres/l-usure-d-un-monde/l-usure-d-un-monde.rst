.. index::
   pair: L'usure d'un monde; Livre
   pair: François-Henri Désérable; Ecrivain


.. _lusure_dun_monde:

================================================================================
**L'usure d'un monde** de François-Henri Désérable
================================================================================

- https://www.gallimard.fr/Catalogue/GALLIMARD/Blanche/L-usure-d-un-monde
- https://www.librairielesquare.com/


.. figure:: images/livre.png
   :align: center


Description
===============

«La peur était pour le peuple iranien une compagne de chaque instant,
la moitié fidèle d’une vie. Les Iraniens vivaient avec dans la bouche le
goût sablonneux de la peur.
Seulement, depuis la mort de Mahsa Amini, la peur était mise en sourdine :
elle s’effaçait au profit du courage. »

Fin 2022, au plus fort de la répression contre les manifestations qui
suivent la mort de Mahsa Amini, François-Henri Désérable passe quarante
jours en Iran, qu’il traverse de part en part, de Téhéran aux confins
du Baloutchistan.

Arrêté par les Gardiens de la révolution, sommé de quitter le pays, il
en revient avec ce récit dans lequel il raconte l’usure d’un monde : celui
d’une République islamique aux abois, qui réprime dans le sang les
aspirations de son peuple.


François-Henri Désérable
L’usure d’un monde. Une traversée de l'Iran
Collection Blanche, Gallimard
Parution : 04-05-2023


Articles
=========

- https://www.lepoint.fr/culture/le-pari-fou-d-un-ecrivain-francais-en-iran-03-05-2023-2518842_3.php
