.. index::
   pair: Femme Rêve Liberté; Livre


.. _livre_femme_reve_liberte:

====================================================================================================
**Femme, Rêve, Liberté** par un collectif
====================================================================================================

- https://www.actes-sud.fr/catalogue/litterature-etrangere/femme-reve-liberte
- https://www.librairielesquare.com/

.. figure:: images/recto_femme_reve_liberte.png
   :align: center

COLLECTIF
===========

- Sorour KASMAÏ
- Bahiyyih NAKHJAVANI
- Sahar DELIJANI
- Zahra KHANLOO
- Azar MAHLOUJIAN
- Aida MORADI AHANI
- Parisa REZA
- Fariba VAFI
- Fahimeh FARSAIE
- Rana SOLEIMANI
- Asieh NEZAM SHAHIDI
- Nasim MARASHI

Que signifie être une femme aujourd’hui en Iran ?

Quelle place occupe-t-elle dans une société régie depuis plus de quatre
décennies par une théocratie totalitaire exclusivement masculine ?

Quelles sont les raisons de la colère qui a embrasé le pays depuis le
16 septembre 2022, le jour où la jeune Mahsa Jina Amini a succombé aux
coups de la police des mœurs ?

À l’invitation de Sorour Kasmaï, douze autrices iraniennes racontent
dans ce recueil, sous des formes diverses, ce qu’évoquent pour elles
les trois mots qui résonnent depuis : FEMME, VIE, LIBERTÉ.

En soutien à celles et ceux qui se sont insurgés avec tant de courage
contre la tyrannie, voici donc douze histoires, douze voix qui viennent
s’unir au cri de cette révolte qui ne s’éteint pas.

Pour qu’on ne l’oublie pas.

Ont participé à ce recueil : Sahar Delijani, Fahimeh Farsaie, Sorour Kasmaï,
Zahra Khanloo, Azar Mahloujian, Nasim Marashi, Aida Moradi Ahani, Bahiyyih Nakhjavani,
Asieh Nezam Shahidi, Parisa Reza, Rana Soleimani et Fariba Vafi.

Les bénéfices des ventes de cet ouvrage seront reversés à une association.

